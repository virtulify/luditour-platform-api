import React from 'react';
import {FunctionField, ImageField, ImageInput, RichTextField} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';
import {HydraAdmin} from '@api-platform/admin';
import parseHydraDocumentation from '@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation';

const entrypoint = 'http://app.luditour.com/api';
/*
const myApiDocumentationParser = entrypoint => parseHydraDocumentation(entrypoint)
    .then( ({ api }) => {
        const medias = api.resources.find(({name}) => 'media' === name);

        const description = medias.fields.find(({name}) => 'filename' === name);

        description.input = props => (
            <RichTextInput {...props} source="description"/>
        );

        description.input.defaultProps = {
            addField: true,
            addLabel: true,
        };


        api.resources.map(resource => {
            if ('http://schema.org/ImageObject' === resource.id) {
                resource.fields.map(field => {
                    if ('http://schema.org/contentUrl' === field.id) {
                        field.denormalizeData = value => ({
                            src: value
                        });

                        field.fieldComponent = (
                            <FunctionField
                                key={field.name}
                                render={
                                    record => (
                                        <ImageField key={field.name} record={record} source={`${field.name}.src`}/>
                                    )
                                }
                                source={field.name}
                            />
                        );

                        field.inputComponent = (
                            <ImageInput accept="image/*" key={field.name} multiple={false} source={field.name}>
                                <ImageField source="src"/>
                            </ImageInput>
                        );

                        field.normalizeData = value => {
                            if (value[0] && value[0].rawFile instanceof File) {
                                const body = new FormData();
                                body.append('file', value[0].rawFile);

                                return fetch(`${entrypoint}/images/upload`, { body, method: 'POST' })
                                    .then(response => response.json());
                            }

                            return value.src;
                        };
                    }

                    return field;
                });
            }

            return resource;
        });

        return { api };
    })
;

export default (props) => <HydraAdmin apiDocumentationParser={myApiDocumentationParser} entrypoint={entrypoint}/>;
*/
export default () => <HydraAdmin entrypoint={entrypoint}/>; // Replace with your own API entrypoint