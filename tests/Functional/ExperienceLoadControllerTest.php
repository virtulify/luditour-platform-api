<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 11-04-19
 * Time: 16:01
 */

namespace App\Tests\Functional;


use App\Entity\Experience;
use App\Tests\WebDbTestCase;

class ExperienceLoadControllerTest extends WebDbTestCase
{
    public function testNotFound()
    {
        $response = $this->jsonRequest('GET', "/api/experiences/100/load");
        self::assertEquals(404, $response->getStatusCode());
    }

    public function testGreenesis()
    {
        $greenesis = $this->entityManager->getRepository(Experience::class)->findOneBy(['name' => 'Greenesis']);
        $response = $this->jsonRequest('GET', "/api/experiences/" . $greenesis->getId() . "/load");
        self::assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        // validate mini game
        self::assertCount(1, $data['miniGames']);
        $catch = $data['miniGames'][0];
        self::assertEquals(1, $catch['type']);
        self::assertCount(3, $catch['enterRules']);

        // validate items
        $items = $data['items'];
        self::assertCount(2, $items);

        // validate quests
        self::assertCount(1, $data['quests']);

        // validate journey
        self::assertEmpty($data['journeys']);
    }

    public function testMiraculous()
    {
        $miraculous = $this->entityManager->getRepository(Experience::class)->findOneBy(['name' => 'Miraculous']);
        $response = $this->jsonRequest('GET', "/api/experiences/" . $miraculous->getId() . "/load");
        self::assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        // validate mini game
        self::assertEmpty($data['miniGames']);

        // validate items
        self::assertEmpty($data['items']);

        // validate quests
        self::assertEmpty($data['quests']);

        // validate journey
        self::assertEmpty($data['journeys']);
    }
}