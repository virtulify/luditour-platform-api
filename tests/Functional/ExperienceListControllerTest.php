<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 09-04-19
 * Time: 14:13
 */

namespace App\Tests\Functional;


use App\Entity\Experience;
use App\Tests\WebDbTestCase;

class ExperienceListControllerTest extends WebDbTestCase
{
    const ROUTE = "/api/experiences/list";

    /**
     * Test the list of public experiences
     */
    public function testPublic()
    {
        $response = $this->jsonRequest('GET', self::ROUTE);

        // Status code should be 200
        self::assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        // Only one publci event : Miraculous
        self::assertCount(1, $data);
        self::assertEquals('Miraculous', $data[0]['name']);
    }

    public function testFormatPositionError()
    {
        $response = $this->jsonRequest('GET', self::ROUTE, ['latitude' => 'coucou', 'longitude' => 'bien ?']);
        self::assertEquals(400, $response->getStatusCode());
    }

    public function testWithPosition()
    {
        $response = $this->jsonRequest('GET', self::ROUTE, ['latitude' => 69.69, 'longitude' => 69.69]);
        self::assertEquals(200, $response->getStatusCode());

        // Experience found must be Miraculous (public) and Greenesis (with position)
        $data = json_decode($response->getContent(), true);
        self::assertCount(2, $data);
    }

    public function testPositionTooFar()
    {
        $response = $this->jsonRequest('GET', self::ROUTE, ['latitude' => 0.69, 'longitude' => 0.69]);
        self::assertEquals(200, $response->getStatusCode());

        // Experience found must be Miraculous (public)
        $data = json_decode($response->getContent(), true);
        self::assertCount(1, $data);
        self::assertEquals('Miraculous', $data[0]['name']);
    }
}