<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 11-04-19
 * Time: 16:39
 */

namespace App\Tests\Functional;


use App\Entity\MiniGame;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use App\Entity\VisitorLine;
use App\Tests\WebDbTestCase;

class PlayMiniGameTest extends WebDbTestCase
{
    /**
     * Play when the mini game is not found
     */
    public function testPlay()
    {
        // Get current visitor, visitor line for greenesis and the catch mini game
        $visitor = $this->entityManager->getRepository(Visitor::class)->findOneBy(['mail' => 'bendo.du69@skynet.be']);
        $visitorLine = $this->entityManager->getRepository(VisitorLine::class)->findOneBy(['visitor' => $visitor]);
        $miniGame = $this->entityManager->getRepository(MiniGame::class)->findOneBy([
            'name' => 'Chat'
        ]);

        $response = $this->jsonRequest('POST', '/api/mini_game_states', [], [], [
            'scorePerformed' => 3000000,
            'miniGame' => '/api/mini_games/' . $miniGame->getId(), // id does not exists
            'player' => '/api/visitor_lines/' . $visitorLine->getId() // id does not exists
        ]);

        $data = json_decode($response->getContent(), true);

        // Assert mini game state and response
        self::assertEquals(201, $response->getStatusCode());
        self::assertEquals(1000, $data['scoreWon']);
        self::assertEquals(100, $data['scoreLost']);

        // Assert visitor item won by visitor
        self::assertCount(1, $data['itemsWon']);
        $visitorItemId = intval(str_replace("/api/visitor_items/", "", $data['itemsWon'][0]));
        $won = $this->entityManager->getRepository(VisitorItem::class)->find($visitorItemId);
        self::assertEquals($visitor->getId(), $won->getVisitor()->getId());
        self::assertEquals('Pièce', $won->getItem()->getName());

        // Assert visitor item lost by visitor
        self::assertCount(1, $data['itemsLost']);
        $visitorItemId = intval(str_replace("/api/visitor_items/", "", $data['itemsLost'][0]));
        $lost = $this->entityManager->getRepository(VisitorItem::class)->find($visitorItemId);
        self::assertNull($lost->getVisitor());
        self::assertTrue('Stellagraine' === $lost->getItem()->getName());
        self::assertNotNull($lost->getDeletedAt());

        // Assert visitor items consumed with the participation of the mini game
        self::assertCount(3, $data['itemsConsumed']);
        foreach ($data['itemsConsumed'] as $idConsumed) {
            $visitorItemId = intval(str_replace('/api/visitor_items/', '', $idConsumed));
            $consumed = $this->entityManager->getRepository(VisitorItem::class)->find($visitorItemId);
            self::assertEquals($consumed->getItem()->getName(), 'Pièce');
            self::assertNull($consumed->getVisitor());
            self::assertFalse($visitor->getVisitorItems()->contains($consumed));
        }

        self::assertFalse($visitor->getVisitorItems()->filter(function (VisitorItem $visitorItem) {
            return !$visitorItem->getDeletedAt();
        })->contains($lost));
        self::assertTrue($visitor->getVisitorItems()->contains($won));

        // Assert score of visitor line, experience team and team
        self::assertEquals(2400, $visitorLine->getScore());
        self::assertEquals(2400, $visitorLine->getExperienceTeam()->getScore());
        self::assertEquals(2400, $visitorLine->getExperienceTeam()->getTeam()->getScore());
    }

    /**
     * Test when the visitor line submitted is not related to the logged in visitor
     */
    public function testNotRelatedVisitor()
    {
        // Get the second visitor, visitor line for greenesis and the catch mini game
        $second = $this->entityManager->getRepository(Visitor::class)->findOneBy(['mail' => 'second.lol@gmail.com']);
        $visitorLine = $this->entityManager->getRepository(VisitorLine::class)->findOneBy(['visitor' => $second]);
        $miniGame = $this->entityManager->getRepository(MiniGame::class)->findOneBy([
            'name' => 'Chat'
        ]);

        $response = $this->jsonRequest('POST', '/api/mini_game_states', [], [], [
            'scorePerformed' => 3000000,
            'miniGame' => '/api/mini_games/' . $miniGame->getId(), // id does not exists
            'player' => '/api/visitor_lines/' . $visitorLine->getId() // id does not exists
        ]);

        // error status code (doctrine extension won't allow that)
        self::assertEquals(500, $response->getStatusCode());
        self::assertStringStartsWith("Item not found for", json_decode($response->getContent(), true)['detail']);
    }

    public function testGameDoneOnce()
    {
        // Get current visitor, visitor line for greenesis and the catch mini game
        $visitor = $this->entityManager->getRepository(Visitor::class)->findOneBy(['mail' => 'bendo.du69@skynet.be']);
        $visitorLine = $this->entityManager->getRepository(VisitorLine::class)->findOneBy(['visitor' => $visitor]);
        $miniGame = $this->entityManager->getRepository(MiniGame::class)->findOneBy([
            'name' => 'Chat'
        ]);

        // First mini game state posted that SHOULD WORK
        $response = $this->jsonRequest('POST', '/api/mini_game_states', [], [], [
            'scorePerformed' => 3000000,
            'miniGame' => '/api/mini_games/' . $miniGame->getId(), // id does not exists
            'player' => '/api/visitor_lines/' . $visitorLine->getId() // id does not exists
        ]);
        self::assertEquals(201, $response->getStatusCode());

        // Second mini game state posted that SHOULD NOT WORK (enter rule with DONE ONCE is activated)
        $response = $this->jsonRequest('POST', '/api/mini_game_states', [], [], [
            'scorePerformed' => 3000000,
            'miniGame' => '/api/mini_games/' . $miniGame->getId(), // id does not exists
            'player' => '/api/visitor_lines/' . $visitorLine->getId() // id does not exists
        ]);
        $data = json_decode($response->getContent(), true);
        self::assertEquals(400, $response->getStatusCode());
        self::assertStringEndsWith('refuses the access of the mini game for the current visitor.', $data['detail']);
    }

    /**
     * Post a MiniGameState while the visitor does not have the required item
     */
    public function testGameNoRequiredItem()
    {
        // Get current visitor, visitor line for greenesis and the catch mini game
        $visitor = $this->entityManager->getRepository(Visitor::class)->findOneBy(['mail' => 'bendo.du69@skynet.be']);
        $visitorLine = $this->entityManager->getRepository(VisitorLine::class)->findOneBy(['visitor' => $visitor]);
        $miniGame = $this->entityManager->getRepository(MiniGame::class)->findOneBy([
            'name' => 'Chat'
        ]);

        // remove all the visitor's inventory
        /** @var VisitorItem $visitorItem */
        foreach ($visitor->getVisitorItems() as $visitorItem) {
            $visitor->removeVisitorItem($visitorItem);
        }
        $this->entityManager->flush();

        // First mini game state posted that SHOULD WORK
        $response = $this->jsonRequest('POST', '/api/mini_game_states', [], [], [
            'scorePerformed' => 3000000,
            'miniGame' => '/api/mini_games/' . $miniGame->getId(), // id does not exists
            'player' => '/api/visitor_lines/' . $visitorLine->getId() // id does not exists
        ]);
        $data = json_decode($response->getContent(), true);
        self::assertEquals(400, $response->getStatusCode());
        self::assertStringEndsWith("refuses the access of the mini game for the current visitor.", $data['detail']);
    }
}