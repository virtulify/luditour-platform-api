<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 09-04-19
 * Time: 14:15
 */

namespace App\Tests;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebDbTestCase
 * @package App\Tests
 * Test case used to interact with the sqlite test database.
 * Basically, it will reset the database after each test case.
 * Strongly inspired by the article https://blog.joeymasip.com/symfony-3-4-phpunit-testing-database-data/ from Joeymasip
 */
abstract class WebDbTestCase extends WebTestCase
{
    /**
     * @var Application
     */
    protected static $application;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ContainerInterface
     */
    protected $containerTest;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param $command
     * @return int
     * @throws \Exception
     */
    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    protected function setUp()
    {
        // Drops the old database and create a new one
        self::runCommand('doctrine:database:drop --force');
        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:update --force');
        self::runCommand('doctrine:fixtures:load');
        $this->client = static::createClient();
        $this->containerTest = $this->client->getContainer();
        $this->entityManager = $this->containerTest->get('doctrine.orm.entity_manager');
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    protected function tearDown()
    {
        // drops the database after test finished
        self::runCommand('doctrine:database:drop --force');

        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }

    /**
     * Performs a JSON request to the
     * @param string $method
     * @param string $uri
     * @param array $parameters
     * @param array $header
     * @param array|null $body
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function jsonRequest(string $method, string $uri, array $parameters = [], array $header = [], array $body = null)
    {
        $header['CONTENT_TYPE'] = 'application/json';
        $header['HTTP_ACCEPT'] = 'application/json';

        if($body) {
            $this->client->request($method, $uri, $parameters, [], $header, json_encode($body));
        }
        else {
            $this->client->request($method, $uri, $parameters, [], $header);
        }
        return $this->client->getResponse();
    }
}