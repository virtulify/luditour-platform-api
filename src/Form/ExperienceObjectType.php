<?php

namespace App\Form;

use App\Entity\ExperienceObject;
use App\Entity\GenerationRule;
use App\Entity\VisitorLine;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperienceObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('latitude', NumberType::class)
            ->add('longitude', NumberType::class)
            ->add('altitude', NumberType::class)
            ->add('creator', EntityType::class, ['class' => VisitorLine::class])
            ->add('generationRule', EntityType::class, ['class' => GenerationRule::class]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExperienceObject::class,
            'csrf_protection' => false
        ]);
    }
}
