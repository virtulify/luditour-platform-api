<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 08/11/2018
 * Time: 18:33
 */

namespace App\Manager;


use App\Entity\Experience;
use App\Entity\Item;
use App\Entity\Position;
use App\Entity\Visitor;
use Doctrine\ORM\EntityManagerInterface;

class ExperienceManager
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExperienceManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns the experiences near visitor
     * @param Visitor $visitor
     * @return mixed
     */
    public function getExperiencesNearVisitor(Visitor $visitor)
    {
        return $this->entityManager->getRepository('App:Experience')->findNearVisitor($visitor);
    }
}