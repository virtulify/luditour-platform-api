<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 08/11/2018
 * Time: 18:34
 */

namespace App\Manager;


class ToolManager
{


    /**
     * Returns the geographic circle boundaries for a specified lat, lng and radius
     *
     * Latitude: 1 deg = 110.574 km
     * Longitude: 1 deg = 110.574*cos(latitude) km
     * °1km lat  = 1/110.574
     * °1km lng = 1/110.574*cos(latitude)
     *
     * @param $kmRadius
     * @param $lat
     * @param $lng
     *
     * @return array
     */
    public function getAreaCoordinates($lat, $lng, $kmRadius = 0.5)
    {
        $kmLat = 0.00904371732957114692423173621285 * $kmRadius;

        $radLat = $lat * pi() / 180;
        $kmLng = (1 / (110.574 * cos($radLat))) * $kmRadius;

        return array(
            'maxLat' => $lat + $kmLat,
            'minLat' => $lat - $kmLat,
            'maxLng' => $lng + $kmLng,
            'minLng' => $lng - $kmLng
        );
    }


}