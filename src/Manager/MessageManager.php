<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 13-03-19
 * Time: 12:16
 */

namespace App\Manager;


use WowApps\SlackBundle\DTO\SlackMessage;
use WowApps\SlackBundle\Service\SlackBot;

final class MessageManager
{
    /**
     * @var SlackBot
     */
    private $slackbot;

    /**
     * MessageManager constructor.
     * @param SlackBot $slackbot
     */
    public function __construct(SlackBot $slackbot)
    {
        $this->slackbot = $slackbot;
    }

    /**
     * Send a new message to slack
     *
     * @param string $message
     */
    public function sendPlatformMessage($message)
    {
        $slackMessage = new SlackMessage();

        $slackMessage
            ->setRecipient('platformdev')
            ->setText('Message de la plateforme')
            ->setShowQuote(true)
            ->setQuoteType(SlackBot::QUOTE_INFO)
            ->setQuoteText($message)
            ->setIconEmoji(':owl:')
            ->setQuoteTitle('Message');

        $this->slackbot->sendMessage($slackMessage);
    }

    /**
     * Sends an error message to slack from the platform
     * @param String $messageTitle
     * @param String $errorTitle
     * @param String $content
     */
    public function sendSlackbotErrorMessage(String $messageTitle, String $errorTitle, String $content)
    {
        $slackMessage = new SlackMessage();

        $slackMessage
            ->setRecipient('platformdev')
            ->setText($messageTitle)
            ->setShowQuote(true)
            ->setQuoteType(SlackBot::QUOTE_DANGER)
            ->setQuoteText($content)
            ->setQuoteTitle($errorTitle)
            ->setIconEmoji(':owl:');

        $this->slackbot->sendMessage($slackMessage);
    }
}