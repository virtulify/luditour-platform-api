<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 13-02-19
 * Time: 13:55
 */

namespace App\Manager;


use App\Entity\Visitor;
use Doctrine\ORM\EntityManagerInterface;
use Facebook\GraphNodes\GraphUser;

class VisitorManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * VisitorManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Retrieves the visitor related to the specified oauth payload of an id token. If the visitor does not exist, it will be created.
     * @param $oauthProfile mixed the oauth payload of the id token used by the user.
     * @return Visitor the visitor related to the specified oauth profile
     */
    public function getOrCreateVisitorGoogle($oauthProfile): Visitor
    {
        /** @var Visitor $visitor */
        $visitor = $this->entityManager->getRepository(Visitor::class)->findOneBy(['googleAuth' => $oauthProfile['sub']]);

        if (!$visitor) { // creates the visitor if not exist
            $visitor = new Visitor();
            $visitor->setGoogleAuth($oauthProfile['sub']);
            $visitor->setMail($oauthProfile['email']);

            $parts = explode("@", $oauthProfile['email']);
            $username = $parts[0];
            $visitor->setName($username);

            // insert in database
            $this->entityManager->persist($visitor);
            $this->entityManager->flush();
        }

        return $visitor;
    }

    /**
     * Retrieves the visitor related to the specified FB Graph User. If the visitor does not exist, it will be created.
     *
     * @param GraphUser $facebookUser the FB Graph User related to the required visitor
     * @param string $accessToken the access token used to authenticate the user
     * @return Visitor the visitor related to the specified facebook user (or created if not exist)
     */
    public function getOrCreateVisitorFacebook(GraphUser $facebookUser, string $accessToken): Visitor
    {
        /** @var Visitor $visitor */
        $visitor = $this->entityManager->getRepository(Visitor::class)->findOneBy(['facebookAuth' => $facebookUser->getId()]);

        if (!$visitor) { // creates the visitor if not exist
            $visitor = new Visitor();

            $visitor->setFacebookAuth($facebookUser->getId());
            $visitor->setMail($facebookUser->getEmail());

            $visitor->setName($facebookUser->getName());
            $visitor->setFbAccessToken($accessToken);
            // insert in database
            $this->entityManager->persist($visitor);
        }

        $visitor->setFbAccessToken($accessToken);
        $this->entityManager->flush();
        return $visitor;
    }
}