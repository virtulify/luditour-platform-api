<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 03/12/2018
 * Time: 13:21
 */

namespace App\Manager;


use App\Entity\ExperienceObject;
use App\Entity\MapDataMiss;
use App\Entity\Visitor;
use Doctrine\ORM\EntityManagerInterface;

class MapManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SpotManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Register a map data miss for the current visitor
     * @param Visitor $visitor
     */
    public function registerMapDataMiss(Visitor $visitor) {
        $mpd = new MapDataMiss();
        $mpd->inflate($visitor);
        $this->entityManager->persist($mpd);
        $this->entityManager->flush();
    }

    /**
     * Returns the current map around the visitor
     * @param Visitor $visitor
     * @return array
     */
    public function getCurrentMap(Visitor $visitor) {
        $objects = $this->entityManager->getRepository('App:ExperienceObject')->findNearVisitor($visitor);
        $map = array();

        /**
         * @var $object ExperienceObject
         */
        foreach($objects as $object) {
            $map['objects'][$object->getLatitude().':'.$object->getLongitude()] = $object;
        }

        return $map;
    }
}