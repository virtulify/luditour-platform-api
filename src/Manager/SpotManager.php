<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 08/11/2018
 * Time: 17:52
 */

namespace App\Manager;


use App\Entity\Visitor;
use Doctrine\ORM\EntityManagerInterface;

class SpotManager
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SpotManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Refresh the map around a visitor, with generators or objects
     * @param Visitor $visitor
     * @return int
     */
    public function refreshWorldAroundVisitor(Visitor $visitor) {


        //$coordinates = $this->getAreaCoordinates($latitude, $longitude);
        //$this->entityManager->getRepository('App:Experience')->findNearLocation($coordinates);

        return 0;

    }

    /**
     * Check if the world around the visitor is fully generated (all generators, objects, ...)
     * @param Visitor $visitor
     * @return bool
     */
    public function checkWorldAroundVisitor(Visitor $visitor) {

        $currentExperience = $visitor->getCurrentExperience();

       // $generationRules = $currentExperience->getG

       return false;
    }


}