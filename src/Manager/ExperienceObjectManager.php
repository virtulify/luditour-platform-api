<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-03-19
 * Time: 14:00
 */

namespace App\Manager;


use App\Dto\ExperienceObjectDto;
use App\Entity\Experience;
use App\Entity\ExperienceObject;
use Doctrine\ORM\EntityManagerInterface;

class ExperienceObjectManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ToolManager
     */
    private $toolManager;

    /**
     * ExperienceObjectManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param ToolManager $toolManager
     */
    public function __construct(EntityManagerInterface $entityManager, ToolManager $toolManager)
    {
        $this->entityManager = $entityManager;
        $this->toolManager = $toolManager;
    }

    /**
     * Gets the experience objects that can be detected in the specified position thanks to the experience object radius available in the specified experience
     * @param Experience $experience
     * @param float $latitude
     * @param float $longitude
     * @return ExperienceObjectDto[]
     */
    public function getNearExperienceObjects(Experience $experience, float $latitude, float $longitude)
    {
        $coordinates = $this->toolManager->getAreaCoordinates($latitude, $longitude, $experience->getExperienceObjectRadius());

        return $this->entityManager->createQuery(
            "SELECT NEW ExperienceObjectDto(eo.id, g.id, eo.latitude, eo.longitude, eo.altitude, mam.filename, aam.filename)
                  
                  FROM ExperienceObject eo
                  JOIN  eo.generationRule g
                  JOIN  eo.mapAppearanceMedia mam
                  JOIN  eo.actionAppearanceMedia aam
                  
                  WHERE eo.deletedAt IS NULL
                  AND   eo.endDate >= :now
                  AND   eo.latitude >= :minLat
                  AND   eo.latitude <= :maxLat
                  AND   eo.longitude >= :minLng
                  AND   eo.longitude <= :maxLng
                  AND   g.experience = :experience
                  AND   g.deletedAt IS NULL
                  "
        )->setParameters([
            'now' => new \DateTime(),
            'minLat' => $coordinates['minLat'],
            'maxLat' => $coordinates['maxLat'],
            'minLng' => $coordinates['minLng'],
            'maxLng' => $coordinates['maxLng'],
            'experience' => $experience
        ])->getResult();
    }
}