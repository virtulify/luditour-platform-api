<?php

namespace App\Repository;

use App\Entity\MiniGame;
use App\Entity\MiniGameState;
use App\Entity\VisitorLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MiniGameState|null find($id, $lockMode = null, $lockVersion = null)
 * @method MiniGameState|null findOneBy(array $criteria, array $orderBy = null)
 * @method MiniGameState[]    findAll()
 * @method MiniGameState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MiniGameStateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MiniGameState::class);
    }

    /**
     * Finds the mini game states related to the specified mini game and visitorline
     * @param VisitorLine $visitorLine
     * @param MiniGame $miniGame
     * @return MiniGameState[]
     */
    public function findByVisitorLineAndMiniGame(VisitorLine $visitorLine, MiniGame $miniGame)
    {
        return $this->createQueryBuilder('mgs')
            ->where('mgs.player = :visitorLine')
            ->andWhere('mgs.miniGame = :miniGame')
            ->andWhere('mgs.deletedAt IS NULL')
            ->setParameters([
                'visitorLine' => $visitorLine,
                'miniGame' => $miniGame
            ])
            ->setParameter('visitorLine', $visitorLine)
            ->getQuery()
            ->getResult();
    }
}
