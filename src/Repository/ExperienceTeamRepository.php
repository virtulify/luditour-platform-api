<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Entity\ExperienceTeam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExperienceTeam|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExperienceTeam|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExperienceTeam[]    findAll()
 * @method ExperienceTeam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExperienceTeamRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExperienceTeam::class);
    }

    /**
     * Finds the ExperienceTeams related to the specified experience joined with the teams related to each experience team
     *  (because of performance issues)
     * @param Experience $experience
     * @param int $hydrationMode
     * @return ExperienceTeam[]|array
     */
    public function findByExperienceJoinedTeam(Experience $experience, int $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT et, t
                  FROM App\Entity\ExperienceTeam et
                  JOIN et.team t
                  WHERE et.deletedAt IS NULL
                  AND   t.deletedAt IS NULL
                  AND   et.experience = :experience"
        )->setParameters([
            'experience' => $experience
        ])->getResult($hydrationMode);
    }
}
