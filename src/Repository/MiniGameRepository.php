<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Entity\MiniGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MiniGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method MiniGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method MiniGame[]    findAll()
 * @method MiniGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MiniGameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MiniGame::class);
    }

    /**
     * Finds the mini games related to the specified experience with the enter rules hydrated (optimization purpose)
     * @param Experience $experience
     * @param int $hydrationMode the hydration mode used to fetch the data from the database. HYDRATE_ARRAY by default for performance purpose
     * @return array|MiniGame
     */
    public function findByExperienceJoinedEnterRules(Experience $experience, int $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT mg, er, i
                  FROM App\Entity\MiniGame mg
                  LEFT JOIN mg.enterRules er
                  LEFT JOIN er.requiredItem i
                  WHERE mg.experience = :experience
                  AND   mg.deletedAt IS NULL
                  AND   er.deletedAt IS NULL
                  AND   i.deletedAt IS NULL"
        )
            ->setParameters([
                'experience' => $experience
            ])
            ->getResult($hydrationMode);
    }
}
