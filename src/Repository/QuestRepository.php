<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 09-04-19
 * Time: 11:36
 */

namespace App\Repository;


use App\Entity\Experience;
use App\Entity\Quest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class QuestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quest::class);
    }

    /**
     * Finds the available quests related to the specified experience (for optimization issues)
     * @param Experience $experience
     * @param int $hydrationMode
     * @return Quest[]|array
     */
    public function findAvailableByExperience(Experience $experience, int $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT q
                  FROM App\Entity\Quest q
                  WHERE q.deletedAt IS NULL
                  AND   q.startDate <= :now
                  AND   q.endDate >= :now
                  AND   q.experience = :experience"
        )->setParameters([
            'experience' => $experience,
            'now' => new \DateTime()
        ])->getResult($hydrationMode);
    }
}