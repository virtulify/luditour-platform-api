<?php

namespace App\Repository;

use App\Entity\EndRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EndRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method EndRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method EndRule[]    findAll()
 * @method EndRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EndRuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EndRule::class);
    }

    // /**
    //  * @return EndRule[] Returns an array of EndRule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EndRule
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
