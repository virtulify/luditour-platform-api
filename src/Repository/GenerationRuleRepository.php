<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-04-19
 * Time: 14:58
 */

namespace App\Repository;


use App\Entity\GenerationRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method GenerationRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method GenerationRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method GenerationRule[]    findAll()
 * @method GenerationRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenerationRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GenerationRule::class);
    }
}