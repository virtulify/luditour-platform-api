<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 01-03-19
 * Time: 10:38
 */

namespace App\Repository;


use App\Entity\ExperienceTeam;
use App\Entity\Visitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VisitorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Visitor::class);
    }

    /**
     * Finds the visitors that have participated in the specified experience team.
     * @param ExperienceTeam $experienceTeam
     * @return Visitor[]
     */
    public function findVisitorsByExperienceTeam(ExperienceTeam $experienceTeam)
    {
        return $this->createQueryBuilder('vi')
            ->join('vi.visitorLines', 'vil')
            ->where('vil.experienceTeam = :experienceTeam')
            ->setParameter('experienceTeam', $experienceTeam)
            ->getQuery()
            ->getResult();
    }

    /**
     * Counts all the visitors available
     * @return integer
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        return $this->createQueryBuilder('vi')
            ->select('count(vi.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Finds the visitor by its id and pre-fetch the related data (inventory, visitor lines, friends, etc)
     * @param int $id the id of the visitor to find
     * @param int $hydrationMode the hydration mode used to get the result (use the default HYDRATE ARRAY for performance)
     * @return array|Visitor
     */
    public function findVisitorLoad(int $id, $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT v, f, vl, vi, i
                  
                  FROM Visitor v
                  JOIN v.visitorLines vl
                  JOIN v.friends f
                  JOIN v.visitorItems vi
                  JOIN vi.item i
                  
                  WHERE v.id = :id
                  AND   v.deletedAt IS NULL
                  AND   vi.endDate <= :now
                  AND   "
        )
            ->setParameter('id', $id)
            ->getResult($hydrationMode);
    }
}