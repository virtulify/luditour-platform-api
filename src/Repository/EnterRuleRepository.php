<?php

namespace App\Repository;

use App\Entity\EnterRule;
use App\Entity\MiniGameState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EnterRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnterRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnterRule[]    findAll()
 * @method EnterRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnterRuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EnterRule::class);
    }
}
