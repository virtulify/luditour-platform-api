<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 15-03-19
 * Time: 10:45
 */

namespace App\Repository;


use App\Entity\Experience;
use App\Entity\VisitorLine;
use Doctrine\ORM\EntityRepository;

class VisitorLineRepository extends EntityRepository
{
    /**
     * Finds the active visitor lines (updated a hour ago max)
     * @return mixed
     */
    public function findActive()
    {
        $quarterAgo = new \DateTime();
        $quarterAgo->setTimestamp($quarterAgo->getTimestamp() - 900);

        return $this->createQueryBuilder('vil')
            ->where('vil.lastUpdate >= :aHourAgo')
            ->setParameter('aHourAgo', $quarterAgo)
            ->getQuery()
            ->getResult();
    }

    public function findAppData(VisitorLine $visitorLine, Experience $experience) {

    }
}