<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 15-03-19
 * Time: 11:03
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class VisitorItemRepository extends EntityRepository
{
    /**
     * Counts the visitor items that were activated max an hour ago
     * @return integer
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countFreshActivated()
    {
        $quarterAgo = new \DateTime();
        $quarterAgo->setTimestamp($quarterAgo->getTimestamp() - 900);

        return $this->createQueryBuilder('vii')
            ->select('count(vii.id)')
            ->where('vii.activationDate >= :aHourAgo')
            ->setParameter('aHourAgo', $quarterAgo)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Find the visitor items related to one of the ids specified in the idArray in parameter
     * @param array $idArray the array of ids
     * @return mixed
     */
    public function findSeveralByIds(array $idArray)
    {
        $queryBuilder = $this->createQueryBuilder('vii');
        $queryBuilder->expr()->in('vii.id', $idArray);
        return $queryBuilder->getQuery()->getResult();
    }
}