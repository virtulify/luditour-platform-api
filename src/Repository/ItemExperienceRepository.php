<?php

namespace App\Repository;

use App\Entity\ItemExperience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemExperience|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemExperience|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemExperience[]    findAll()
 * @method ItemExperience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemExperienceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemExperience::class);
    }

    // /**
    //  * @return ItemExperience[] Returns an array of ItemExperience objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ItemExperience
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
