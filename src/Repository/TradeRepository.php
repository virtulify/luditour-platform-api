<?php

namespace App\Repository;

use App\Entity\Trade;
use App\Entity\Visitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Trade|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trade|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trade[]    findAll()
 * @method Trade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Trade::class);
    }

    /**
     * Finds the trades in the "ASKED" state with the specified destination visitor
     * @param Visitor $destination the visitor that is the destination visitor of the ASKED trades
     * @param int $hydrationMode the hydration mode. Keep default "HYDRATE ARRAY" in case of performance issue
     * @return Trade[]|array
     */
    public function findAskedTradesByDestination(Visitor $destination, int $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT t, vi
                  FROM Trade t
                  JOIN t.donations vi
                  WHERE t.destinationVisitor = :destination
                  AND   t.deletedAt IS NULL"
        )
            ->setParameter('destination', $destination)
            ->getResult($hydrationMode);
    }
}
