<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-04-19
 * Time: 18:00
 */

namespace App\Repository;


use App\Entity\Experience;
use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    /**
     * Finds the items related to the specified experience (or without any experience related)
     * @param Experience $experience
     * @param int $hydrationMode
     * @return Item[]|array
     */
    public function findByExperience(Experience $experience, $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->createQueryBuilder('i')
            ->where('i.deletedAt IS NULL')
            ->andWhere(':experience MEMBER OF i.experiences')
            ->setParameter('experience', $experience)
            ->getQuery()
            ->getResult($hydrationMode);
    }
}