<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 07/12/2018
 * Time: 15:55
 */

namespace App\Repository;


use App\Entity\Visitor;
use Doctrine\ORM\EntityRepository;

class ExperienceObjectRepository extends EntityRepository
{

    /**
     * Returns all experience objects near visitor
     * @param Visitor $visitor
     * @return mixed
     */
    public function findNearVisitor(Visitor $visitor)
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('o');

        $qb->join('o.generationRule', 'g')
            ->join('g.experience', 'e')
            ->where('o.endDate >= :now')
            ->andWhere('a.lat >= :minLat')
            ->andWhere('a.lat <= :maxLat')
            ->andWhere('a.lng >= :minLng')
            ->andWhere('a.lng <= :maxLng')
            ->andWhere('j.status >= 1')
            ->andWhere('o.endDate > :now')
            ->setParameters(array(
                'now' => $now,
                'maxLat' => $visitor->getScopeMaxLat(),
                'maxLng' => $visitor->getScopeMaxLong(),
                'minLat' => $visitor->getScopeMinLat(),
                'minLng' => $visitor->getScopeMinLong()
            ));

        return $qb->getQuery()->getResult();
    }

    /**
     * Finds the experience objects near the specified locations
     * @param array $coordinates
     * @return mixed
     */
    public function findNearLocation(array $coordinates)
    {
        return $this->createQueryBuilder('eo')
            ->where('eo.endDate >= :now')
            ->andWhere('eo.latitude >= :minLat')
            ->andWhere('eo.latitude <= :maxLat')
            ->andWhere('eo.longitude >= :minLng')
            ->andWhere('eo.longitude <= :maxLng')
            ->setParameter('now', new \DateTime())
            ->setParameter('maxLat', $coordinates['maxLat'])
            ->setParameter('maxLng', $coordinates['maxLng'])
            ->setParameter('minLat', $coordinates['minLat'])
            ->setParameter('minLng', $coordinates['minLng'])
            ->getQuery()
            ->getResult();
    }

    /**
     * Counts the experience objects that are not ended (so they are active)
     * @return integer
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countActive()
    {
        return $this->createQueryBuilder('eo')
            ->select('count(eo.id)')
            ->where('eo.endDate > :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Finds the experience objects that are not finished.
     * @return mixed
     */
    public function findActive()
    {
        return $this->createQueryBuilder('eo')
            ->where('eo.endDate > :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult();
    }
}