<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-04-19
 * Time: 11:56
 */

namespace App\Repository;


use App\Entity\Spot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Spot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spot[]    findAll()
 * @method Spot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpotRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Spot::class);
    }

    /**
     * Finds the spot that are located in the range good range for the spot
     * @param array $coordinates
     * @param int $experienceId the id of the experience to retrieve
     * @param int $hydrationMode
     * @return Spot[]|array
     */
    public function findNearLocationByExperience(array $coordinates, int $experienceId, int $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->createQueryBuilder('s')
            ->where('s.experience = :experience')
            ->andWhere('s.latitude >= :minLat')
            ->andWhere('s.latitude <= :maxLat')
            ->andWhere('s.longitude >= :minLng')
            ->andWhere('s.longitude <= :maxLng')
            ->setParameter('experience', $experienceId)
            ->setParameter('maxLat', $coordinates['maxLat'])
            ->setParameter('maxLng', $coordinates['maxLng'])
            ->setParameter('minLat', $coordinates['minLat'])
            ->setParameter('minLng', $coordinates['minLng'])
            ->getQuery()
            ->getResult($hydrationMode);
    }
}