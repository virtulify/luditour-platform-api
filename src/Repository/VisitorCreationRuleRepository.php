<?php

namespace App\Repository;

use App\Entity\VisitorCreationRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VisitorCreationRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitorCreationRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitorCreationRule[]    findAll()
 * @method VisitorCreationRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitorCreationRuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VisitorCreationRule::class);
    }

    // /**
    //  * @return VisitorCreationRule[] Returns an array of VisitorCreationRule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VisitorCreationRule
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
