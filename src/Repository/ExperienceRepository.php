<?php /** @noinspection SqlNoDataSourceInspection */

/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 08/11/2018
 * Time: 18:04
 */

namespace App\Repository;


use App\Entity\Experience;
use App\Entity\Visitor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ExperienceRepository extends EntityRepository
{
    /**
     * Returns all experiences near the visitor location
     * @param Visitor $visitor
     *
     * @throws
     *
     * @return array
     */
    public function findNearVisitor(Visitor $visitor)
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('j');

        $andWhere = $qb->expr()->andX();
        $andWhere->add($qb->expr()->lte('e.startDate', ':now'));
        $andWhere->add($qb->expr()->gte('e.endDate', ':now'));

        $orWhere = $qb->expr()->orX();
        $orWhere->add($andWhere);
        $orWhere->add('e.status = ' . Event::EVENT_TYPE_TESTING);

        $qb->join('j.event', 'e')
            ->join('j.address', 'a')
            ->where($orWhere)
            ->andWhere('e.status >= ' . Event::EVENT_TYPE_MANAGEABLE)
            ->andWhere('a.lat >= :minLat')
            ->andWhere('a.lat <= :maxLat')
            ->andWhere('a.lng >= :minLng')
            ->andWhere('a.lng <= :maxLng')
            ->andWhere('j.enabled = 1')
            ->setParameters(array(
                'now' => $now,
                'maxLat' => $coordinates['maxLat'],
                'maxLng' => $coordinates['maxLng'],
                'minLat' => $coordinates['minLat'],
                'minLng' => $coordinates['minLng']
            ));

        /**
         * If the app is white mark > Only get the linked event
         * If the app is the classic Hackeo, don't get the white mark events
         */
        if ($appName != null) {
            $qb->join('e.setting', 's')
                ->andWhere('s.key = :appName')
                ->setParameter('appName', $appName);
        } else {
            $qb->andWhere('e.setting is NULL');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Finds the available experiences (located in the current position or that does not have any position limit
     * @param float $latitude
     * @param float $longitude
     * @param $hydrationMode int the mode of doctrine hydration to retrieve the result from the database (array by default)
     * @return array|Experience[]
     */
    public function findAvailableExperiences(float $latitude, float $longitude, $hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT e, vid, cat, p, lo, ba FROM App\Entity\Experience e 
                  LEFT JOIN e.presentationVideo vid 
                  LEFT JOIN e.categories cat
                  LEFT JOIN e.allowedPositions p
                  LEFT JOIN e.logoMedia lo
                  LEFT JOIN e.bannerMedia ba
                  WHERE e.status = :status
                  AND   e.deletedAt IS NULL
                  AND   
                          (
                              (e.startDate IS NULL OR e.endDate IS NULL)
                              OR
                              (e.startDate <= :now AND e.endDate >= :now)
                          )
                  AND   
                          (
                              e.allowedPositions IS EMPTY 
                              OR 
                              (p.scopeMinLat <= :latitude AND p.scopeMaxLat >= :latitude AND p.scopeMinLong <= :longitude AND p.scopeMaxLong >= :longitude)
                          )"
        )->setParameters([
            'status' => Experience::STATUS_AVAILABLE,
            'now' => new \DateTime(),
            'latitude' => $latitude,
            'longitude' => $longitude
        ])->getResult($hydrationMode);
    }

    /**
     * Finds the public experiences (those that don't need position)
     * @param int $hydrationMode hydration mode to use for the dql query. HYDRATE_ARRAY by default for optimization issues
     * @return Experience[]|array
     */
    public function findPublicExperiences($hydrationMode = Query::HYDRATE_ARRAY)
    {
        return $this->getEntityManager()->createQuery(
            "SELECT e, vid, cat, lo, ba FROM App\Entity\Experience e 
                  LEFT JOIN  e.presentationVideo vid 
                  LEFT JOIN  e.categories cat
                  LEFT JOIN e.logoMedia lo
                  LEFT JOIN e.bannerMedia ba
                  WHERE e.status = :status
                  AND   e.deletedAt IS NULL
                  AND   
                        (
                            (e.startDate IS NULL OR e.endDate IS NULL)
                            OR
                            (e.startDate <= :now AND e.endDate >= :now)
                        )
                  AND   e.allowedPositions IS EMPTY"
        )->setParameters([
            'status' => Experience::STATUS_AVAILABLE,
            'now' => new \DateTime()
        ])->getResult($hydrationMode);
    }
}