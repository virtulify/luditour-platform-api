<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 01-03-19
 * Time: 16:04
 */

namespace App\EntityListener;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\UnitOfWork;

abstract class AbstractOnFlushListener
{
    /**
     * Callback used to react to an update of the entity
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param $entity mixed the entity that is updated.
     */
    protected function onUpdate(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
    }

    /**
     * Callback used to react to an insertion of the entity.
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param $entity mixed the inserted entity.
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
    }

    /**
     * Callback used to react to a deletion of the specified entity.
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param $entity mixed the entity that is deleted.
     */
    protected function onDelete(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
    }

    /**
     * Callback used to react to a soft delete action to the specified entity
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param $entity
     */
    protected function onSoftDelete(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {

    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $entityManager = $args->getEntityManager();
        $uow = $entityManager->getUnitOfWork();

        $supportedEntity = $this->getSupportedEntity();

        foreach ($uow->getScheduledEntityInsertions() as $insertion) {
            if ($insertion instanceof $supportedEntity) {
                $this->onInsert($entityManager, $uow, $insertion);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $update) {
            if ($update instanceof $supportedEntity) {
                $changeSet = $uow->getEntityChangeSet($update);
                if (array_key_exists('deletedAt', $changeSet)) {
                    $this->onSoftDelete($entityManager, $uow, $update);
                } else {
                    $this->onUpdate($entityManager, $uow, $update);
                }
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $deletion) {
            if ($deletion instanceof $supportedEntity) {
                $this->onDelete($entityManager, $uow, $deletion);
            }
        }
    }

    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public abstract function getSupportedEntity();

    protected function triggerChanges(UnitOfWork $unitOfWork, EntityManager $entityManager, $class, $entity): void
    {
        /** @var ClassMetadata $meta */
        $meta = $entityManager->getClassMetadata($class);

        if ($unitOfWork->getEntityChangeSet($entity)) {
            $unitOfWork->recomputeSingleEntityChangeSet($meta, $entity);
        } else {
            $unitOfWork->computeChangeSet($meta, $entity);
        }
    }
}