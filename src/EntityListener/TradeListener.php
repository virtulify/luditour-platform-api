<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 04-04-19
 * Time: 16:02
 */

namespace App\EntityListener;


use App\Entity\Trade;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\UnitOfWork;

class TradeListener extends AbstractOnFlushListener
{

    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public function getSupportedEntity()
    {
        return Trade::class;
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param Trade $entity
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $this->onTradeBegin($entityManager, $uow, $entity);
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param Trade $entity
     * @throws \Doctrine\ORM\ORMException
     */
    protected function onUpdate(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $changeSet = $uow->getEntityChangeSet($entity);
        if (array_key_exists('state', $changeSet) && $changeSet['state'][1] === Trade::STATE_ACCEPTED) {
            $this->onTradeAccepted($entityManager, $uow, $entity);
        }

        if (array_key_exists('state', $changeSet) && $changeSet['state'][1] === Trade::STATE_DENIED) {
            $this->onTradeDenied($entityManager, $uow, $entity);
        }

        // Updates all last update dates
        $entity->setUpdatedAt(new \DateTime());
        $entity->getSourceVisitor()->setLastUpdate(new \DateTime());
        $entity->getDestinationVisitor()->setLastUpdate(new \DateTime());
        $this->triggerChanges($uow, $entityManager, Trade::class, $entity);
        $this->triggerChanges($uow, $entityManager, Visitor::class, $entity->getSourceVisitor());
        $this->triggerChanges($uow, $entityManager, Visitor::class, $entity->getDestinationVisitor());
    }

    /**
     * Callback used when a Trade has been created. The donations leave the source visitor's inventory and
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param Trade $trade
     */
    private function onTradeBegin(EntityManager $entityManager, UnitOfWork $uow, Trade $trade)
    {
        foreach ($trade->getDonations() as $donation) {
            $donation->setVisitor(null);
            $trade->getSourceVisitor()->removeVisitorItem($donation);
            $this->triggerChanges($uow, $entityManager, VisitorItem::class, $donation);
        }
        $this->triggerChanges($uow, $entityManager, Visitor::class, $trade->getSourceVisitor());
    }

    /**
     * Callback used when the trade has been accepted by the destination. The donations will be transferred from the source visitor to the destination visitor
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param Trade $trade
     */
    private function onTradeAccepted(EntityManager $entityManager, UnitOfWork $uow, Trade $trade)
    {
        foreach ($trade->getDonations() as $donation) {
            $donation->setVisitor($trade->getDestinationVisitor());
            $trade->getDestinationVisitor()->addVisitorItem($donation);
            $this->triggerChanges($uow, $entityManager, VisitorItem::class, $donation);
        }
    }

    /**
     * Callback used when the trade has been denied by the destination
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param Trade $trade
     * @throws \Doctrine\ORM\ORMException
     */
    private function onTradeDenied(EntityManager $entityManager, UnitOfWork $uow, Trade $trade)
    {
        foreach ($trade->getDonations() as $donation) {
            $entityManager->remove($donation);
            $this->triggerChanges($uow, $entityManager, VisitorItem::class, $donation);
        }
    }
}