<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 01-03-19
 * Time: 10:23
 */

namespace App\EntityListener;


use App\Entity\Item;
use App\Entity\Visitor;
use App\Entity\VisitorCreationRule;
use App\Entity\VisitorItem;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;

class VisitorCreationRuleListener
{
    /**
     * @param OnFlushEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $entityManager = $args->getEntityManager();
        $uow = $entityManager->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $insertion) {
            if ($insertion instanceof VisitorCreationRule) {
                $this->addNewItemsInAllVisitorLines($entityManager, $uow, $insertion);
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorCreationRule $visitorCreationRule
     * @throws \Doctrine\ORM\ORMException
     */
    private function addNewItemsInAllVisitorLines(EntityManager $entityManager, UnitOfWork $uow, VisitorCreationRule $visitorCreationRule)
    {
        $experienceTeam = $visitorCreationRule->getExperienceTeam();

        /** @var Visitor[] $visitors */
        $visitors = $entityManager->getRepository(Visitor::class)->findVisitorsByExperienceTeam($experienceTeam);
        $item = $visitorCreationRule->getItem();

        foreach ($visitors as $visitor) {
            // add the quantity of new visitor items
            for ($i = 0; $i < $visitorCreationRule->getQuantity(); $i++) {

                /** @var VisitorItem $visitorItem */
                $visitorItem = $this->createVisitorItem($item, $visitor, $visitorCreationRule);
                $visitor->addVisitorItem($visitorItem);
                $entityManager->persist($visitorItem);
                $uow->computeChangeSet($entityManager->getClassMetadata(VisitorItem::class), $visitorItem);
                $uow->computeChangeSet($entityManager->getClassMetadata(Visitor::class), $visitor);
            }
        }
    }

    private function createVisitorItem(Item $item, Visitor $visitor, VisitorCreationRule $visitorCreationRule): VisitorItem
    {
        $visitorItem = new VisitorItem();
        $visitorItem->setItem($item);
        $visitorItem->setUseCount($item->getMultipleUseCount());
        $visitorItem->setVisitor($visitor);
        return $visitorItem;
    }
}