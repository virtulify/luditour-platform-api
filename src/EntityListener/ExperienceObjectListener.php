<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 06-03-19
 * Time: 16:37
 */

namespace App\EntityListener;


use App\Entity\ExperienceObject;
use App\Entity\GenerationRule;
use App\Entity\VisitorLine;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;

class ExperienceObjectListener extends AbstractOnFlushListener
{
    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public function getSupportedEntity()
    {
        return ExperienceObject::class;
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param ExperienceObject $entity
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $this->addScore($entityManager, $uow, $entity);
    }

    /**
     * TODO : change that after laval
     * Adds score when a new experience object is created
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param ExperienceObject $experienceObject
     */
    private function addScore(EntityManager $entityManager, UnitOfWork $uow, ExperienceObject $experienceObject)
    {
//        /** @var GenerationRule $generationRule */
//        $generationRule = $experienceObject->getGenerationRule();
//        $creator = $experienceObject->getCreator();
//        $newScore = $creator->getScore();
//
//        // if simple tree, one point
//        if ($generationRule->getId() === 12 || $generationRule->getId() === 13) {
//            $newScore++;
//        }
//
//        // if mega tree, two points
//        // if simple tree, one point
//        if ($generationRule->getId() === 16 || $generationRule->getId() === 17) {
//            $newScore += 2;
//        }
//        $creator->setScore($newScore);
//        $this->triggerChanges($uow, $entityManager, VisitorLine::class, $creator);
    }
}