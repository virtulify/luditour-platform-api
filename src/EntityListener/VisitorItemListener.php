<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 07-03-19
 * Time: 14:37
 */

namespace App\EntityListener;


use App\Entity\Visitor;
use App\Entity\VisitorItem;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;

class VisitorItemListener extends AbstractOnFlushListener
{

    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public function getSupportedEntity()
    {
        return VisitorItem::class;
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorItem $entity
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $item = $entity->getItem();
        $entity->setEndDate($item->getUsePeriodEnd());
        $this->triggerChanges($uow, $entityManager, VisitorItem::class, $entity);

        // update visitor lastUpdate date
        $entity->getVisitor()->setLastUpdate(new \DateTime());
        $this->triggerChanges($uow, $entityManager, Visitor::class, $entity->getVisitor());
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorItem $entity
     */
    protected function onUpdate(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        // update date
        $entity->setUpdated(new \DateTime());
    }
}