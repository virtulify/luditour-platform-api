<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 03-04-19
 * Time: 16:35
 */

namespace App\EntityListener;


use App\Entity\EndRule;
use App\Entity\EnterRule;
use App\Entity\Item;
use App\Entity\MiniGameState;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use App\Entity\VisitorLine;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MiniGameStateListener extends AbstractOnFlushListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * MiniGameStateListener constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public function getSupportedEntity()
    {
        return MiniGameState::class;
    }

    /**
     * PrePersist entity listener used to update the score of the mini game state according to the end rules of the related mini game
     * @param MiniGameState $miniGameState
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     */
    public function prePersist(MiniGameState $miniGameState, LifecycleEventArgs $args)
    {
        /** @var Visitor $visitor */
        $visitor = $this->tokenStorage->getToken()->getUser();

        $miniGame = $miniGameState->getMiniGame();

        $endRules = $miniGame->getEndRules()->filter(function (EndRule $endRule) {
            return $endRule->getDeletedAt() === null;
        });

        foreach ($endRules as $endRule) {
            // IF the player has won the end rule
            if ($this->isEndRuleTriggered($endRule, $miniGameState)) {
                $miniGameState->setScoreWon($miniGameState->getScoreWon() + $endRule->getScoreWon());

                // Creates and add the visitor items to win to the visitor
                foreach ($endRule->getWonItems() as $wonItem) {
                    dump('scouscou');
                    // insert visitor item
                    $visitorItem = $this->buildVisitorItem($wonItem, $miniGameState);
                    $miniGameState->addItemsWon($visitorItem);
                    $visitor->addVisitorItem($visitorItem);
                    $args->getEntityManager()->persist($visitorItem);
                    $this->triggerChanges($args->getEntityManager()->getUnitOfWork(), $args->getEntityManager(), VisitorItem::class, $visitorItem);
                }
            } // ELSE...
            else {
                $miniGameState->setScoreLost($miniGameState->getScoreLost() + $endRule->getScoreLost());
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param MiniGameState $entity
     * @throws \Doctrine\ORM\ORMException
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $this->consumeItem($entity, $entityManager, $uow); // must be first (consumes required items before losing the others)
        $this->updateVisitorItemsOfVisitorLine($entity, $entityManager, $uow);
        $this->updateScores($entity, $entityManager, $uow);
    }

    /**
     * @param MiniGameState $miniGameState
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @throws \Doctrine\ORM\ORMException
     */
    private function updateVisitorItemsOfVisitorLine(MiniGameState $miniGameState, EntityManager $entityManager, UnitOfWork $uow)
    {
        $miniGame = $miniGameState->getMiniGame();

        $endRules = $miniGame->getEndRules()->filter(function (EndRule $endRule) {
            return $endRule->getDeletedAt() === null;
        });

        /** @var EndRule $endRule */
        foreach ($endRules as $endRule) {
            // IF the score performed is not enough to win
            if (!$this->isEndRuleTriggered($endRule, $miniGameState)) {
                // Selects and removes the visitor items of the visitor related to the lost items
                foreach ($endRule->getLostItems() as $lostItem) {
                    $visitorItemToDelete = $entityManager->getRepository(VisitorItem::class)->findOneBy([
                        'deletedAt' => null,
                        'item' => $lostItem,
                        'visitor' => $this->tokenStorage->getToken()->getUser()
                    ]);

                    if (!$visitorItemToDelete) {
                        continue;
                    }

                    $visitorItemToDelete->setLosingMiniGameState($miniGameState);
                    $visitorItemToDelete->setDeletedAt(new \DateTime()); // remove visitor item without telling the entity manager to prevent an exception
                    $visitorItemToDelete->setVisitor(null);
                    $this->triggerChanges($uow, $entityManager, VisitorItem::class, $visitorItemToDelete);
                }
            } // ELSE...
        }
    }

    /**
     * Updates the scores of the mini game state and the player (visitor line)
     * @param MiniGameState $miniGameState
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     */
    private function updateScores(MiniGameState $miniGameState, EntityManager $entityManager, UnitOfWork $uow)
    {
        $visitorLine = $miniGameState->getPlayer();
        $miniGame = $miniGameState->getMiniGame();

        /** @var EndRule[] $endRules the active end rules */
        $endRules = $miniGame->getEndRules()->filter(function (EndRule $endRule) {
            return $endRule->getDeletedAt() === null;
        })->getValues();

        foreach ($endRules as $endRule) {
            // IF the player has won the end rule, add the score to win defined in the end rule for the visitor line and its team
            if ($this->isEndRuleTriggered($endRule, $miniGameState)) {
                $visitorLine->setScore($visitorLine->getScore() + $endRule->getScoreWon());
                $visitorLine->getExperienceTeam()->setScore($visitorLine->getExperienceTeam()->getScore() + $endRule->getScoreWon());
                $visitorLine->getExperienceTeam()->getTeam()->setScore($visitorLine->getExperienceTeam()->getTeam()->getScore() + $endRule->getScoreWon());
            } // ELSE, substract with the score lost
            else {
                $visitorLine->setScore($visitorLine->getScore() - $endRule->getScoreLost());
                $visitorLine->getExperienceTeam()->setScore($visitorLine->getExperienceTeam()->getScore() - $endRule->getScoreLost());
                $visitorLine->getExperienceTeam()->getTeam()->setScore($visitorLine->getExperienceTeam()->getTeam()->getScore() - $endRule->getScoreLost());
            }
        }

        $this->triggerChanges($uow, $entityManager, VisitorLine::class, $visitorLine);
    }

    /**
     * Consumes the items required by the enter rules of the mini game
     * @param MiniGameState $miniGameState
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     */
    private function consumeItem(MiniGameState $miniGameState, EntityManager $entityManager, UnitOfWork $uow)
    {
        /** @var EnterRule[] $enterRules the active enter rules */
        $enterRules = $miniGameState->getMiniGame()->getEnterRules()->filter(function (EnterRule $enterRule) {
            return !$enterRule->getDeletedAt();
        })->getValues();

        foreach ($enterRules as $enterRule) {
            /** @var Visitor $visitor */
            $visitor = $this->tokenStorage->getToken()->getUser();
            if ($enterRule->getIsItemConsumed() && $enterRule->getConditionType() === EnterRule::CONDITION_ITEM_REQUIRED) {
                $visitorItems = $entityManager->getRepository(VisitorItem::class)->findBy([
                    'visitor' => $visitor,
                    'deletedAt' => null,
                    'item' => $enterRule->getRequiredItem(),
                ], null, $enterRule->getRequiredQuantity());

                foreach ($visitorItems as $visitorItem) {
                    $visitor->removeVisitorItem($visitorItem);
                    $visitor->setLastUpdate(new \DateTime());
                    $miniGameState->addItemsConsumed($visitorItem);

                    $this->triggerChanges($uow, $entityManager, VisitorItem::class, $visitorItem);
                    $this->triggerChanges($uow, $entityManager, Visitor::class, $visitor);
                }
            }
        }
    }

    /**
     * Builds a visitor item based on the mini game state and the item to add in the visitor's inventory
     * @param Item $item
     * @param MiniGameState $miniGameState
     * @return VisitorItem the build visitorItem
     */
    private function buildVisitorItem(Item $item, MiniGameState $miniGameState)
    {
        $visitorItem = new VisitorItem();
        /** @var Visitor $visitor */
        $visitor = $this->tokenStorage->getToken()->getUser();
        $visitorItem->setVisitor($visitor);
        $visitorItem->setItem($item);
        $visitorItem->setWinningMiniGameState($miniGameState);
        $visitorItem->setEndDate($item->getUsePeriodEnd());

        return $visitorItem;
    }

    /**
     * Determines whether the end rule is triggered by the new mini game state
     * @param EndRule $endRule
     * @param MiniGameState $miniGameState
     * @return bool true if the end rule is triggered
     */
    private function isEndRuleTriggered(EndRule $endRule, MiniGameState $miniGameState)
    {
        switch ($endRule->getConditionType()) {
            case EndRule::CONDITION_EQ:
                return $endRule->getScoreCondition() === $miniGameState->getScorePerformed();
            case EndRule::CONDITION_GT:
                return $endRule->getScoreCondition() < $miniGameState->getScorePerformed();
            case EndRule::CONDITION_GTE:
                return $endRule->getScoreCondition() <= $miniGameState->getScorePerformed();
            case EndRule::CONDITION_LT:
                return $endRule->getScoreCondition() > $miniGameState->getScorePerformed();
            case EndRule::CONDITION_LTE:
                return $endRule->getScoreCondition() >= $miniGameState->getScorePerformed();
            default:
                return false;
        }
    }
}