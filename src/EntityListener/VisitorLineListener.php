<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 01-03-19
 * Time: 15:41
 */

namespace App\EntityListener;


use App\Entity\Experience;
use App\Entity\ExperienceTeam;
use App\Entity\Team;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use App\Entity\VisitorLine;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;

class VisitorLineListener extends AbstractOnFlushListener
{
    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorLine $entity
     * @throws \Doctrine\ORM\ORMException
     */
    protected function onInsert(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $this->updateVisitCount($entityManager, $uow, $entity);
        if ($entity->getScore() !== 0) {
            $this->updateScoreAggregates($entityManager, $uow, $entity, $entity->getScore());
        }

        if ($entity->getKilometersWalked() !== 0) {
            $this->updateKilometersWalked($entityManager, $uow, $entity, $entity->getKilometersWalked());
        }

        $this->applyVisitorCreationRule($entityManager, $uow, $entity);

        // update visitor lastUpdate date
        $entity->getVisitor()->setLastUpdate(new \DateTime());
        $this->triggerChanges($uow, $entityManager, Visitor::class, $entity->getVisitor());
    }

    /**
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorLine $entity
     */
    protected function onUpdate(EntityManager $entityManager, UnitOfWork $uow, $entity)
    {
        $changeSet = $uow->getEntityChangeSet($entity);
        if (array_key_exists('score', $changeSet)) {
            $scoreAdded = $changeSet['score'][1] - $changeSet['score'][0];
            if ($scoreAdded !== 0) {
                $this->updateScoreAggregates($entityManager, $uow, $entity, $scoreAdded);
            }
        }

        if (array_key_exists('kilometersWalked', $changeSet)) {
            $kilometersAdded = $changeSet['kilometersWalked'][1] - $changeSet['kilometersWalked'][0];
            if ($kilometersAdded !== 0) {
                $this->updateKilometersWalked($entityManager, $uow, $entity, $kilometersAdded);
            }
        }

        // Add the new gametime with the old one.
        if (array_key_exists('gameTime', $changeSet)) {
            $newGameTime = $changeSet['gameTime'][0] + $changeSet['gameTime'][1];
            $entity->setGameTime($newGameTime);
        }

        // update the kilometers walked based on the new position
        if (array_key_exists('currentLatitude', $changeSet)
            && array_key_exists('currentLongitude', $changeSet)
            && isset($changeSet['currentLatitude'][0])
            && isset($changeSet['currentLongitude'][0])) {

            $kilometersToAdd = $this->computeDistance($changeSet['currentLatitude'][0], $changeSet['currentLongitude'][0], $changeSet['currentLatitude'][1], $changeSet['currentLongitude'][1]);
            $entity->setKilometersWalked($entity->getKilometersWalked() + $kilometersToAdd);
        }

        $entity->setLastUpdate(new \DateTime());

        // update visitor lastUpdate date
        $entity->getVisitor()->setLastUpdate(new \DateTime());
        $this->triggerChanges($uow, $entityManager, Visitor::class, $entity->getVisitor());
    }

    /**
     * Adds the items related to the creation rules of the experience team that the visitor joined.
     * @param EntityManager $entityManager
     * @param UnitOfWork $uow
     * @param VisitorLine $visitorLine
     * @throws \Doctrine\ORM\ORMException
     */
    private function applyVisitorCreationRule(EntityManager $entityManager, UnitOfWork $uow, VisitorLine $visitorLine)
    {
        $experienceTeam = $visitorLine->getExperienceTeam();
        foreach ($experienceTeam->getVisitorCreationRules() as $visitorCreationRule) {
            for ($i = 0; $i < $visitorCreationRule->getQuantity(); $i++) {
                $visitorItem = new VisitorItem();
                $visitorItem->setVisitor($visitorLine->getVisitor());
                $visitorItem->setItem($visitorCreationRule->getItem());

                // insert the new visitor item
                $entityManager->persist($visitorItem);
                $this->triggerChanges($uow, $entityManager, VisitorItem::class, $visitorItem);
            }
        }
    }

    private function updateScoreAggregates(EntityManager $entityManager, UnitOfWork $uow, VisitorLine $visitorLine, int $scoreAdded)
    {
        // update score of the experience team
        /** @var ExperienceTeam $experienceTeam */
        $experienceTeam = $visitorLine->getExperienceTeam();
        $experienceTeam->setScore($experienceTeam->getScore() + $scoreAdded);
        $this->triggerChanges($uow, $entityManager, ExperienceTeam::class, $experienceTeam);

        // update score of the team
        /** @var Team $team */
        $team = $experienceTeam->getTeam();
        $team->setScore($team->getScore() + $scoreAdded);
        $this->triggerChanges($uow, $entityManager, Team::class, $team);
    }

    private function updateVisitCount(EntityManager $entityManager, UnitOfWork $uow, VisitorLine $visitorLine)
    {
        // update visitor count of the experience team
        /** @var ExperienceTeam $experienceTeam */
        $experienceTeam = $visitorLine->getExperienceTeam();
        $experienceTeam->setVisitorCount($experienceTeam->getVisitorCount() + 1);
        $this->triggerChanges($uow, $entityManager, ExperienceTeam::class, $experienceTeam);

        // update visitor count of the team
        /** @var Team $team */
        $team = $experienceTeam->getTeam();
        $team->setVisitorCount($team->getVisitorCount() + 1);
        $this->triggerChanges($uow, $entityManager, Team::class, $team);

        // update visitor count of the experience
        $experience = $experienceTeam->getExperience();
        $experience->setVisitorCount($experience->getVisitorCount() + 1);
        $this->triggerChanges($uow, $entityManager, Experience::class, $experience);
    }

    private function updateKilometersWalked(EntityManager $entityManager, UnitOfWork $uow, VisitorLine $visitorLine, float $kilometersAdded)
    {
        // update kilometers walked of the experience team
        /** @var ExperienceTeam $experienceTeam */
        $experienceTeam = $visitorLine->getExperienceTeam();
        $experienceTeam->setKilometersWalked($experienceTeam->getKilometersWalked() + $kilometersAdded);
        $this->triggerChanges($uow, $entityManager, ExperienceTeam::class, $experienceTeam);

        // update kilometers walked of the visitor
        /** @var Visitor $visitor */
        $visitor = $visitorLine->getVisitor();
        $visitor->setKilometersWalked($visitor->getKilometersWalked() + $kilometersAdded);
        $this->triggerChanges($uow, $entityManager, Visitor::class, $visitor);
    }

    /**
     * Calculates the distance between two gps coordinates (lat/long)
     *
     * @param float $lat1 the latitude of the first point
     * @param float $lon1 the latitude of the first point
     * @param float $lat2 the latitude of the first point
     * @param float $lon2 the latitude of the first point
     * @param string $unit the unit used to compute the distance
     * @return float the distance between the two coordinates in the unit in parameter
     */
    private function computeDistance(float $lat1, float $lon1, float $lat2, float $lon2, string $unit = "K"): float
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * @return mixed the class name of the entity that is supported by the OnFlush listener.
     */
    public function getSupportedEntity()
    {
        return VisitorLine::class;
    }
}