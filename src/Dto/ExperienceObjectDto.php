<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-04-19
 * Time: 15:48
 */

namespace App\Dto;


final class ExperienceObjectDto
{
    /**
     * @var int
     */
    public $experienceObjectId;

    /**
     * @var int
     */
    public $generationRuleId;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var float
     */
    public $altitude;

    /**
     * @var string
     */
    public $mapAppearanceMedia;

    /**
     * @var string
     */
    public $actionAppearanceMedia;

    /**
     * ExperienceObjectDto constructor.
     * @param int $experienceObjectId
     * @param int $generationRuleId
     * @param float $latitude
     * @param float $longitude
     * @param float $altitude
     * @param string $mapAppearanceMedia
     * @param string $actionAppearanceMedia
     */
    public function __construct(int $experienceObjectId, int $generationRuleId, float $latitude, float $longitude, float $altitude, string $mapAppearanceMedia, string $actionAppearanceMedia)
    {
        $this->experienceObjectId = $experienceObjectId;
        $this->generationRuleId = $generationRuleId;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->altitude = $altitude;
        $this->mapAppearanceMedia = $mapAppearanceMedia;
        $this->actionAppearanceMedia = $actionAppearanceMedia;
    }
}