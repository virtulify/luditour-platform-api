<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 06-03-19
 * Time: 15:23
 */

namespace App\Dto;


use App\Entity\AssetBundleMedia;

final class AssetBundleMediaOutput
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $androidMedia;

    /**
     * @var string
     */
    public $iosMedia;

    /**
     * @var string
     */
    public $editorMedia;

    public function __construct(AssetBundleMedia $assetBundleMedia)
    {
        $this->id = $assetBundleMedia->getId();
        $this->name = $assetBundleMedia->getName();
        $this->description = $assetBundleMedia->getDescription();
        $this->androidMedia = $assetBundleMedia->getAndroidMedia() ? $assetBundleMedia->getAndroidMedia()->getWebPath() : null;
        $this->iosMedia = $assetBundleMedia->getIosMedia() ? $assetBundleMedia->getIosMedia()->getWebPath() : null;
        $this->editorMedia = $assetBundleMedia->getEditorMedia() ? $assetBundleMedia->getEditorMedia()->getWebPath() : null;
    }
}