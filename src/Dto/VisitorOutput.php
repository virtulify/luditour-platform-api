<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 07-03-19
 * Time: 17:26
 */

namespace App\Dto;


class VisitorOutput
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;
}