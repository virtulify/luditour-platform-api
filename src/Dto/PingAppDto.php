<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 29-03-19
 * Time: 11:53
 */

namespace App\Dto;


use App\Exception\BadRequestException;

final class PingAppDto
{
    /**
     * @var int
     */
    public $score;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var int
     */
    public $gameTime;

    /**
     * PingAppDto constructor.
     * @param $content
     */
    public function __construct($content)
    {
        if (!$content || !is_array($content)) {
            throw new BadRequestException("Invalid or no content provided.");
        }

        $this->score = array_search('score', $content);
        $this->longitude = array_search('longitude', $content);
        $this->latitude = array_search('latitude', $content);
        $this->gameTime = array_search('gameTime', $content);

        $isValid = is_int($this->score) && is_float($this->longitude) && is_float($this->latitude) && is_int($this->gameTime) && $this->gameTime >= 0;
        if (!$isValid) {
            throw new BadRequestException("Content parameters not valid.");
        }
    }
}