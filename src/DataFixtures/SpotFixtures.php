<?php

namespace App\DataFixtures;

use App\Entity\Spot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SpotFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create spot1
        $spot1 = new Spot();
        $spot1->setName('Spot 1');
        $spot1->setExperience($this->getReference('experience-greenesis'));
        $spot1->setType(1); // fake parameter TODO change it
        $spot1->setAltitude(0);
        $spot1->setLatitude(69);
        $spot1->setLongitude(69);
        $spot1->setPrivate(false);
        $spot1->addMiniGame($this->getReference('minigame-catch'));
        $spot1->setRange(10);
        $manager->persist($spot1);

        // Create spot2
        $spot2 = new Spot();
        $spot2->setName('Spot 2');
        $spot2->setExperience($this->getReference('experience-greenesis'));
        $spot2->setType(1); // fake parameter TODO change it
        $spot2->setAltitude(0);
        $spot2->setLatitude(70);
        $spot2->setLongitude(70);
        $spot2->setPrivate(false);
        $spot2->addMiniGame($this->getReference('minigame-catch'));
        $spot2->setRange(10);
        $manager->persist($spot2);

        // End
        $manager->flush();
        $this->addReference('spot-spot1', $spot1);
        $this->addReference('spot-spot2', $spot2);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class,
            MiniGameFixtures::class
        ];
    }
}
