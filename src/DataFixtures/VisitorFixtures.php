<?php

namespace App\DataFixtures;

use App\Entity\Visitor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VisitorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create the visitor used in test
        $test = new Visitor();
        $test->setGoogleAuth('Scou scou banlieue sale');
        $test->setMail('bendo.du69@skynet.be');
        $test->setName('Bendu');
        $test->setBirthDate(new \DateTime('1997-04-01'));
        $manager->persist($test);

        // Create the second visitor (friend of the test visitor)
        $second = new Visitor();
        $second->setFacebookAuth('glouglou');
        $second->setMail('second.lol@gmail.com');
        $second->addFriend($test);
        $test->addFriend($second);
        $manager->persist($second);

        $manager->flush();

        $this->addReference('visitor-test', $test);
        $this->addReference('visitor-second', $second);
    }
}
