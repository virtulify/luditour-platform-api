<?php

namespace App\DataFixtures;

use App\Entity\ExperienceTeam;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ExperienceTeamFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Stellarion for Greenesis
        $stellarion = new ExperienceTeam();
        $stellarion->setExperience($this->getReference('experience-greenesis'));
        $stellarion->setTeam($this->getReference('team-stellarion'));
        $manager->persist($stellarion);

        // Ancestrial for Greenesis
        $ancestrial = new ExperienceTeam();
        $ancestrial->setExperience($this->getReference('experience-greenesis'));
        $ancestrial->setTeam($this->getReference('team-ancestrial'));
        $manager->persist($ancestrial);

        // end
        $manager->flush();
        $this->addReference('experienceteam-stellarion', $stellarion);
        $this->addReference('experienceteam-ancestrial', $ancestrial);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class,
            TeamFixtures::class
        ];
    }
}
