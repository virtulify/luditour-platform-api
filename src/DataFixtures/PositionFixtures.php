<?php

namespace App\DataFixtures;

use App\Entity\Position;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PositionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create position 1 for greenesis
        $position1 = new Position();
        $position1->setLongitude(69.69);
        $position1->setLatitude(69.69);
        $position1->setAltitude(0);
        $position1->setScopeMinLat(69);
        $position1->setScopeMaxLat(70);
        $position1->setScopeMinLong(69);
        $position1->setScopeMaxLong(70);
        $position1->setHashCode(uniqid());
        $manager->persist($position1);

        // Create position 2 for greenesis
        $position2 = new Position();
        $position2->setLongitude(42.42);
        $position2->setLatitude(42.42);
        $position2->setAltitude(0);
        $position2->setScopeMinLat(42);
        $position2->setScopeMaxLat(43);
        $position2->setScopeMinLong(42);
        $position2->setScopeMaxLong(43);
        $position2->setHashCode(uniqid());
        $manager->persist($position2);
        $manager->flush();
        $this->addReference('position-greenesis1', $position1);
        $this->addReference('position-greenesis2', $position2);
    }
}
