<?php

namespace App\DataFixtures;

use App\Entity\EndRule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EndRuleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create end rule for catch mini game (lose 100 points if you don't reach 0 score performed)
        $lost = new EndRule();
        $lost->setMiniGame($this->getReference('minigame-catch'));
        $lost->setScoreLost(100);
        $lost->setScoreCondition(0);
        $lost->setConditionType(EndRule::CONDITION_EQ);
        $lost->addLostItem($this->getReference('item-stellagraine'));
        $manager->persist($lost);

        // Create end rule for catch mini game (win 1000 score when more than 100 score)
        $win = new EndRule();
        $win->setMiniGame($this->getReference('minigame-catch'));
        $win->setScoreWon(1000);
        $win->setScoreCondition(1000);
        $win->setConditionType(EndRule::CONDITION_GTE);
        $win->addWonItem($this->getReference('item-coin'));
        $manager->persist($win);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MiniGameFixtures::class,
            ItemFixtures::class
        ];
    }
}
