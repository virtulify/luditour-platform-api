<?php

namespace App\DataFixtures;

use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create stellarion team
        $stellarion = new Team();
        $stellarion->setDescription('Équipe des Stellarions');
        $stellarion->setName('Stellarion');
        $manager->persist($stellarion);

        // Create ancestrial team
        $ancestrial = new Team();
        $ancestrial->setDescription('Équipe des Ancestrials');
        $ancestrial->setName('Ancestrial');
        $manager->persist($ancestrial);

        $manager->flush();
        $this->addReference('team-stellarion', $stellarion);
        $this->addReference('team-ancestrial', $ancestrial);
    }
}
