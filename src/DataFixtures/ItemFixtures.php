<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create stellagraine (linked to Experience greenesis)
        $stellagraine = new Item();
        $stellagraine->setName('Stellagraine');
        $stellagraine->setDescription('Graine originaire de la planète des Stellarions');
        $stellagraine->setIsTradeable(true);
        $stellagraine->setIsExperienceOnly(true);
        $stellagraine->addExperience($this->getReference('experience-greenesis'));

        $image = new Media();
        $image->setFilename('asb://stellagraine-image');
        $image->setDiskUsage(10);
        $manager->persist($image);
        $stellagraine->setAppearanceImageMedia($image);

        $object = new Media();
        $object->setFilename('asb://stellagraine-object');
        $object->setDiskUsage(10);
        $manager->persist($object);
        $stellagraine->setAppearanceObjectMedia($object);

        $manager->persist($stellagraine);


        // Create ancestrigraine
        $ancestrigraine = new Item();
        $ancestrigraine->setName('Ancestrigraine');
        $ancestrigraine->setDescription('Graine originaire de la tribu Ancestrial');
        $ancestrigraine->setIsTradeable(true);
        $ancestrigraine->setIsExperienceOnly(true);
        $ancestrigraine->addExperience($this->getReference('experience-greenesis'));

        $image = new Media();
        $image->setFilename('asb://ancestrigraine-image');
        $image->setDiskUsage(10);
        $manager->persist($image);
        $ancestrigraine->setAppearanceImageMedia($image);

        $object = new Media();
        $object->setFilename('asb://ancestrigraine-object');
        $object->setDiskUsage(10);
        $manager->persist($object);
        $ancestrigraine->setAppearanceObjectMedia($object);

        $manager->persist($ancestrigraine);


        // Create coin
        $coin = new Item();
        $coin->setName('Pièce');
        $coin->setDescription('Pièce faite pour dépenser');
        $coin->setIsTradeable(true);
        $coin->setIsExperienceOnly(false);

        $image = new Media();
        $image->setFilename('asb://coin-image');
        $image->setDiskUsage(10);
        $manager->persist($image);
        $coin->setAppearanceImageMedia($image);

        $object = new Media();
        $object->setFilename('asb://coin-object');
        $object->setDiskUsage(10);
        $manager->persist($object);
        $coin->setAppearanceObjectMedia($object);

        $manager->persist($coin);


        // Create slip
        $underpants = new Item();
        $underpants->setName('Caleçon');
        $underpants->setDescription('Sous-vêtement très important à garder privé À TOUT PRIX !!!');
        $underpants->setIsTradeable(false);
        $underpants->setIsExperienceOnly(false);

        $image = new Media();
        $image->setFilename('asb://underpants-image');
        $image->setDiskUsage(10);
        $manager->persist($image);
        $underpants->setAppearanceImageMedia($image);

        $object = new Media();
        $object->setFilename('asb://underpants-object');
        $object->setDiskUsage(10);
        $manager->persist($object);
        $underpants->setAppearanceObjectMedia($object);

        $manager->persist($underpants);

        // end
        $manager->flush();
        $this->addReference('item-stellagraine', $stellagraine);
        $this->addReference('item-ancestrigraine', $ancestrigraine);
        $this->addReference('item-coin', $coin);
        $this->addReference('item-underpants', $underpants);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class
        ];
    }
}
