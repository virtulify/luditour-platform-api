<?php

namespace App\DataFixtures;

use App\Entity\Quest;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class QuestFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create quest (car race)
        $race = new Quest();
        $race->setType(1); // fake type not used for now
        $race->setStartDate(new \DateTime('2013-03-30'));
        $race->setEndDate(new \DateTime('2050-03-30'));
        $race->setExperience($this->getReference('experience-greenesis'));
        $race->setPeriodType(0); // todo fake data go change that
        $manager->persist($race);

        // Create quest outdated
        $outdated = new Quest();
        $outdated->setStartDate(new \DateTime('2013-03-1'));
        $outdated->setEndDate(new \DateTime('2013-03-30'));
        $outdated->setExperience($this->getReference('experience-greenesis'));
        $outdated->setPeriodType(0); // todo fake data change that
        $manager->persist($outdated);

        // End
        $manager->flush();
        $this->addReference('quest-race', $race);
        $this->addReference('quest-outdated', $outdated);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class
        ];
    }
}
