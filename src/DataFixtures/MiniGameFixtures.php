<?php

namespace App\DataFixtures;

use App\Entity\Media;
use App\Entity\MiniGame;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MiniGameFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create catch minigame for Greenesis experience
        $catch = new MiniGame();
        $catch->setType(1);
        $catch->setExperience($this->getReference('experience-greenesis'));
        $catch->setName('Chat'); // Je n'ai pas d'idée sorry
        $catch->setDescription('Course pour rattraper la graine magique');
        $arMedia = new Media();
        $arMedia->setDiskUsage(10);
        $arMedia->setFilename('asb://greenesis-catch');
        $manager->persist($arMedia);
        $catch->setArMedia($arMedia);
        $manager->persist($catch);

        // end
        $manager->flush();
        $this->addReference('minigame-catch', $catch);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class
        ];
    }
}
