<?php

namespace App\DataFixtures;

use App\Entity\ExperienceTeam;
use App\Entity\VisitorLine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class VisitorLineFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create visitor line for visitor test in greenesis (he is a Stellarion !)
        $test = new VisitorLine();
        $test->setVisitor($this->getReference('visitor-test'));
        $test->setRegistrationAltitude(0);
        $test->setRegistrationDate(new \DateTime());
        $test->setScore(1500);
        $test->setRegistrationLatitude(69);
        $test->setRegistrationLongitude(69);
        $test->setExperienceTeam($this->getReference('experienceteam-stellarion'));
        $test->setCurrentLatitude(69);
        $test->setCurrentLongitude(69);
        $test->setCurrentAltitude(0);
        $test->setOpinion("J'adore Greenesis et Sami est un développeur génial");
        $test->setRating(5);
        $manager->persist($test);

        // Create visitor line for visitor second in greenesis (he is an Ancestrial)
        $second = new VisitorLine();
        $second->setVisitor($this->getReference('visitor-second'));
        $second->setRegistrationAltitude(0);
        $second->setRegistrationDate(new \DateTime());
        $second->setScore(1000);
        $second->setRegistrationLatitude(69);
        $second->setRegistrationLongitude(69);
        $second->setExperienceTeam($this->getReference('experienceteam-ancestrial'));
        $second->setCurrentLatitude(69);
        $second->setCurrentLongitude(69);
        $second->setCurrentAltitude(0);
        $second->setOpinion("J'adore Greenesis et Hootside est une application qui ne bugue jamais");
        $second->setRating(5);
        $manager->persist($second);

        // end
        $manager->flush();
        $this->addReference('visitorline-test', $test);
        $this->addReference('visitorline-second', $second);
    }

    public function getDependencies()
    {
        return [
            VisitorFixtures::class,
            ExperienceFixtures::class,
            ExperienceTeamFixtures::class,
            TeamFixtures::class
        ];
    }
}
