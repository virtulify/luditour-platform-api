<?php

namespace App\DataFixtures;

use App\Entity\EnterRule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EnterRuleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create enter rule with no condition for catch mini-game
        $none = new EnterRule();
        $none->setMiniGame($this->getReference('minigame-catch'));
        $none->setConditionType(EnterRule::CONDITION_NONE);
        $none->setIsItemConsumed(false);
        $manager->persist($none);

        // Create enter rule with item required for catch mini-game
        //      needs 3 coins to validate
        $itemRequired = new EnterRule();
        $itemRequired->setConditionType(EnterRule::CONDITION_ITEM_REQUIRED);
        $itemRequired->setIsItemConsumed(true); // consumes item
        $itemRequired->setRequiredQuantity(3);
        $itemRequired->setMiniGame($this->getReference('minigame-catch'));
        $itemRequired->setRequiredItem($this->getReference('item-coin'));
        $manager->persist($itemRequired);

        // Create enter rule with done once for catch mini-game
        $once = new EnterRule();
        $once->setConditionType(EnterRule::CONDITION_DONE_ONCE);
        $once->setMiniGame($this->getReference('minigame-catch'));
        $manager->persist($once);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MiniGameFixtures::class,
            ItemFixtures::class
        ];
    }
}
