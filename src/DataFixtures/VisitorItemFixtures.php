<?php

namespace App\DataFixtures;

use App\Entity\Visitor;
use App\Entity\VisitorItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class VisitorItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create stellagraine for test visitor
        $stellagraine = new VisitorItem();
        $stellagraine->setItem($this->getReference('item-stellagraine'));
        $stellagraine->setVisitor($this->getReference('visitor-test'));
        $manager->persist($stellagraine);

        // Create coin for test visitor
        $coin = new VisitorItem();
        $coin->setItem($this->getReference('item-coin'));
        $coin->setVisitor($this->getReference('visitor-test'));
        $manager->persist($coin);

        // Create second coin for test visitor
        $coin = new VisitorItem();
        $coin->setItem($this->getReference('item-coin'));
        $coin->setVisitor($this->getReference('visitor-test'));
        $manager->persist($coin);

        // Create third coin for test visitor
        $coin = new VisitorItem();
        $coin->setItem($this->getReference('item-coin'));
        $coin->setVisitor($this->getReference('visitor-test'));
        $manager->persist($coin);

        // End
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            VisitorFixtures::class,
            ItemFixtures::class
        ];
    }
}
