<?php

namespace App\DataFixtures;

use App\Entity\AssetBundleMedia;
use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AssetBundleMediaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create asset bundle for greenesis
        $greenesis = new AssetBundleMedia();
        $greenesis->setName('Greenesis bundle');
        $greenesis->setDescription('Asset bundle utilisé pour l\'expérience Greenesis');
        $media = new Media();
        $media->setFilename('GreenesisAssetBundleMedia');
        $media->setDiskUsage(10);
        $manager->persist($media);
        $greenesis->setAndroidMedia($media);
        $greenesis->setIosMedia($media);
        $greenesis->setEditorMedia($media);
        $manager->persist($greenesis);

        $manager->flush();
        $this->addReference('assetbundle-greenesis', $greenesis);
    }
}
