<?php

namespace App\DataFixtures;

use App\Entity\Setting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SettingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Create general setting
        $general = new Setting();
        $general->setName('General');
        $general->setKey('lang');
        $general->setValue('FR');
        $manager->persist($general);

        // Create greenesis setting
        $greenesis = new Setting();
        $greenesis->setName('Setting pour Greenesis');
        $greenesis->setKey('lang');
        $greenesis->setValue('FR');
        $greenesis->setExperience($this->getReference('experience-greenesis'));
        $manager->persist($greenesis);

        // end
        $manager->flush();
        $this->addReference('setting-general', $general);
        $this->addReference('setting-greenesis', $greenesis);
    }

    public function getDependencies()
    {
        return [
            ExperienceFixtures::class
        ];
    }
}
