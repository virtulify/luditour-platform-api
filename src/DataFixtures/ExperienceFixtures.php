<?php

namespace App\DataFixtures;

use App\Entity\Experience;
use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ExperienceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Greenesis creation
        $greenesis = new Experience();
        $greenesis->setName('Greenesis');
        $greenesis->addCategory($this->getReference('category-fun'));
        $greenesis->addCategory($this->getReference('category-action'));
        $greenesis->setShortDescription('Événement qui s\'appelle Greenesis.');
        $greenesis->setLongDescription('Événement qui s\'appelle Greenesis.');

        $logo = new Media();
        $logo->setFilename('asb://greenesis-logo');
        $logo->setDiskUsage(10);
        $manager->persist($logo);
        $greenesis->setLogoMedia($logo);

        $video = new Media();
        $video->setFilename('asb://greenesis-video');
        $video->setDiskUsage(10);
        $manager->persist($video);
        $greenesis->setPresentationVideo($video);

        $banner = new Media();
        $banner->setFilename('asb://greenesis-banner');
        $banner->setDiskUsage(10);
        $manager->persist($banner);
        $greenesis->setBannerMedia($banner);

        $greenesis->setWorldPackageMedia($this->getReference('assetbundle-greenesis'));
        $greenesis->addAllowedPosition($this->getReference('position-greenesis1'));
        $greenesis->addAllowedPosition($this->getReference('position-greenesis2'));
        $manager->persist($greenesis);


        // Warhammer creation (outdated)
        $warhammer = new Experience();
        $warhammer->setName('Warhammer');
        $warhammer->setStartDate(new \DateTime('2000-01-01'));
        $warhammer->setEndDate(new \DateTime('2000-01-02'));
        $warhammer->setShortDescription('Événement qui s\'appelle Warhammer.');
        $warhammer->setLongDescription('Événement qui s\'appelle Warhammer.');
        $warhammer->addCategory($this->getReference('category-action'));
        $manager->persist($warhammer);


        // Miraculous creation (public)
        $miraculous = new Experience();
        $miraculous->setName('Miraculous');
        $miraculous->setShortDescription('Événement qui s\'appelle Miraculous.');
        $miraculous->setLongDescription('Événement qui s\'appelle Miraculous.');
        $miraculous->addCategory($this->getReference('category-fun'));
        $manager->persist($miraculous);

        // finish
        $manager->flush();
        $this->addReference('experience-greenesis', $greenesis);
        $this->addReference('experience-warhammer', $warhammer);
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            AssetBundleMediaFixtures::class,
            PositionFixtures::class
        ];
    }
}
