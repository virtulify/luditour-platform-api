<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create action category
        $action = new Category();
        $action->setLabel('Action');
        $manager->persist($action);

        // create fun category
        $fun = new Category();
        $fun->setLabel('Fun');
        $manager->persist($fun);

        // end of load
        $manager->flush();
        $this->addReference('category-action', $action);
        $this->addReference('category-fun', $fun);
    }
}
