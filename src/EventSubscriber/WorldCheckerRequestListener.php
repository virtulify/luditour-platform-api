<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 03/12/2018
 * Time: 12:01
 */

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Visitor;
use App\Manager\MapManager;
use App\Manager\SpotManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class WorldCheckerRequestListener implements EventSubscriberInterface
{

    /**
     * @var SpotManager
     */
    private $spotManager;

    /**
     * @var MapManager
     */
    private $mapManager;

    /**
     * @param SpotManager $spotManager
     * @param MapManager $mapManager
     */
    public function _construct(SpotManager $spotManager, MapManager $mapManager) {
        $this->spotManager = $spotManager;
        $this->mapManager = $mapManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['controlWorldAround', EventPriorities::POST_DESERIALIZE],
        ];
    }

    /**
     * Control and check the world around the user, if need some new generation points
     * @param GetResponseForControllerResultEvent $event
     */
    public function controlWorldAround(GetResponseForControllerResultEvent $event) {

        /**
         * @var $visitor Visitor
         */
        $visitor = $event->getControllerResult();
        if (!$visitor instanceof Visitor) {
            return;
        }

        /**
         * Check if there is something to update (spots, objects)
         */
        $toUpdate = $this->spotManager->checkWorldAroundVisitor($visitor);
        if($toUpdate == false) {
            return;
        }

        /**
         * If there is, register a miss on the map in the server for the cron job
         */
        $this->mapManager->registerMapDataMiss($visitor);
    }
}