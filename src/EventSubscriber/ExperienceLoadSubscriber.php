<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 13-03-19
 * Time: 09:44
 */

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Experience;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExperienceLoadSubscriber implements EventSubscriberInterface
{
    const GET_EXPERIENCE_ID_ROUTE = 'api_experiences_get_item';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExperienceLoadSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['updateStatus', EventPriorities::PRE_SERIALIZE]
        ];
    }

    /**
     * Updates the status of the experience when it is loaded.
     * If the experience has an end date that is inferior to now, then the status is "NOT AVAILABLE"
     *
     * @param GetResponseForControllerResultEvent $resultEvent
     */
    public function updateStatus(GetResponseForControllerResultEvent $resultEvent)
    {
        if ($resultEvent->getRequest()->attributes->get('_route') !== self::GET_EXPERIENCE_ID_ROUTE) {
            return;
        }

        /** @var Experience $experience */
        $experience = $resultEvent->getControllerResult();

        if ($experience->getEndDate() <= new \DateTime()) { // if the experience is ended
            $experience->setStatus(Experience::STATUS_NOT_AVAILABLE);
            $this->entityManager->flush();
        }
    }
}