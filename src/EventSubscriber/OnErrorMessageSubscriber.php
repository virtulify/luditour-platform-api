<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 13-03-19
 * Time: 14:16
 */

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Manager\MessageManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OnErrorMessageSubscriber implements EventSubscriberInterface
{
    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * OnErrorMessageSubscriber constructor.
     * @param MessageManager $messageManager
     */
    public function __construct(MessageManager $messageManager)
    {
        $this->messageManager = $messageManager;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['onErrorMessage', EventPriorities::POST_RESPOND]
        ];
    }

    public function onErrorMessage(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();

        if ($response->getStatusCode() < 400 || !$response->headers->has('X-Debug-Token-Link')) { // if the response is not an error, continue
            return;
        }

        $title = $this->getErrorTitle($request, $response);
        $content = $this->getContent($request, $response);

        $this->messageManager->sendSlackbotErrorMessage('Hootside bot', $title, $content);
    }

    private function getErrorTitle(Request $request, Response $response)
    {
        switch ($response->getStatusCode()) {
            case Response::HTTP_BAD_REQUEST :
                $statusText = 'HttpBadRequest';
                break;
            case Response::HTTP_NOT_FOUND:
                $statusText = 'HttpNotFound';
                break;
            case Response::HTTP_CONFLICT:
                $statusText = 'HttpConflict';
                break;
            case Response::HTTP_INTERNAL_SERVER_ERROR:
                $statusText = 'InternalServerError';
                break;
            case Response::HTTP_UNAUTHORIZED:
                $statusText = 'HttpUnauthorized';
                break;
            case Response::HTTP_FORBIDDEN:
                $statusText = 'HttpForbidden';
                break;
            default:
                $statusText = 'HTTP error';
                break;
        }
        $now = new \DateTime();
        $title = $statusText . ' ( ' . $response->getStatusCode() . ')' . $request->getUri() . ' at ' . $now->format('d-m-Y H:i:s');


        return $title;
    }

    private function getContent(Request $request, Response $response)
    {
        $params = '';
        foreach ($request->request->all() as $key => $item) {
            if (is_array($item)) {
                $params .= $key . ': Array [Not shown]';
            } else {
                if ($key == 'delay') {
                    $appResponseTime = $item / 1000;
                }
                $params .= $key . ':' . $item . ',';
            }
        }
        $errorContent = '';

        // check if there is a debug link for the profiler's page linked to the response.
        if ($response->headers->has('X-Debug-Token-Link')) {
            $errorContent .= "################# DEBUG TOKEN ##################\n" . $response->headers->get('X-Debug-Token-Link') . "\n";
        }

        $paramCount = count($request->request->all()) + count($request->query->all());
        $errorContent .= "################# PARAMETERS (" . $paramCount . ") ##################\n";

        $errorContent .= $params;

        $errorContent .= "\n";
        $errorContent .= "############## RESPONSE CONTENT ################\n";
        $errorContent .= $response->getContent() . "\n";

        return $errorContent;
    }
}