<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-03-19
 * Time: 10:38
 */

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\VisitorLine;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CurrentVisitorForVisitorLine implements EventSubscriberInterface
{
    const POST_VISITOR_LINE_ROUTE = 'api_visitor_lines_post_collection';
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * CurrentVisitorForVisitorLine constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setVisitor', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setVisitor(GetResponseForControllerResultEvent $resultEvent)
    {
        $visitorLine = $resultEvent->getControllerResult();

        if ($resultEvent->getRequest()->attributes->get('_route') !== self::POST_VISITOR_LINE_ROUTE) {
            return;
        }

        /** @var VisitorLine $visitorLine */
        $visitorLine->setVisitor($this->tokenStorage->getToken()->getUser());
    }
}