<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 07/12/2018
 * Time: 15:23
 */

namespace App\EventSubscriber;


use App\Entity\Visitor;
use App\Manager\MapManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class VisitorUpdateListener
{
    /**
     * @var MapManager
     */
    private $mapManager;

    /**
     * @param MapManager $mapManager
     */
    public function _construct(MapManager $mapManager) {
        $this->mapManager = $mapManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            Events::onFlush
        );

    }

    /**
     * Compute and add the current map data to the response
     * @param LifecycleEventArgs $args
     */
    public function onFlush(LifecycleEventArgs $args) {

        $entity = $args->getEntity();
        $uow = $args->getEntityManager()->getUnitOfWork();
        $entityManager = $args->getEntityManager();

        if($entity instanceof Visitor) {
            /**
             * Compute map
             */
            $map = $this->mapManager->getCurrentMap($entity);

            /**
             * Encode the map to JSON and set it to the current visitor
             */
            $encodedMap = json_encode($map);
            $entity->setCurrentMap($encodedMap);

            $uow->computeChangeSet($entityManager->getClassMetadata('App\Entity\Visitor'), $entity);
        }

    }
}