<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301160057 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visitor_creation_rule (id INT AUTO_INCREMENT NOT NULL, experience_team_id INT NOT NULL, item_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_6794EB7E6C47333C (experience_team_id), INDEX IDX_6794EB7E126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE visitor_creation_rule ADD CONSTRAINT FK_6794EB7E6C47333C FOREIGN KEY (experience_team_id) REFERENCES experience_team (id)');
        $this->addSql('ALTER TABLE visitor_creation_rule ADD CONSTRAINT FK_6794EB7E126F525E FOREIGN KEY (item_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE experience_objects DROP FOREIGN KEY FK_F7312761CF158378');
        $this->addSql('DROP INDEX IDX_F7312761CF158378 ON experience_objects');
        $this->addSql('ALTER TABLE experience_objects CHANGE generator_id creator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE experience_objects ADD CONSTRAINT FK_F731276161220EA6 FOREIGN KEY (creator_id) REFERENCES visitor_lines (id)');
        $this->addSql('CREATE INDEX IDX_F731276161220EA6 ON experience_objects (creator_id)');
        $this->addSql('ALTER TABLE experience_team ADD visitor_count INT NOT NULL, ADD total_visit_time INT NOT NULL, ADD kilometers_walked DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE team ADD visitor_count INT NOT NULL');
        $this->addSql('ALTER TABLE visitors ADD kilometers_walked DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE visitor_lines ADD kilometers_walked DOUBLE PRECISION NOT NULL, ADD score INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE visitor_creation_rule');
        $this->addSql('ALTER TABLE experience_objects DROP FOREIGN KEY FK_F731276161220EA6');
        $this->addSql('DROP INDEX IDX_F731276161220EA6 ON experience_objects');
        $this->addSql('ALTER TABLE experience_objects CHANGE creator_id generator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE experience_objects ADD CONSTRAINT FK_F7312761CF158378 FOREIGN KEY (generator_id) REFERENCES spots (id)');
        $this->addSql('CREATE INDEX IDX_F7312761CF158378 ON experience_objects (generator_id)');
        $this->addSql('ALTER TABLE experience_team DROP visitor_count, DROP total_visit_time, DROP kilometers_walked');
        $this->addSql('ALTER TABLE team DROP visitor_count');
        $this->addSql('ALTER TABLE visitor_lines DROP kilometers_walked, DROP score');
        $this->addSql('ALTER TABLE visitors DROP kilometers_walked');
    }
}
