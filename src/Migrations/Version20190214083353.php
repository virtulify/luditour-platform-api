<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190214083353 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitors CHANGE scope_min_lat scope_min_lat NUMERIC(20, 17) DEFAULT NULL, CHANGE scope_max_lat scope_max_lat NUMERIC(20, 17) DEFAULT NULL, CHANGE scope_min_long scope_min_long NUMERIC(20, 17) DEFAULT NULL, CHANGE scope_max_long scope_max_long NUMERIC(20, 17) DEFAULT NULL, CHANGE current_map current_map LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitors CHANGE scope_min_lat scope_min_lat NUMERIC(20, 17) NOT NULL, CHANGE scope_max_lat scope_max_lat NUMERIC(20, 17) NOT NULL, CHANGE scope_min_long scope_min_long NUMERIC(20, 17) NOT NULL, CHANGE scope_max_long scope_max_long NUMERIC(20, 17) NOT NULL, CHANGE current_map current_map LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
