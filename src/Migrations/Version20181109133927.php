<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109133927 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE medias CHANGE hash_code hash_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE poly_medias CHANGE hash_code hash_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE spots DROP FOREIGN KEY FK_D2BBDDF7D5C9896F');
        $this->addSql('DROP INDEX IDX_D2BBDDF7D5C9896F ON spots');
        $this->addSql('ALTER TABLE spots DROP journey_id');
        $this->addSql('ALTER TABLE three_dimension_medias CHANGE hash_code hash_code VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE medias CHANGE hash_code hash_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE poly_medias CHANGE hash_code hash_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE spots ADD journey_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE spots ADD CONSTRAINT FK_D2BBDDF7D5C9896F FOREIGN KEY (journey_id) REFERENCES spots (id)');
        $this->addSql('CREATE INDEX IDX_D2BBDDF7D5C9896F ON spots (journey_id)');
        $this->addSql('ALTER TABLE three_dimension_medias CHANGE hash_code hash_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
