<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213132118 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitors CHANGE facebook_auth facebook_auth VARCHAR(191) DEFAULT NULL, CHANGE google_auth google_auth VARCHAR(191) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7B74A43FEB27B394 ON visitors (facebook_auth)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7B74A43F1C86055A ON visitors (google_auth)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_7B74A43FEB27B394 ON visitors');
        $this->addSql('DROP INDEX UNIQ_7B74A43F1C86055A ON visitors');
        $this->addSql('ALTER TABLE visitors CHANGE facebook_auth facebook_auth VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE google_auth google_auth VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
