<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109162720 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE avatar_medias (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE medias DROP mime_type');
        $this->addSql('ALTER TABLE poly_medias DROP mime_type');
        $this->addSql('ALTER TABLE three_dimension_medias DROP mime_type');
        $this->addSql('ALTER TABLE visitors ADD avatar_media_id INT DEFAULT NULL, ADD score INT DEFAULT 0 NOT NULL, ADD level INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE visitors ADD CONSTRAINT FK_7B74A43F8B224CA9 FOREIGN KEY (avatar_media_id) REFERENCES avatar_medias (id)');
        $this->addSql('CREATE INDEX IDX_7B74A43F8B224CA9 ON visitors (avatar_media_id)');
        $this->addSql('ALTER TABLE visitor_lines ADD experience_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE visitor_lines ADD CONSTRAINT FK_4124886F46E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('CREATE INDEX IDX_4124886F46E90E27 ON visitor_lines (experience_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitors DROP FOREIGN KEY FK_7B74A43F8B224CA9');
        $this->addSql('DROP TABLE avatar_medias');
        $this->addSql('ALTER TABLE medias ADD mime_type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE poly_medias ADD mime_type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE three_dimension_medias ADD mime_type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE visitor_lines DROP FOREIGN KEY FK_4124886F46E90E27');
        $this->addSql('DROP INDEX IDX_4124886F46E90E27 ON visitor_lines');
        $this->addSql('ALTER TABLE visitor_lines DROP experience_id');
        $this->addSql('DROP INDEX IDX_7B74A43F8B224CA9 ON visitors');
        $this->addSql('ALTER TABLE visitors DROP avatar_media_id, DROP score, DROP level');
    }
}
