<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190308142112 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE visitor_visitor_lines');
        $this->addSql('ALTER TABLE experiences DROP map_overlay_theme');
        $this->addSql('ALTER TABLE items ADD description VARCHAR(255) NOT NULL, CHANGE nfc_commong_tag nfc_common_tag VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE item_actions ADD description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE visitors ADD sex SMALLINT DEFAULT NULL, ADD birth_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE visitor_lines DROP arrival_latitude, DROP arrival_longitude, DROP arrival_altitude, DROP arrival_date');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visitor_visitor_lines (visitor_id INT NOT NULL, visitor_line_id INT NOT NULL, INDEX IDX_BD1E89F970BEE6D (visitor_id), UNIQUE INDEX UNIQ_BD1E89F98F68E58A (visitor_line_id), PRIMARY KEY(visitor_id, visitor_line_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE visitor_visitor_lines ADD CONSTRAINT FK_BD1E89F970BEE6D FOREIGN KEY (visitor_id) REFERENCES visitors (id)');
        $this->addSql('ALTER TABLE visitor_visitor_lines ADD CONSTRAINT FK_BD1E89F98F68E58A FOREIGN KEY (visitor_line_id) REFERENCES visitor_lines (id)');
        $this->addSql('ALTER TABLE experiences ADD map_overlay_theme VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE item_actions DROP description');
        $this->addSql('ALTER TABLE items DROP description, CHANGE nfc_common_tag nfc_commong_tag VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE visitor_lines ADD arrival_latitude NUMERIC(20, 17) NOT NULL, ADD arrival_longitude NUMERIC(20, 17) NOT NULL, ADD arrival_altitude NUMERIC(20, 17) NOT NULL, ADD arrival_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE visitors DROP sex, DROP birth_date');
    }
}
