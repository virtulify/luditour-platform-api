<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307142146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE experiences ADD status INT NOT NULL');
        $this->addSql('ALTER TABLE items ADD use_period_end DATETIME DEFAULT NULL, ADD use_period_time_end TIME DEFAULT NULL, ADD effect_period INT NOT NULL, ADD nfc_commong_tag VARCHAR(255) DEFAULT NULL, DROP multiple_use_countdown, CHANGE use_period use_period_start DATETIME DEFAULT NULL, CHANGE use_period_time use_period_time_start TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE visitor_items ADD activation_date DATETIME DEFAULT NULL, ADD end_date DATETIME DEFAULT NULL, CHANGE visitor_id visitor_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE experiences DROP status');
        $this->addSql('ALTER TABLE items ADD use_period DATETIME DEFAULT NULL, ADD use_period_time TIME DEFAULT NULL, ADD multiple_use_countdown INT DEFAULT NULL, DROP use_period_start, DROP use_period_end, DROP use_period_time_start, DROP use_period_time_end, DROP effect_period, DROP nfc_commong_tag');
        $this->addSql('ALTER TABLE visitor_items DROP activation_date, DROP end_date, CHANGE visitor_id visitor_id INT NOT NULL');
    }
}
