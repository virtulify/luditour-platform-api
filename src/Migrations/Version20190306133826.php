<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306133826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_bundle_medias ADD ios_media_id INT DEFAULT NULL, ADD editor_media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE asset_bundle_medias ADD CONSTRAINT FK_90385435322A4028 FOREIGN KEY (ios_media_id) REFERENCES medias (id)');
        $this->addSql('ALTER TABLE asset_bundle_medias ADD CONSTRAINT FK_903854358EF72960 FOREIGN KEY (editor_media_id) REFERENCES medias (id)');
        $this->addSql('CREATE INDEX IDX_90385435322A4028 ON asset_bundle_medias (ios_media_id)');
        $this->addSql('CREATE INDEX IDX_903854358EF72960 ON asset_bundle_medias (editor_media_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_bundle_medias DROP FOREIGN KEY FK_90385435322A4028');
        $this->addSql('ALTER TABLE asset_bundle_medias DROP FOREIGN KEY FK_903854358EF72960');
        $this->addSql('DROP INDEX IDX_90385435322A4028 ON asset_bundle_medias');
        $this->addSql('DROP INDEX IDX_903854358EF72960 ON asset_bundle_medias');
        $this->addSql('ALTER TABLE asset_bundle_medias DROP ios_media_id, DROP editor_media_id');
    }
}
