<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109133104 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE medias ADD disk_usage INT NOT NULL, ADD filename VARCHAR(255) NOT NULL, ADD mime_type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE poly_medias ADD disk_usage INT NOT NULL, ADD filename VARCHAR(255) NOT NULL, ADD mime_type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE three_dimension_medias ADD disk_usage INT NOT NULL, ADD filename VARCHAR(255) NOT NULL, ADD mime_type VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE medias DROP disk_usage, DROP filename, DROP mime_type');
        $this->addSql('ALTER TABLE poly_medias DROP disk_usage, DROP filename, DROP mime_type');
        $this->addSql('ALTER TABLE three_dimension_medias DROP disk_usage, DROP filename, DROP mime_type');
    }
}
