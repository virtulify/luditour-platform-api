<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109101117 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE positions (id INT AUTO_INCREMENT NOT NULL, latitude NUMERIC(20, 17) NOT NULL, longitude NUMERIC(20, 17) NOT NULL, altitude NUMERIC(20, 17) NOT NULL, scope_min_lat NUMERIC(20, 17) NOT NULL, scope_max_lat NUMERIC(20, 17) NOT NULL, scope_min_long NUMERIC(20, 17) NOT NULL, scope_max_long NUMERIC(20, 17) NOT NULL, hash_code VARCHAR(191) NOT NULL, UNIQUE INDEX UNIQ_D69FE57CC8C66728 (hash_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quests (id INT AUTO_INCREMENT NOT NULL, experience_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME DEFAULT NULL, start_period_time TIME DEFAULT NULL, end_period_time TIME DEFAULT NULL, period_type INT NOT NULL, weather_type INT NOT NULL, spot_visible TINYINT(1) NOT NULL, INDEX IDX_989E5D3446E90E27 (experience_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quest_spots (quest_id INT NOT NULL, spot_id INT NOT NULL, INDEX IDX_8106446B209E9EF4 (quest_id), INDEX IDX_8106446B2DF1D37C (spot_id), PRIMARY KEY(quest_id, spot_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spots (id INT AUTO_INCREMENT NOT NULL, journey_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type INT NOT NULL, latitude NUMERIC(20, 17) NOT NULL, longitude NUMERIC(20, 17) NOT NULL, altitude NUMERIC(20, 17) NOT NULL, `range` DOUBLE PRECISION NOT NULL, private TINYINT(1) NOT NULL, INDEX IDX_D2BBDDF7D5C9896F (journey_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE three_dimension_medias (id INT AUTO_INCREMENT NOT NULL, hash_code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visitors (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, mail VARCHAR(255) DEFAULT NULL, facebook_auth LONGTEXT DEFAULT NULL, google_auth LONGTEXT DEFAULT NULL, scope_min_lat NUMERIC(20, 17) NOT NULL, scope_max_lat NUMERIC(20, 17) NOT NULL, scope_min_long NUMERIC(20, 17) NOT NULL, scope_max_long NUMERIC(20, 17) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visitor_visitor_lines (visitor_id INT NOT NULL, visitor_line_id INT NOT NULL, INDEX IDX_BD1E89F970BEE6D (visitor_id), UNIQUE INDEX UNIQ_BD1E89F98F68E58A (visitor_line_id), PRIMARY KEY(visitor_id, visitor_line_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visitor_lines (id INT AUTO_INCREMENT NOT NULL, registration_latitude NUMERIC(20, 17) NOT NULL, registration_longitude NUMERIC(20, 17) NOT NULL, registration_altitude NUMERIC(20, 17) NOT NULL, registration_date DATETIME DEFAULT NULL, arrival_latitude NUMERIC(20, 17) NOT NULL, arrival_longitude NUMERIC(20, 17) NOT NULL, arrival_altitude NUMERIC(20, 17) NOT NULL, arrival_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE waypoints (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE quests ADD CONSTRAINT FK_989E5D3446E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('ALTER TABLE quest_spots ADD CONSTRAINT FK_8106446B209E9EF4 FOREIGN KEY (quest_id) REFERENCES quests (id)');
        $this->addSql('ALTER TABLE quest_spots ADD CONSTRAINT FK_8106446B2DF1D37C FOREIGN KEY (spot_id) REFERENCES spots (id)');
        $this->addSql('ALTER TABLE spots ADD CONSTRAINT FK_D2BBDDF7D5C9896F FOREIGN KEY (journey_id) REFERENCES spots (id)');
        $this->addSql('ALTER TABLE visitor_visitor_lines ADD CONSTRAINT FK_BD1E89F970BEE6D FOREIGN KEY (visitor_id) REFERENCES visitors (id)');
        $this->addSql('ALTER TABLE visitor_visitor_lines ADD CONSTRAINT FK_BD1E89F98F68E58A FOREIGN KEY (visitor_line_id) REFERENCES visitor_lines (id)');
        $this->addSql('ALTER TABLE experiences ADD CONSTRAINT FK_82020E70BAAE86A3 FOREIGN KEY (logo_media_id) REFERENCES medias (id)');
        $this->addSql('ALTER TABLE experiences ADD CONSTRAINT FK_82020E70E8A61356 FOREIGN KEY (banner_media_id) REFERENCES medias (id)');
        $this->addSql('ALTER TABLE experience_positions ADD CONSTRAINT FK_A230733F46E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('ALTER TABLE experience_positions ADD CONSTRAINT FK_A230733FDD842E46 FOREIGN KEY (position_id) REFERENCES positions (id)');
        $this->addSql('ALTER TABLE experience_objects ADD CONSTRAINT FK_F7312761CF158378 FOREIGN KEY (generator_id) REFERENCES spots (id)');
        $this->addSql('ALTER TABLE experience_objects ADD CONSTRAINT FK_F73127618B176834 FOREIGN KEY (generation_rule_id) REFERENCES generation_rules (id)');
        $this->addSql('ALTER TABLE experience_objects ADD CONSTRAINT FK_F73127617BB1FD97 FOREIGN KEY (waypoint_id) REFERENCES waypoints (id)');
        $this->addSql('ALTER TABLE generation_rules ADD CONSTRAINT FK_3B4B6309767755AF FOREIGN KEY (appearence_media_id) REFERENCES medias (id)');
        $this->addSql('ALTER TABLE journeys ADD CONSTRAINT FK_231E1B0946E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('ALTER TABLE journey_spots ADD CONSTRAINT FK_80CC534E46E90E27 FOREIGN KEY (experience_id) REFERENCES journeys (id)');
        $this->addSql('ALTER TABLE journey_spots ADD CONSTRAINT FK_80CC534E2DF1D37C FOREIGN KEY (spot_id) REFERENCES spots (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE experience_positions DROP FOREIGN KEY FK_A230733FDD842E46');
        $this->addSql('ALTER TABLE quest_spots DROP FOREIGN KEY FK_8106446B209E9EF4');
        $this->addSql('ALTER TABLE experience_objects DROP FOREIGN KEY FK_F7312761CF158378');
        $this->addSql('ALTER TABLE journey_spots DROP FOREIGN KEY FK_80CC534E2DF1D37C');
        $this->addSql('ALTER TABLE quest_spots DROP FOREIGN KEY FK_8106446B2DF1D37C');
        $this->addSql('ALTER TABLE spots DROP FOREIGN KEY FK_D2BBDDF7D5C9896F');
        $this->addSql('ALTER TABLE visitor_visitor_lines DROP FOREIGN KEY FK_BD1E89F970BEE6D');
        $this->addSql('ALTER TABLE visitor_visitor_lines DROP FOREIGN KEY FK_BD1E89F98F68E58A');
        $this->addSql('ALTER TABLE experience_objects DROP FOREIGN KEY FK_F73127617BB1FD97');
        $this->addSql('DROP TABLE positions');
        $this->addSql('DROP TABLE quests');
        $this->addSql('DROP TABLE quest_spots');
        $this->addSql('DROP TABLE spots');
        $this->addSql('DROP TABLE three_dimension_medias');
        $this->addSql('DROP TABLE visitors');
        $this->addSql('DROP TABLE visitor_visitor_lines');
        $this->addSql('DROP TABLE visitor_lines');
        $this->addSql('DROP TABLE waypoints');
        $this->addSql('ALTER TABLE experience_objects DROP FOREIGN KEY FK_F73127618B176834');
        $this->addSql('ALTER TABLE experience_positions DROP FOREIGN KEY FK_A230733F46E90E27');
        $this->addSql('ALTER TABLE experiences DROP FOREIGN KEY FK_82020E70BAAE86A3');
        $this->addSql('ALTER TABLE experiences DROP FOREIGN KEY FK_82020E70E8A61356');
        $this->addSql('ALTER TABLE generation_rules DROP FOREIGN KEY FK_3B4B6309767755AF');
        $this->addSql('ALTER TABLE journey_spots DROP FOREIGN KEY FK_80CC534E46E90E27');
        $this->addSql('ALTER TABLE journeys DROP FOREIGN KEY FK_231E1B0946E90E27');
    }
}
