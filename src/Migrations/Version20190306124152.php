<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306124152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_bundle_medias DROP hash_code, DROP disk_usage, DROP filename');
        $this->addSql('ALTER TABLE experiences ADD world_package_media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE experiences ADD CONSTRAINT FK_82020E705F0E649 FOREIGN KEY (world_package_media_id) REFERENCES asset_bundle_medias (id)');
        $this->addSql('CREATE INDEX IDX_82020E705F0E649 ON experiences (world_package_media_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_bundle_medias ADD hash_code VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD disk_usage INT NOT NULL, ADD filename VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE experiences DROP FOREIGN KEY FK_82020E705F0E649');
        $this->addSql('DROP INDEX IDX_82020E705F0E649 ON experiences');
        $this->addSql('ALTER TABLE experiences DROP world_package_media_id');
    }
}
