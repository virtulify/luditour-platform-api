<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301083158 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE experience_item (experience_id INT NOT NULL, item_id INT NOT NULL, INDEX IDX_4B0BEA0346E90E27 (experience_id), INDEX IDX_4B0BEA03126F525E (item_id), PRIMARY KEY(experience_id, item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experience_team (id INT AUTO_INCREMENT NOT NULL, team_id INT NOT NULL, experience_id INT NOT NULL, score INT NOT NULL, INDEX IDX_90F06902296CD8AE (team_id), INDEX IDX_90F0690246E90E27 (experience_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(35) NOT NULL, description VARCHAR(255) NOT NULL, score INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE experience_item ADD CONSTRAINT FK_4B0BEA0346E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experience_item ADD CONSTRAINT FK_4B0BEA03126F525E FOREIGN KEY (item_id) REFERENCES items (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experience_team ADD CONSTRAINT FK_90F06902296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE experience_team ADD CONSTRAINT FK_90F0690246E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('ALTER TABLE asset_bundle_medias ADD description LONGTEXT DEFAULT NULL, DROP root');
        $this->addSql('ALTER TABLE visitor_items ADD visitor_id INT NOT NULL');
        $this->addSql('ALTER TABLE visitor_items ADD CONSTRAINT FK_EF3BEDB470BEE6D FOREIGN KEY (visitor_id) REFERENCES visitors (id)');
        $this->addSql('CREATE INDEX IDX_EF3BEDB470BEE6D ON visitor_items (visitor_id)');
        $this->addSql('ALTER TABLE visitor_lines DROP FOREIGN KEY FK_4124886F46E90E27');
        $this->addSql('DROP INDEX IDX_4124886F46E90E27 ON visitor_lines');
        $this->addSql('ALTER TABLE visitor_lines ADD experience_team_id INT NOT NULL, DROP experience_id');
        $this->addSql('ALTER TABLE visitor_lines ADD CONSTRAINT FK_4124886F6C47333C FOREIGN KEY (experience_team_id) REFERENCES experience_team (id)');
        $this->addSql('CREATE INDEX IDX_4124886F6C47333C ON visitor_lines (experience_team_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitor_lines DROP FOREIGN KEY FK_4124886F6C47333C');
        $this->addSql('ALTER TABLE experience_team DROP FOREIGN KEY FK_90F06902296CD8AE');
        $this->addSql('DROP TABLE experience_item');
        $this->addSql('DROP TABLE experience_team');
        $this->addSql('DROP TABLE team');
        $this->addSql('ALTER TABLE asset_bundle_medias ADD root VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP description');
        $this->addSql('ALTER TABLE visitor_items DROP FOREIGN KEY FK_EF3BEDB470BEE6D');
        $this->addSql('DROP INDEX IDX_EF3BEDB470BEE6D ON visitor_items');
        $this->addSql('ALTER TABLE visitor_items DROP visitor_id');
        $this->addSql('DROP INDEX IDX_4124886F6C47333C ON visitor_lines');
        $this->addSql('ALTER TABLE visitor_lines ADD experience_id INT DEFAULT NULL, DROP experience_team_id');
        $this->addSql('ALTER TABLE visitor_lines ADD CONSTRAINT FK_4124886F46E90E27 FOREIGN KEY (experience_id) REFERENCES experiences (id)');
        $this->addSql('CREATE INDEX IDX_4124886F46E90E27 ON visitor_lines (experience_id)');
    }
}
