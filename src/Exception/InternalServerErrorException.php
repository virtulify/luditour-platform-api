<?php
/**
 * Created by PhpStorm.
 * User: Stagiaire
 * Date: 12-10-18
 * Time: 11:16
 */

namespace App\Exception;


final class InternalServerErrorException extends \RuntimeException
{
}