<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-03-19
 * Time: 11:35
 */

namespace App\Exception;


class ForbiddenException extends \RuntimeException
{
}