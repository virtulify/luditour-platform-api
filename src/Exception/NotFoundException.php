<?php
/**
 * Created by PhpStorm.
 * User: Stagiaire
 * Date: 04-10-18
 * Time: 16:45
 */

namespace App\Exception;


final class NotFoundException extends \RuntimeException
{
}