<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *
 * @ORM\Entity()
 * @ORM\Table(name="asset_bundle_medias")
 * @ApiResource()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class AssetBundleMedia
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"experience:read"})
     *
     */
    protected $id;

    /**
     * @var string
     **
     * @ORM\Column(type="string")
     * @Groups({"experience:read"})
     */
    private $name;

    /**
     * @var string
     **
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"experience:read"})
     */
    private $description;

    /**
     * @var Media
     * @Groups({"experience:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="android_media_id", referencedColumnName="id")
     *
     */
    private $androidMedia;

    /**
     * @var Media
     * @Groups({"experience:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="ios_media_id", referencedColumnName="id")
     *
     */
    private $iosMedia;

    /**
     * @var Media
     * @Groups({"experience:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="editor_media_id", referencedColumnName="id")
     *
     */
    private $editorMedia;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AssetBundleMedia
     */
    public function setName(string $name): AssetBundleMedia
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return AssetBundleMedia
     */
    public function setDescription(string $description): AssetBundleMedia
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Media
     */
    public function getAndroidMedia()
    {
        return $this->androidMedia;
    }

    /**
     * @param Media $androidMedia
     * @return AssetBundleMedia
     */
    public function setAndroidMedia($androidMedia)
    {
        $this->androidMedia = $androidMedia;
        return $this;
    }

    /**
     * @return Media
     */
    public function getIosMedia()
    {
        return $this->iosMedia;
    }

    /**
     * @param Media $iosMedia
     * @return AssetBundleMedia
     */
    public function setIosMedia($iosMedia)
    {
        $this->iosMedia = $iosMedia;
        return $this;
    }

    /**
     * @return Media
     */
    public function getEditorMedia()
    {
        return $this->editorMedia;
    }

    /**
     * @param Media $editorMedia
     * @return AssetBundleMedia
     */
    public function setEditorMedia($editorMedia)
    {
        $this->editorMedia = $editorMedia;
        return $this;
    }


    /**
     * @return int
     */
    public function getDiskUsage(): int
    {
        $diskUsageAndroid = $this->androidMedia ? $this->androidMedia->getDiskUsage() : 0;
        $diskUsageIos = $this->iosMedia ? $this->iosMedia->getDiskUsage() : 0;
        $diskUsageEditor = $this->editorMedia ? $this->editorMedia->getDiskUsage() : 0;
        return $diskUsageAndroid + $diskUsageEditor + $diskUsageIos;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}