<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Generation Rule
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity(repositoryClass="App\Repository\GenerationRuleRepository")
 * @ORM\Table(name="generation_rules")
 * @ApiResource(
 *     normalizationContext={
 *          "groups"={"generationRule", "generationRule:read"}
 *     },
 *     denormalizationContext={
 *          "groups"={"generationRule", "generationRule:write"}
 *     }
 * )
 */
class GenerationRule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"generationRule:read"})
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="spawn_ratio", type="float")
     *
     * @Groups({"generationRule"})
     */
    private $spawnRatio;

    /**
     * @var float
     *
     * @ORM\Column(name="view_range", type="float")
     *
     * @Groups({"generationRule"})
     */
    private $viewRange;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="appearence_media_id", referencedColumnName="id")
     *
     * @Groups({"generationRule:read"})
     */
    private $mapAppearanceMedia;

    /**
     * @var boolean
     * @ORM\Column(name="from_user", type="boolean")
     * @Groups({"generationRule"})
     */
    private $collectable;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     * @Assert\Range(min=1, max=30)
     * @Groups({"generationRule"})
     */
    private $actionType;

    /**
     * @var Experience
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
     * @Groups({"generationRule"})
     */
    private $experience;

    /**
     * @var Media
     * @Groups({"generationRule:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     */
    private $actionAppearanceMedia;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getSpawnRatio(): float
    {
        return $this->spawnRatio;
    }

    /**
     * @param float $spawnRatio
     * @return GenerationRule
     */
    public function setSpawnRatio(float $spawnRatio): GenerationRule
    {
        $this->spawnRatio = $spawnRatio;
        return $this;
    }

    /**
     * @return float
     */
    public function getViewRange(): float
    {
        return $this->viewRange;
    }

    /**
     * @param float $viewRange
     * @return GenerationRule
     */
    public function setViewRange(float $viewRange): GenerationRule
    {
        $this->viewRange = $viewRange;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapAppearanceMedia()
    {
        return $this->mapAppearanceMedia;
    }

    /**
     * @param mixed $mapAppearanceMedia
     * @return GenerationRule
     */
    public function setMapAppearanceMedia($mapAppearanceMedia)
    {
        $this->mapAppearanceMedia = $mapAppearanceMedia;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCollectable(): bool
    {
        return $this->collectable;
    }

    /**
     * @param bool $collectable
     * @return GenerationRule
     */
    public function setCollectable(bool $collectable): GenerationRule
    {
        $this->collectable = $collectable;
        return $this;
    }

    /**
     * @return int
     */
    public function getActionType(): int
    {
        return $this->actionType;
    }

    /**
     * @param int $actionType
     * @return GenerationRule
     */
    public function setActionType(int $actionType): GenerationRule
    {
        $this->actionType = $actionType;
        return $this;
    }

    /**
     * @return Experience
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed $experience
     * @return GenerationRule
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
        return $this;
    }

    public function getActionAppearanceMedia(): ?Media
    {
        return $this->actionAppearanceMedia;
    }

    public function setActionAppearanceMedia(?Media $actionAppearanceMedia): self
    {
        $this->actionAppearanceMedia = $actionAppearanceMedia;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}