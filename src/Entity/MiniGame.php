<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiResource(
 *     description="A mini game in the experience"
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MiniGameRepository")
 */
class MiniGame
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ApiProperty(description="The type of minigame")
     */
    private $type;

    /**
     * @var Media
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(description="The AR media used in the mini game")
     */
    private $arMedia;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="Soft delete field")
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MiniGameState", mappedBy="miniGame")
     * @ApiProperty(description="The instances of the mini game that has been played")
     */
    private $miniGameStates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EnterRule", mappedBy="miniGame", orphanRemoval=true)
     * @ApiProperty(description="The conditions required to play the current mini game")
     */
    private $enterRules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EndRule", mappedBy="miniGame", orphanRemoval=true)
     * @ApiProperty(description="The rules describing the resulting consequences of the mini game depending on the score of the player")
     */
    private $endRules;

    /**
     * @var Experience
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="miniGames")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The experience where the mini game is playable.")
     */
    private $experience;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Waypoint", inversedBy="miniGames")
     * @ApiProperty(description="The waypoints where the mini game can be played")
     */
    private $waypoints;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Spot", inversedBy="miniGames")
     * @ApiProperty(description="The spots where the mini game can be played.")
     */
    private $spots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExperienceObject", mappedBy="miniGame")
     * @ApiProperty(description="The experience objects where the mini game can be accessed.")
     */
    private $experienceObjects;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->miniGameStates = new ArrayCollection();
        $this->enterRules = new ArrayCollection();
        $this->endRules = new ArrayCollection();
        $this->waypoints = new ArrayCollection();
        $this->spots = new ArrayCollection();
        $this->experienceObjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getArMedia(): ?Media
    {
        return $this->arMedia;
    }

    public function setArMedia(Media $arMedia): self
    {
        $this->arMedia = $arMedia;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|MiniGameState[]
     */
    public function getMiniGameStates(): Collection
    {
        return $this->miniGameStates;
    }

    public function addMiniGameState(MiniGameState $miniGameState): self
    {
        if (!$this->miniGameStates->contains($miniGameState)) {
            $this->miniGameStates[] = $miniGameState;
            $miniGameState->setMiniGame($this);
        }

        return $this;
    }

    public function removeMiniGameState(MiniGameState $miniGameState): self
    {
        if ($this->miniGameStates->contains($miniGameState)) {
            $this->miniGameStates->removeElement($miniGameState);
            // set the owning side to null (unless already changed)
            if ($miniGameState->getMiniGame() === $this) {
                $miniGameState->setMiniGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EnterRule[]
     */
    public function getEnterRules(): Collection
    {
        return $this->enterRules;
    }

    public function addEnterRule(EnterRule $enterRule): self
    {
        if (!$this->enterRules->contains($enterRule)) {
            $this->enterRules[] = $enterRule;
            $enterRule->setMiniGame($this);
        }

        return $this;
    }

    public function removeEnterRule(EnterRule $enterRule): self
    {
        if ($this->enterRules->contains($enterRule)) {
            $this->enterRules->removeElement($enterRule);
            // set the owning side to null (unless already changed)
            if ($enterRule->getMiniGame() === $this) {
                $enterRule->setMiniGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EndRule[]
     */
    public function getEndRules(): Collection
    {
        return $this->endRules;
    }

    public function addEndRule(EndRule $endRule): self
    {
        if (!$this->endRules->contains($endRule)) {
            $this->endRules[] = $endRule;
            $endRule->setMiniGame($this);
        }

        return $this;
    }

    public function removeEndRule(EndRule $endRule): self
    {
        if ($this->endRules->contains($endRule)) {
            $this->endRules->removeElement($endRule);
            // set the owning side to null (unless already changed)
            if ($endRule->getMiniGame() === $this) {
                $endRule->setMiniGame(null);
            }
        }

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return Collection|Waypoint[]
     */
    public function getWaypoints(): Collection
    {
        return $this->waypoints;
    }

    public function addWaypoint(Waypoint $waypoint): self
    {
        if (!$this->waypoints->contains($waypoint)) {
            $this->waypoints[] = $waypoint;
        }

        return $this;
    }

    public function removeWaypoint(Waypoint $waypoint): self
    {
        if ($this->waypoints->contains($waypoint)) {
            $this->waypoints->removeElement($waypoint);
        }

        return $this;
    }

    /**
     * @return Collection|Spot[]
     */
    public function getSpots(): Collection
    {
        return $this->spots;
    }

    public function addSpot(Spot $spot): self
    {
        if (!$this->spots->contains($spot)) {
            $this->spots[] = $spot;
        }

        return $this;
    }

    public function removeSpot(Spot $spot): self
    {
        if ($this->spots->contains($spot)) {
            $this->spots->removeElement($spot);
        }

        return $this;
    }

    /**
     * @return Collection|ExperienceObject[]
     */
    public function getExperienceObjects(): Collection
    {
        return $this->experienceObjects;
    }

    public function addExperienceObject(ExperienceObject $experienceObject): self
    {
        if (!$this->experienceObjects->contains($experienceObject)) {
            $this->experienceObjects[] = $experienceObject;
            $experienceObject->setMiniGame($this);
        }

        return $this;
    }

    public function removeExperienceObject(ExperienceObject $experienceObject): self
    {
        if ($this->experienceObjects->contains($experienceObject)) {
            $this->experienceObjects->removeElement($experienceObject);
            // set the owning side to null (unless already changed)
            if ($experienceObject->getMiniGame() === $this) {
                $experienceObject->setMiniGame(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
