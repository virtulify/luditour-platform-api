<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExperienceTeamRepository")
 * @ApiResource()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class ExperienceTeam
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"experience:read"})
     */
    private $id;

    /**
     * @Assert\Type(type="integer")
     * @Assert\GreaterThanOrEqual(0)
     * @ORM\Column(type="integer")
     * @Groups({"experience:read"})
     */
    private $score;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorLine", mappedBy="experienceTeam")
     */
    private $visitorLines;

    /**
     * @var Team
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="experienceTeams")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"experience:read"})
     */
    private $team;

    /**
     * @var Experience
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="experienceTeams")
     * @ORM\JoinColumn(nullable=false)
     */
    private $experience;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorCreationRule", mappedBy="experienceTeam", orphanRemoval=true)
     */
    private $visitorCreationRules;

    /**
     * @Groups({"experience:read"})
     * @ORM\Column(type="integer")
     */
    private $visitorCount;

    /**
     * @Groups({"experience:read"})
     * @ORM\Column(type="integer")
     */
    private $totalVisitTime;

    /*
     * @Groups({"experience:read"})
     * @ORM\Column(type="float")
     */
    private $kilometersWalked;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->visitorLines = new ArrayCollection();
        $this->score = 0;
        $this->visitorCreationRules = new ArrayCollection();
        $this->totalVisitTime = 0;
        $this->visitorCount = 0;
        $this->kilometersWalked = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return Collection|VisitorLine[]
     */
    public function getVisitorLines(): Collection
    {
        return $this->visitorLines;
    }

    public function addVisitorLine(VisitorLine $visitorLine): self
    {
        if (!$this->visitorLines->contains($visitorLine)) {
            $this->visitorLines[] = $visitorLine;
            $visitorLine->setExperienceTeam($this);
        }

        return $this;
    }

    public function removeVisitorLine(VisitorLine $visitorLine): self
    {
        if ($this->visitorLines->contains($visitorLine)) {
            $this->visitorLines->removeElement($visitorLine);
            // set the owning side to null (unless already changed)
            if ($visitorLine->getExperienceTeam() === $this) {
                $visitorLine->setExperienceTeam(null);
            }
        }

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return Collection|VisitorCreationRule[]
     */
    public function getVisitorCreationRules(): Collection
    {
        return $this->visitorCreationRules;
    }

    public function addVisitorCreationRule(VisitorCreationRule $visitorCreationRule): self
    {
        if (!$this->visitorCreationRules->contains($visitorCreationRule)) {
            $this->visitorCreationRules[] = $visitorCreationRule;
            $visitorCreationRule->setExperienceTeam($this);
        }

        return $this;
    }

    public function removeVisitorCreationRule(VisitorCreationRule $visitorCreationRule): self
    {
        if ($this->visitorCreationRules->contains($visitorCreationRule)) {
            $this->visitorCreationRules->removeElement($visitorCreationRule);
            // set the owning side to null (unless already changed)
            if ($visitorCreationRule->getExperienceTeam() === $this) {
                $visitorCreationRule->setExperienceTeam(null);
            }
        }

        return $this;
    }

    public function getVisitorCount(): ?int
    {
        return $this->visitorCount;
    }

    public function setVisitorCount(int $visitorCount): self
    {
        $this->visitorCount = $visitorCount;

        return $this;
    }

    public function getTotalVisitTime(): ?int
    {
        return $this->totalVisitTime;
    }

    public function setTotalVisitTime(int $totalVisitTime): self
    {
        $this->totalVisitTime = $totalVisitTime;

        return $this;
    }

    public function getKilometersWalked(): ?float
    {
        return $this->kilometersWalked;
    }

    public function setKilometersWalked(float $kilometersWalked): self
    {
        $this->kilometersWalked = $kilometersWalked;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
