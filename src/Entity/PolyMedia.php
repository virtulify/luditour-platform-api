<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:41
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * PolyMedia
 *
 * @ORM\Entity
 * @ORM\Table(name="poly_medias")
 * @ApiResource()
 */
class PolyMedia extends Media
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}