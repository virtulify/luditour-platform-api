<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:05
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Experience
 *
 * @ORM\Entity(repositoryClass="App\Repository\ExperienceRepository")
 * @ORM\Table(name="experiences")
 * @ApiResource(
 *     normalizationContext={"groups"={"experience", "experience:read"}},
 *     denormalizationContext={"groups"={"experience", "experience:write"}},
 *     description="The experience loaded in the Hootside App when it starts."
 * )
 * @ApiFilter(
 *     RangeFilter::class,
 *     properties={
 *          "allowedPositions.scopeMinLat",
 *          "allowedPositions.scopeMaxLat",
 *          "allowedPositions.scopeMinLong",
 *          "allowedPositions.scopeMaxLong"
 *     }
 * )
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Experience
{
    /**
     * The experience is archived by the user
     */
    const STATUS_ARCHIVED = -2;

    /**
     * The experience is blocked by admin or pushed offline by the system
     */
    const STATUS_NOT_AVAILABLE = -1;

    /**
     * The experience is currently in configuration
     */
    const STATUS_DRAFT = 0;

    /**
     * The experience is currently in test
     */
    const STATUS_TEST = 1;

    /**
     * The experience is available for the end users
     */
    const STATUS_AVAILABLE = 2;

    const STATUS = [
        'Archived' => self::STATUS_ARCHIVED,
        'Not available' => self::STATUS_NOT_AVAILABLE,
        'Draft' => self::STATUS_DRAFT,
        'Test' => self::STATUS_TEST,
        'Available' => self::STATUS_AVAILABLE
    ];

    /**
     * @var int
     *
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @ApiProperty(description="Identifier for the experience.")
     */
    protected $id;

    /**
     * @var string
     *
     * @Groups({"experience"})
     *
     * @ORM\Column(name="name",type="string")
     * @Assert\Length(min = 3, max = 15)
     *
     * @ApiProperty(description="The experience's name.")
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Groups({"experience"})
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     *
     * @ApiProperty(description="The starting date of the experience.")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @Groups({"experience"})
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     *
     * @ApiProperty(description="The ending date of the experience.")
     */
    private $endDate;

    /**
     * @var string
     *
     * @Groups({"experience"})
     *
     * @ORM\Column(name="short_description",type="string")
     * @Assert\Length(min = 50, max = 100)
     *
     * @ApiProperty(description="The short description for the experience, displayed in the experience list in Luditour App.")
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @Groups({"experience"})
     *
     * @ORM\Column(name="long_description",type="text")
     * @Assert\Length(min = 100, max = 200)
     *
     * @ApiProperty(description="The detailed description for the experience.")
     */
    private $longDescription;

    /**
     * @var Media
     * @Groups({"experience"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="logo_media_id", referencedColumnName="id")
     *
     * @ApiProperty(description="The logo picture of the experience.")
     */
    private $logoMedia;

    /**
     * @var Media
     * @Groups({"experience"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="banner_media_id", referencedColumnName="id")
     *
     * @ApiProperty(description="The banner picture of the experience.")
     */
    private $bannerMedia;

    /**
     * @Groups({"experience:read"})
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Position")
     * @ORM\JoinTable(name="experience_positions",
     *      joinColumns={@ORM\JoinColumn(name="experience_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="position_id", referencedColumnName="id", unique=true)}
     *      )
     *
     * @ApiProperty(description="The positions where the experience can be played. For example : can represent several cities, etc.")
     * @ApiSubresource()
     */
    private $allowedPositions;

    /**
     * @var integer
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="visitor_count", type="integer", options={"default" : 0})
     *
     * @ApiProperty(description="The number of visitors that did the experience.")
     */
    private $visitorCount;

    /**
     * @var integer
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="total_visit_time", type="integer", options={"default" : 0})
     *
     * @ApiProperty(description="The total time spent by all the visitors.")
     */
    private $totalVisitTime;

    /**
     * @var float
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="rating_average", type="decimal", options={"default" : 0.0})
     * @Assert\Type(type="float")
     */
    private $ratingAverage;

    /**
     * @var float
     *
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="price", type="decimal", options={"default" : 0.0})
     * @Assert\Type(type="float")
     *
     * @ApiProperty(description="The price of the experience.")
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @Groups({"experience:read"})
     *
     * @ORM\Column(type="datetime")
     *
     * @ApiProperty(description="Date of creation for the experience.")
     */
    private $created;

    /**
     * @var \DateTime
     * @Groups({"experience:read"})
     *
     * @ORM\Column(type="datetime")
     *
     * @ApiProperty(description="Date of last update for the experience.")
     */
    private $updated;

    /**
     * @var \DateTime
     * @Groups({"experience:read"})
     *
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     *
     * @ApiProperty(description="Date of deletion for the experience.")
     */
    private $deletedAt;

    /**
     * @Groups({"experience:read"})
     * One Experience has Many Journeys (or none).
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="experience")
     *
     * @ApiProperty(description="The journeys related to the experience.")
     */
    private $journeys;

    /**
     * @Groups({"experience:read"})
     * One Experience has Many Journeys (or none).
     * @ORM\OneToMany(targetEntity="App\Entity\Quest", mappedBy="experience")
     *
     * @ApiProperty(description="The quests related to the experience.")
     */
    private $quests;

    /**
     * @ApiSubresource()
     * @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="experiences")
     * @Groups({"experience:read"})
     */
    private $items;

    /**
     * @Groups({"experience:read"})
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity="App\Entity\ExperienceTeam", mappedBy="experience", orphanRemoval=true)
     */
    private $experienceTeams;

    /**
     * World asset bundle media (can be null if there is not world media)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetBundleMedia")
     * @ORM\JoinColumn(name="world_package_media_id", referencedColumnName="id", nullable=true)
     * @Groups({"experience:read"})
     */
    private $worldPackageMedia;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"experience:read"})
     * @ApiProperty(
     *     description="The status of the experience in integer value."
     * )
     */
    private $status;

    /**
     * @var Media
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"persist", "remove"})
     * @Groups({"experience:read"})
     * @ApiProperty(description="Presentation video displayed when the visitor watches the long description of the experience in the catalogue")
     */
    private $presentationVideo;

    /**
     * @Groups({"experience"})
     * @ApiProperty(description="The list of categories the current experience is related to.")
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="experiences")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MiniGame", mappedBy="experience")
     * @ApiProperty(description="The mini games available in the current experience.")
     */
    private $miniGames;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Spot", mappedBy="experience")
     * @ApiProperty(description="The spots related to the current experience. These will be loaded by the location of a logged in visitor after each refresh data route call.")
     */
    private $spots;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     * @ApiProperty(description="The radius in kilometers where the experience objects related to the current experience can be detected in the map for a player.")
     * @Groups({"experience"})
     */
    private $experienceObjectRadius;

    /**
     * Experience constructor.
     */
    public function __construct()
    {
        $this->journeys = new ArrayCollection();
        $this->quests = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->experienceTeams = new ArrayCollection();
        $this->allowedPositions = new ArrayCollection();
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->price = 0.0;
        $this->ratingAverage = 0.0;
        $this->totalVisitTime = 0;
        $this->visitorCount = 0;
        $this->status = self::STATUS_AVAILABLE;
        $this->categories = new ArrayCollection();
        $this->miniGames = new ArrayCollection();
        $this->spots = new ArrayCollection();
        $this->experienceObjectRadius = 0.5;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Experience
     */
    public function setId(int $id): Experience
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Experience
     */
    public function setName(string $name): Experience
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     * @return Experience
     */
    public function setShortDescription(string $shortDescription): Experience
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongDescription(): string
    {
        return $this->longDescription;
    }

    /**
     * @param string $longDescription
     * @return Experience
     */
    public function setLongDescription(string $longDescription): Experience
    {
        $this->longDescription = $longDescription;
        return $this;
    }

    /**
     * @return Media
     */
    public function getLogoMedia()
    {
        return $this->logoMedia;
    }

    /**
     * @param mixed $logoMedia
     * @return Experience
     */
    public function setLogoMedia($logoMedia)
    {
        $this->logoMedia = $logoMedia;
        return $this;
    }

    /**
     * @return Media
     */
    public function getBannerMedia()
    {
        return $this->bannerMedia;
    }

    /**
     * @param Media $bannerMedia
     * @return Experience
     */
    public function setBannerMedia($bannerMedia)
    {
        $this->bannerMedia = $bannerMedia;
        return $this;
    }

    /**
     * @return Collection|Position[]
     */
    public function getAllowedPositions()
    {
        return $this->allowedPositions;
    }

    public function addAllowedPosition(Position $allowedPosition): self
    {
        if (!$this->allowedPositions->contains($allowedPosition)) {
            $this->allowedPositions[] = $allowedPosition;
        }

        return $this;
    }

    public function removeAllowedPosition(Position $allowedPosition): self
    {
        if ($this->allowedPositions->contains($allowedPosition)) {
            $this->allowedPositions->removeElement($allowedPosition);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getVisitorCount(): int
    {
        return $this->visitorCount;
    }

    /**
     * @param int $visitorCount
     * @return Experience
     */
    public function setVisitorCount(int $visitorCount): Experience
    {
        $this->visitorCount = $visitorCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalVisitTime(): int
    {
        return $this->totalVisitTime;
    }

    /**
     * @param int $totalVisitTime
     * @return Experience
     */
    public function setTotalVisitTime(int $totalVisitTime): Experience
    {
        $this->totalVisitTime = $totalVisitTime;
        return $this;
    }

    /**
     * @return float
     */
    public function getRatingAverage(): float
    {
        return $this->ratingAverage;
    }

    /**
     * @param float $ratingAverage
     * @return Experience
     */
    public function setRatingAverage(float $ratingAverage): Experience
    {
        $this->ratingAverage = $ratingAverage;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Experience
     */
    public function setPrice(float $price): Experience
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return Experience
     */
    public function setCreated(\DateTime $created): Experience
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return Experience
     */
    public function setUpdated(\DateTime $updated): Experience
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     * @return Experience
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJourneys()
    {
        return $this->journeys;
    }

    /**
     * @param Journey $journey
     * @return Experience
     */
    public function addJourney(Journey $journey)
    {
        $this->journeys[] = $journey;
        return $this;
    }

    /**
     * @param Journey $journey
     * @return Experience
     */
    public function removeJourney(Journey $journey)
    {
        $this->journeys->removeElement($journey);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuests()
    {
        return $this->quests;
    }

    /**
     * @param Quest $quest
     * @return Experience
     */
    public function addQuest(Quest $quest)
    {
        $this->quests[] = $quest;
        return $this;
    }

    /**
     * @param Quest $quest
     * @return Experience
     */
    public function removeQuest(Quest $quest)
    {
        $this->quests->removeElement($quest);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return Experience
     */
    public function setStartDate(\DateTime $startDate): Experience
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Experience
     */
    public function setEndDate(\DateTime $endDate): Experience
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|ExperienceTeam[]
     */
    public function getExperienceTeams(): Collection
    {
        return $this->experienceTeams;
    }

    public function addExperienceTeam(ExperienceTeam $experienceTeam): self
    {
        if (!$this->experienceTeams->contains($experienceTeam)) {
            $this->experienceTeams[] = $experienceTeam;
            $experienceTeam->setExperience($this);
        }

        return $this;
    }

    public function removeExperienceTeam(ExperienceTeam $experienceTeam): self
    {
        if ($this->experienceTeams->contains($experienceTeam)) {
            $this->experienceTeams->removeElement($experienceTeam);
            // set the owning side to null (unless already changed)
            if ($experienceTeam->getExperience() === $this) {
                $experienceTeam->setExperience(null);
            }
        }

        return $this;
    }

    /**
     * @return AssetBundleMedia
     */
    public function getWorldPackageMedia()
    {
        return $this->worldPackageMedia;
    }

    /**
     * @param AssetBundleMedia $worldPackageMedia
     * @return Experience
     */
    public function setWorldPackageMedia($worldPackageMedia)
    {
        $this->worldPackageMedia = $worldPackageMedia;
        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @Groups({"experience:read"})
     * @ApiProperty(
     *     description="The status of the experience in string value.",
     *     attributes={
     *          "swagger_context"={
     *              "type"="string",
     *              "enum"={"Archived", "Not available", "Draft", "Test", "Available"}
     *          }
     *     }
     * )
     * @return string
     */
    public function getTextStatus()
    {
        return array_search($this->status, self::STATUS);
    }

    /**
     * @Groups({"experience"})
     * @ApiProperty(
     *     attributes={
     *          "swagger_context"={
     *              "type"="string",
     *              "enum"={"Archived", "Not available", "Draft", "Test", "Available"}
     *          }
     *     }
     * )
     * @param string $textStatus
     */
    public function setTextStatus(string $textStatus)
    {
        if (!array_key_exists($textStatus, self::STATUS)) {
            throw new BadRequestHttpException("The status is not part of the available values.");
        }

        $this->status = self::STATUS[$textStatus];
    }

    public function getPresentationVideo(): ?Media
    {
        return $this->presentationVideo;
    }

    public function setPresentationVideo(?Media $presentationVideo): self
    {
        $this->presentationVideo = $presentationVideo;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addExperience($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeExperience($this);
        }

        return $this;
    }

    /**
     * @return Collection|MiniGame[]
     */
    public function getMiniGames(): Collection
    {
        return $this->miniGames;
    }

    public function addMiniGame(MiniGame $miniGame): self
    {
        if (!$this->miniGames->contains($miniGame)) {
            $this->miniGames[] = $miniGame;
            $miniGame->setExperience($this);
        }

        return $this;
    }

    public function removeMiniGame(MiniGame $miniGame): self
    {
        if ($this->miniGames->contains($miniGame)) {
            $this->miniGames->removeElement($miniGame);
            // set the owning side to null (unless already changed)
            if ($miniGame->getExperience() === $this) {
                $miniGame->setExperience(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Spot[]
     */
    public function getSpots(): Collection
    {
        return $this->spots;
    }

    public function addSpot(Spot $spot): self
    {
        if (!$this->spots->contains($spot)) {
            $this->spots[] = $spot;
            $spot->setExperience($this);
        }

        return $this;
    }

    public function removeSpot(Spot $spot): self
    {
        if ($this->spots->contains($spot)) {
            $this->spots->removeElement($spot);
            // set the owning side to null (unless already changed)
            if ($spot->getExperience() === $this) {
                $spot->setExperience(null);
            }
        }

        return $this;
    }

    public function getExperienceObjectRadius(): ?float
    {
        return $this->experienceObjectRadius;
    }

    public function setExperienceObjectRadius(float $experienceObjectRadius): self
    {
        $this->experienceObjectRadius = $experienceObjectRadius;

        return $this;
    }
}