<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:29
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * ThreeDimensionMedia
 *
 * @ORM\Entity
 * @ORM\Table(name="three_dimension_medias")
 * @ApiResource()
 */
class ThreeDimensionMedia extends Media
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}