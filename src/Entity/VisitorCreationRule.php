<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     description="The rules to apply to a visitor when he joins the experience team.",
 * )
 * @ORM\Entity(repositoryClass="App\Repository\VisitorCreationRuleRepository")
 * @ORM\EntityListeners({
 *     "App\EntityListener\VisitorCreationRuleListener"
 * })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class VisitorCreationRule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExperienceTeam", inversedBy="visitorCreationRules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $experienceTeam;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @Assert\GreaterThanOrEqual(1)
     * @Assert\NotBlank()
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->quantity = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExperienceTeam(): ?ExperienceTeam
    {
        return $this->experienceTeam;
    }

    public function setExperienceTeam(?ExperienceTeam $experienceTeam): self
    {
        $this->experienceTeam = $experienceTeam;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
