<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 12/11/2018
 * Time: 16:47
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use App\Exception\BadRequestException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Item
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 * @ORM\Table(name="items")
 *
 * @ApiFilter(NumericFilter::class, properties={"id"})
 * @ApiResource(
 *     normalizationContext={
 *          "groups"={"item", "item:read" }
 *     },
 *     denormalizationContext={
 *          "groups"={"item", "item:write"}
 *     }
 * )
 *
 * //TODO  softdeletable
 */
class Item
{
    const TYPE_COLLECTION = 1;
    const TYPE_BAG = 2;
    const TYPE_CONSUMABLE = 3;
    const TYPE_INSTALLABLE = 4;

    const TYPES = [
        "Collection" => self::TYPE_COLLECTION,
        "Bag" => self::TYPE_BAG,
        "Consumable" => self::TYPE_CONSUMABLE,
        "Installable" => self::TYPE_INSTALLABLE
    ];

    /**
     * @var int
     * @Groups({"item:read", "experience:read", "visitorItem:read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name",type="string")
     * @Assert\Length(min = 3, max = 15)
     *
     * @Groups({"item", "experience:read"})
     *
     * @ApiProperty(description="The name of the Item.")
     */
    private $name;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="appearance_image_media_id", referencedColumnName="id")
     *
     * @Groups({"item", "experience:read"})
     *
     * @ApiProperty(description="The in-app inventory appearance of the Item.")
     */
    private $appearanceImageMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="appearance_object_media_id", referencedColumnName="id")
     *
     * @Groups({"item", "experience:read"})
     *
     * @ApiProperty(description="The in-app Three-D appearance of the Item.")
     */
    private $appearanceObjectMedia;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", options={"default" : 1})
     *
     * @Groups({"item", "experience:read"})
     *
     * @ApiProperty(description="The in-app inventory slot used by this Item. 1 unit by default.")
     *
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="multiple_use_count", type="integer", options={"default" : 1})
     * @Groups({"item", "experience:read"})
     * @Assert\Type(type="integer")
     * @ApiProperty(description="The number of use remaining for this visitor and item. One use by default.")
     *
     */
    private $multipleUseCount;

    /**
     * One Item has Many ItemActions
     * @ORM\OneToMany(targetEntity="App\Entity\ItemAction", mappedBy="item")
     *
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="The actions related to the item.")
     */
    private $actions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Experience", mappedBy="items")
     * @Groups({"item"})
     */
    private $experiences;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The start date from where the current item can be used.")
     * @Groups({"item", "experience:read"})
     */
    private $usePeriodStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The end date until the current item can be used.")
     * @Groups({"item", "experience:read"})
     */
    private $usePeriodEnd;

    /**
     * @var \DateTime
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="The start time in the day from where the item can be used.")
     * @ORM\Column(type="time", nullable=true)
     */
    private $usePeriodTimeStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="The end time in the day until the item cannot be used anylore.")
     */
    private $usePeriodTimeEnd;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="The period after activation during which the item is active.")
     */
    private $effectPeriod;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="The NFC tag for a general object that can be scanned by everyone. Can be null because it is a field associated with a specified case of ")
     */
    private $nfcCommonTag;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="Description of the item. Must be readable for the app's user.")
     */
    private $description;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="Flag used to indicate if the item can be trade with another item between two visitors")
     */
    private $isTradeable;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="Flag used to indicate whether the item must be shown only in the experience")
     */
    private $isExperienceOnly;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"item", "experience:read"})
     * @ApiProperty(description="Type number to know if the item is a usable item, is part of the collection, etc")
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->isTradeable = true;
        $this->isExperienceOnly = false;
        $this->weight = 1;
        $this->multipleUseCount = 1;
        $this->experiences = new ArrayCollection();
        $this->actions = new ArrayCollection();
        $this->type = self::TYPE_CONSUMABLE;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Item
     */
    public function setId(int $id): Item
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Media
     */
    public function getAppearanceImageMedia()
    {
        return $this->appearanceImageMedia;
    }

    /**
     * @param Media $appearanceImageMedia
     * @return Item
     */
    public function setAppearanceImageMedia(Media $appearanceImageMedia)
    {
        $this->appearanceImageMedia = $appearanceImageMedia;
        return $this;
    }

    /**
     * @return Media
     */
    public function getAppearanceObjectMedia()
    {
        return $this->appearanceObjectMedia;
    }

    /**
     * @param Media $appearanceObjectMedia
     * @return Item
     */
    public function setAppearanceObjectMedia(Media $appearanceObjectMedia)
    {
        $this->appearanceObjectMedia = $appearanceObjectMedia;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return Item
     */
    public function setWeight(int $weight): Item
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @param \DateTime $usePeriod
     * @return Item
     */
    public function setUsePeriod(\DateTime $usePeriod): Item
    {
        $this->usePeriod = $usePeriod;
        return $this;
    }

    /**
     * @param \DateTime $usePeriodTime
     * @return Item
     */
    public function setUsePeriodTime(\DateTime $usePeriodTime): Item
    {
        $this->usePeriodTime = $usePeriodTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getMultipleUseCount(): int
    {
        return $this->multipleUseCount;
    }

    /**
     * @param int $multipleUseCount
     * @return Item
     */
    public function setMultipleUseCount(int $multipleUseCount): Item
    {
        $this->multipleUseCount = $multipleUseCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getMultipleUseCountdown(): int
    {
        return $this->multipleUseCountdown;
    }

    /**
     * @param int $multipleUseCountdown
     * @return Item
     */
    public function setMultipleUseCountdown(int $multipleUseCountdown): Item
    {
        $this->multipleUseCountdown = $multipleUseCountdown;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param mixed $actions
     * @return Item
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
            $experience->addItem($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->contains($experience)) {
            $this->experiences->removeElement($experience);
            $experience->removeItem($this);
        }

        return $this;
    }

    public function getUsePeriodStart(): ?\DateTimeInterface
    {
        return $this->usePeriodStart;
    }

    public function setUsePeriodStart(?\DateTimeInterface $usePeriodStart): self
    {
        $this->usePeriodStart = $usePeriodStart;

        return $this;
    }

    public function getUsePeriodEnd(): ?\DateTimeInterface
    {
        return $this->usePeriodEnd;
    }

    public function setUsePeriodEnd(?\DateTimeInterface $usePeriodEnd): self
    {
        $this->usePeriodEnd = $usePeriodEnd;

        return $this;
    }

    public function getUsePeriodTimeStart(): ?\DateTimeInterface
    {
        return $this->usePeriodTimeStart;
    }

    public function setUsePeriodTimeStart(?\DateTimeInterface $usePeriodTimeStart): self
    {
        $this->usePeriodTimeStart = $usePeriodTimeStart;

        return $this;
    }

    public function getUsePeriodTimeEnd(): ?\DateTimeInterface
    {
        return $this->usePeriodTimeEnd;
    }

    public function setUsePeriodTimeEnd(?\DateTimeInterface $usePeriodTimeEnd): self
    {
        $this->usePeriodTimeEnd = $usePeriodTimeEnd;

        return $this;
    }

    public function getEffectPeriod(): ?int
    {
        return $this->effectPeriod;
    }

    public function setEffectPeriod(int $effectPeriod): self
    {
        $this->effectPeriod = $effectPeriod;

        return $this;
    }

    public function getNfcCommonTag(): ?string
    {
        return $this->nfcCommonTag;
    }

    public function setNfcCommonTag(?string $nfcCommonTag): self
    {
        $this->nfcCommonTag = $nfcCommonTag;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsTradeable(): ?bool
    {
        return $this->isTradeable;
    }

    public function setIsTradeable(?bool $isTradeable): self
    {
        $this->isTradeable = $isTradeable;

        return $this;
    }

    public function getIsExperienceOnly(): ?bool
    {
        return $this->isExperienceOnly;
    }

    public function setIsExperienceOnly(?bool $isExperienceOnly): self
    {
        $this->isExperienceOnly = $isExperienceOnly;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @Groups({"item:read", "experience:read"})
     * @ApiProperty(
     *     description="The textual representation of the type of item",
     *     attributes={
     *          "swagger_context"={
     *              "type"="string",
     *              "enum"={"Consumable", "Installable", "Bag", "Collection"}
     *          }
     *     }
     * )
     * @return string
     */
    public function getTextType()
    {
        return array_search($this->type, self::TYPES);
    }

    /**
     * @Groups({"item", "experience:read"})
     * @ApiProperty(
     *     attributes={
     *          "swagger_context"={
     *              "type"="string",
     *              "enum"={"Consumable", "Installable", "Bag", "Collection"}
     *          }
     *     }
     * )
     * @param string $textType
     */
    public function setTextType(string $textType)
    {
        if (!array_key_exists($textType, self::TYPES)) {
            throw new BadRequestException("The type is not part of the available values.");
        }

        $this->type = self::TYPES[$textType];
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}