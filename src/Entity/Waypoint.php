<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity
 * @ORM\Table(name="waypoints")
 * @ApiResource()
 */
class Waypoint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MiniGame", mappedBy="waypoints")
     */
    private $miniGames;

    public function __construct()
    {
        $this->miniGames = new ArrayCollection();
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|MiniGame[]
     */
    public function getMiniGames(): Collection
    {
        return $this->miniGames;
    }

    public function addMiniGame(MiniGame $miniGame): self
    {
        if (!$this->miniGames->contains($miniGame)) {
            $this->miniGames[] = $miniGame;
            $miniGame->addWaypoint($this);
        }

        return $this;
    }

    public function removeMiniGame(MiniGame $miniGame): self
    {
        if ($this->miniGames->contains($miniGame)) {
            $this->miniGames->removeElement($miniGame);
            $miniGame->removeWaypoint($this);
        }

        return $this;
    }
}