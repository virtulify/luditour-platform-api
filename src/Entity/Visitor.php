<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:30
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Visitor
 *
 * @ORM\Entity(repositoryClass="App\Repository\VisitorRepository")
 * @ORM\Table(name="visitors")
 * @ApiResource(
 *     normalizationContext={"groups"={"visitor", "visitor:read", "experience:read"}},
 *     denormalizationContext={"groups"={"visitor", "experience:write"}},
 *     description="The visitor/player entity for all experiences.",
 *     itemOperations={"put", "delete"},
 *     collectionOperations={}
 * )
 */
class Visitor implements UserInterface
{
    const FEMALE = 1;
    const MALE = 2;
    const OTHER = 3;

    /**
     * @var int
     *
     * @Groups({"visitor:read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * //sortable
     * @Groups({"visitor", "experienceObject:read"})
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     *
     * //sortable
     * @Groups({"visitor"})
     * @ORM\Column(name="mail", type="string", nullable=true)
     */
    private $mail;

    /**
     * @Groups({"visitor:read"})
     * @ORM\Column(name="facebook_auth", type="string", length=191, nullable=true, unique=true)
     */
    private $facebookAuth;

    /**
     * @Groups({"visitor:read"})
     * @ORM\Column(name="google_auth", type="string", length=191, nullable=true, unique=true)
     */
    private $googleAuth;

    /**
     * @var double
     * @Groups({"visitor"})
     * @ORM\Column(name="scope_min_lat", type="decimal", precision=20, scale=17, nullable=true)
     * @Assert\Type(type="double")
     */
    private $scopeMinLat;

    /**
     * @var double
     * @Groups({"visitor"})
     * @ORM\Column(name="scope_max_lat", type="decimal", precision=20, scale=17, nullable=true)
     * @Assert\Type(type="double")
     */
    private $scopeMaxLat;

    /**
     * @var double
     * @Groups({"visitor"})
     * @ORM\Column(name="scope_min_long", type="decimal", precision=20, scale=17, nullable=true)
     * @Assert\Type(type="double")
     */
    private $scopeMinLong;

    /**
     * @var double
     * @Groups({"visitor"})
     * @ORM\Column(name="scope_max_long", type="decimal", precision=20, scale=17, nullable=true)
     * @Assert\Type(type="double")
     */
    private $scopeMaxLong;

    /**
     * One Visitor have Many allowed Visitor Lines.
     * @Groups({"visitor:read"})
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorLine", mappedBy="visitor", fetch="EXTRA_LAZY")
     * @ApiSubresource()
     */
    private $visitorLines;

    /**
     * @var integer
     * @Groups({"visitor"})
     * @ORM\Column(name="score", type="integer", options={"default" : 0})
     *
     */
    private $score;

    /**
     * @var integer
     * @Groups({"visitor"})
     * @ORM\Column(name="level", type="integer", options={"default" : 0})
     *
     */
    private $level;

    /**
     * @Groups({"visitor"})
     * @ORM\ManyToOne(targetEntity="App\Entity\AvatarMedia")
     * @ORM\JoinColumn(name="avatar_media_id", referencedColumnName="id", nullable=true)
     */
    private $avatarMedia;

    /**
     * @var string
     * @Groups({"visitor"})
     * @ORM\Column(name="current_map", type="text", nullable=true)
     */
    private $currentMap;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorItem", mappedBy="visitor", orphanRemoval=true)
     * @Groups({"visitor:read"})
     */
    private $visitorItems;

    /**
     * @ORM\Column(type="float")
     * @Groups({"visitor:read"})
     */
    private $kilometersWalked;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"visitor"})
     */
    private $sex;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"visitor"})
     */
    private $birthDate;

    /**
     * @var \DateTime
     * @Groups({"visitor:read"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $fbAccessToken;

    /**
     * @ApiProperty(description="The list of visitors that are friends with the current visitor in the application.")
     * @ORM\ManyToMany(targetEntity="App\Entity\Visitor", inversedBy="friends")
     * @Groups({"visitor"})
     */
    private $friends;

    public function __construct()
    {
        $this->visitorLines = new ArrayCollection();
        $this->score = 0;
        $this->level = 0;
        $this->kilometersWalked = 0;
        $this->visitorItems = new ArrayCollection();
        $this->lastUpdate = new \DateTime();
        $this->friends = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Visitor
     */
    public function setId(int $id): Visitor
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Visitor
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     * @return Visitor
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebookAuth()
    {
        return $this->facebookAuth;
    }

    /**
     * @param mixed $facebookAuth
     * @return Visitor
     */
    public function setFacebookAuth($facebookAuth)
    {
        $this->facebookAuth = $facebookAuth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleAuth()
    {
        return $this->googleAuth;
    }

    /**
     * @param mixed $googleAuth
     * @return Visitor
     */
    public function setGoogleAuth($googleAuth)
    {
        $this->googleAuth = $googleAuth;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getScopeMinLat()
    {
        return $this->scopeMinLat;
    }

    /**
     * @param float $scopeMinLat
     * @return Visitor
     */
    public function setScopeMinLat(float $scopeMinLat): Visitor
    {
        $this->scopeMinLat = $scopeMinLat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getScopeMaxLat()
    {
        return $this->scopeMaxLat;
    }

    /**
     * @param float $scopeMaxLat
     * @return Visitor
     */
    public function setScopeMaxLat(float $scopeMaxLat): Visitor
    {
        $this->scopeMaxLat = $scopeMaxLat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getScopeMinLong()
    {
        return $this->scopeMinLong;
    }

    /**
     * @param float $scopeMinLong
     * @return Visitor
     */
    public function setScopeMinLong(float $scopeMinLong): Visitor
    {
        $this->scopeMinLong = $scopeMinLong;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getScopeMaxLong()
    {
        return $this->scopeMaxLong;
    }

    /**
     * @param float $scopeMaxLong
     * @return Visitor
     */
    public function setScopeMaxLong(float $scopeMaxLong): Visitor
    {
        $this->scopeMaxLong = $scopeMaxLong;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVisitorLines()
    {
        return $this->visitorLines;
    }

    /**
     * @param mixed $visitorLines
     * @return Visitor
     */
    public function setVisitorLines($visitorLines)
    {
        $this->visitorLines = $visitorLines;
        return $this;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return Visitor
     */
    public function setScore(int $score): Visitor
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return Visitor
     */
    public function setLevel(int $level): Visitor
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatarMedia()
    {
        return $this->avatarMedia;
    }

    /**
     * @param mixed $avatarMedia
     * @return Visitor
     */
    public function setAvatarMedia($avatarMedia)
    {
        $this->avatarMedia = $avatarMedia;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrentMap()
    {
        return $this->currentMap;
    }

    /**
     * @param string $currentMap
     * @return Visitor
     */
    public function setCurrentMap(string $currentMap): Visitor
    {
        $this->currentMap = $currentMap;
        return $this;
    }

    /**
     * Returns the current visitor line data the visitor is playing in mobile app
     * @return VisitorLine|null
     */
    public function getCurrentVisitorLine()
    {
        /**
         * @var $visitorLine VisitorLine
         */
        foreach ($this->visitorLines as $visitorLine) {
            if ($visitorLine->isCurrent()) {
                return $visitorLine;
            }
        }
        return null;
    }


    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return '';
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return '';
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|VisitorItem[]
     */
    public function getVisitorItems(): Collection
    {
        return $this->visitorItems;
    }

    public function addVisitorItem(VisitorItem $visitorItem): self
    {
        if (!$this->visitorItems->contains($visitorItem)) {
            $this->visitorItems[] = $visitorItem;
            $visitorItem->setVisitor($this);
        }

        return $this;
    }

    public function removeVisitorItem(VisitorItem $visitorItem): self
    {
        if ($this->visitorItems->contains($visitorItem)) {
            $this->visitorItems->removeElement($visitorItem);
            // set the owning side to null (unless already changed)
            if ($visitorItem->getVisitor() === $this) {
                $visitorItem->setVisitor(null);
            }
        }

        return $this;
    }

    public function getKilometersWalked(): ?float
    {
        return $this->kilometersWalked;
    }

    public function setKilometersWalked(float $kilometersWalked): self
    {
        $this->kilometersWalked = $kilometersWalked;

        return $this;
    }

    public function getSex(): ?int
    {
        return $this->sex;
    }

    public function setSex(?int $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(?\DateTimeInterface $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    public function getFbAccessToken(): ?string
    {
        return $this->fbAccessToken;
    }

    public function setFbAccessToken(?string $fbAccessToken): self
    {
        $this->fbAccessToken = $fbAccessToken;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFriends(): Collection
    {
        return $this->friends;
    }

    public function addFriend(self $friend): self
    {
        if (!$this->friends->contains($friend)) {
            $this->friends[] = $friend;
        }

        return $this;
    }

    public function removeFriend(self $friend): self
    {
        if ($this->friends->contains($friend)) {
            $this->friends->removeElement($friend);
        }

        return $this;
    }
}