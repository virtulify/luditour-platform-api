<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 08/11/2018
 * Time: 12:01
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer
 *
 * @ORM\Entity
 * @ORM\Table(name="customers")
 * @ApiResource()
 */
class Customer extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}