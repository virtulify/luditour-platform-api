<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:29
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Journey
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 * @ORM\Table(name="medias")
 * @ApiResource(
 *     description="The experience loaded in the Luditour App when it starts.",
 *     normalizationContext={"groups"={"media", "media:read"}},
 *     denormalizationContext={"groups"={"media", "media:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     itemOperations={
 *          "get"
 *     }
 * )
 *
 * @Vich\Uploadable
 */
class Media
{
    /**
     * @var int
     *
     * @Groups({"media:read", "experience:read", "generationRule:read", "assetBundleMedia:read", "item:read"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @Groups({"media:read"})
     *
     * @ORM\Column(name="hash_code",type="string", nullable=true)
     */
    protected $hashCode;

    /**
     * @var File|null
     * @Vich\UploadableField(
     *     mapping="media",
     *     fileNameProperty="filename",
     *     size="diskUsage"
     * )
     */
    public $file;

    /**
     * @var int
     *
     * @Groups({"media"})
     *
     * @ORM\Column(type="integer")
     */
    protected $diskUsage;

    /**
     * @var string
     *
     * @Groups({"media", "experience:read", "generationRule:read", "assetBundleMedia:read", "item:read"})
     *
     * @ORM\Column(type="string")
     */
    protected $filename;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getHashCode()
    {
        return $this->hashCode;
    }

    /**
     * @param string|null $hashCode
     * @return Media
     */
    public function setHashCode(string $hashCode): Media
    {
        $this->hashCode = $hashCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiskUsage(): int
    {
        return $this->diskUsage;
    }

    /**
     * @param int $diskUsage
     * @return Media
     */
    public function setDiskUsage(int $diskUsage): Media
    {
        $this->diskUsage = $diskUsage;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return Media
     */
    public function setFilename(string $filename): Media
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     *
     * @Groups({"media:read", "experience:read", "generationRule:read", "assetBundleMedia:read", "item:read"})
     *
     */
    public function getWebPath()
    {
        // if the string begins by asb://, then it is a link to an asset bundle
        return substr($this->filename, 0, 6) === "asb://" ? $this->filename : $_SERVER['SERVER_NAME'] . '/media/' . $this->filename;
    }

    /**
     * @return string
     *
     * @Groups({"media:read", "experience:read", "generationRule:read", "assetBundleMedia:read"})
     *
     * @throws \ReflectionException
     */
    public function getType()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}