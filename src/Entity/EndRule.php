<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity(repositoryClass="App\Repository\EndRuleRepository")
 * @ApiResource(
 *     description="The rules describing the resulting consequences of the mini game depending on the score of the player"
 * )
 */
class EndRule
{
    const CONDITION_EQ = 1; // equals
    const CONDITION_GT = 2; // Greater than
    const CONDITION_GTE = 3; // Greater than or equals
    const CONDITION_LT = 4; // Lower than
    const CONDITION_LTE = 5; // Lower than or equals

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ApiProperty(description="The score to have to trigger the current end rule")
     */
    private $scoreCondition;

    /**
     * @var MiniGame
     * @ORM\ManyToOne(targetEntity="App\Entity\MiniGame", inversedBy="endRules")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The related mini game")
     */
    private $miniGame;

    /**
     * @var int
     * @ApiProperty(description="The score won after the mini game")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $scoreWon;

    /**
     * @var int
     * @ApiProperty(description="The score lost after the mini game")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $scoreLost;

    /**
     * @ApiProperty(description="The items won after the mini game")
     * @ORM\ManyToMany(targetEntity="App\Entity\Item")
     * @ORM\JoinTable(name="end_rule_item_won",
     *     joinColumns={@ORM\JoinColumn(name="end_rule_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="item_id", referencedColumnName="id")})
     */
    private $wonItems;

    /**
     * @ApiProperty(description="The items lost after the mini game")
     * @ORM\ManyToMany(targetEntity="App\Entity\Item")
     * @ORM\JoinTable(name="end_rule_item_lost",
     *     joinColumns={@ORM\JoinColumn(name="end_rule_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="item_id", referencedColumnName="id")})
     */
    private $lostItems;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="Soft delete field")
     */
    private $deletedAt;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ApiProperty(description="Defines the condition type used to trigger the end rule.")
     */
    private $conditionType;

    public function __construct()
    {
        $this->wonItems = new ArrayCollection();
        $this->lostItems = new ArrayCollection();
        $this->scoreCondition = self::CONDITION_EQ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScoreCondition(): ?float
    {
        return $this->scoreCondition;
    }

    public function setScoreCondition(float $scoreCondition): self
    {
        $this->scoreCondition = $scoreCondition;

        return $this;
    }

    public function getMiniGame(): ?MiniGame
    {
        return $this->miniGame;
    }

    public function setMiniGame(?MiniGame $miniGame): self
    {
        $this->miniGame = $miniGame;

        return $this;
    }

    public function getScoreWon(): ?float
    {
        return $this->scoreWon;
    }

    public function setScoreWon(?float $scoreWon): self
    {
        $this->scoreWon = $scoreWon;

        return $this;
    }

    public function getScoreLost(): ?float
    {
        return $this->scoreLost;
    }

    public function setScoreLost(?float $scoreLost): self
    {
        $this->scoreLost = $scoreLost;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getWonItems(): Collection
    {
        return $this->wonItems;
    }

    public function addWonItem(Item $wonItem): self
    {
        if (!$this->wonItems->contains($wonItem)) {
            $this->wonItems[] = $wonItem;
        }

        return $this;
    }

    public function removeWonItem(Item $wonItem): self
    {
        if ($this->wonItems->contains($wonItem)) {
            $this->wonItems->removeElement($wonItem);
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getLostItems(): Collection
    {
        return $this->lostItems;
    }

    public function addLostItem(Item $lostItem): self
    {
        if (!$this->lostItems->contains($lostItem)) {
            $this->lostItems[] = $lostItem;
        }

        return $this;
    }

    public function removeLostItem(Item $lostItem): self
    {
        if ($this->lostItems->contains($lostItem)) {
            $this->lostItems->removeElement($lostItem);
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getConditionType(): ?int
    {
        return $this->conditionType;
    }

    public function setConditionType(int $conditionType): self
    {
        $this->conditionType = $conditionType;

        return $this;
    }
}
