<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:09
 *
 * Generator class
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Spot
 *
 * @ORM\Entity(repositoryClass="App\Repository\SpotRepository")
 * @ORM\Table(name="spots")
 * @ApiResource()
 */
class Spot
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name",type="string")
     * @Assert\Length(min = 3, max = 15)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     * @Assert\Range(min=1, max=30)
     *
     */
    private $type;

    /**
     * @var double
     *
     * @ORM\Column(name="latitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $latitude;

    /**
     * @var double
     *
     * @ORM\Column(name="longitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $longitude;

    /**
     * @var double
     *
     * @ORM\Column(name="altitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $altitude;

    /**
     * @var float
     *
     * @ORM\Column(name="range", type="float")
     */
    private $range;

    /**
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    private $private;

    /**
     * @var string
     *
     * @ORM\Column(name="weather", type="string", nullable=true)
     *
     */
    private $weather;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MiniGame", mappedBy="spots")
     */
    private $miniGames;

    /**
     * @var Experience
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="spots")
     */
    private $experience;

    public function __construct()
    {
        $this->miniGames = new ArrayCollection();
        $this->private = true;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Spot
     */
    public function setId(int $id): Spot
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Spot
     */
    public function setName(string $name): Spot
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Spot
     */
    public function setType(int $type): Spot
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return Spot
     */
    public function setLatitude(float $latitude): Spot
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return Spot
     */
    public function setLongitude(float $longitude): Spot
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getAltitude(): float
    {
        return $this->altitude;
    }

    /**
     * @param float $altitude
     * @return Spot
     */
    public function setAltitude(float $altitude): Spot
    {
        $this->altitude = $altitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getRange(): float
    {
        return $this->range;
    }

    /**
     * @param float $range
     * @return Spot
     */
    public function setRange(float $range): Spot
    {
        $this->range = $range;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     * @return Spot
     */
    public function setPrivate(bool $private): Spot
    {
        $this->private = $private;
        return $this;
    }

    /**
     * @return string
     */
    public function getWeather(): string
    {
        return $this->weather;
    }

    /**
     * @param string $weather
     * @return Spot
     */
    public function setWeather(string $weather): Spot
    {
        $this->weather = $weather;
        return $this;
    }

    /**
     * @return Collection|MiniGame[]
     */
    public function getMiniGames(): Collection
    {
        return $this->miniGames;
    }

    public function addMiniGame(MiniGame $miniGame): self
    {
        if (!$this->miniGames->contains($miniGame)) {
            $this->miniGames[] = $miniGame;
            $miniGame->addSpot($this);
        }

        return $this;
    }

    public function removeMiniGame(MiniGame $miniGame): self
    {
        if ($this->miniGames->contains($miniGame)) {
            $this->miniGames->removeElement($miniGame);
            $miniGame->removeSpot($this);
        }

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

}