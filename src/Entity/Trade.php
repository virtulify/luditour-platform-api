<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiResource(
 *     description="Represents a trade of visitor items between two visitors. In the application, this is a simple transfer from the source visitor to the destination visitor of every donations."
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TradeRepository")
 * @ORM\EntityListeners({
 *     "App\EntityListener\TradeListener"
 * })
 */
class Trade
{
    const STATE_ASKED = 0;
    const STATE_DENIED = 1;
    const STATE_ACCEPTED = 2;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The last update date kept up to date")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="Soft deleteable field")
     */
    private $deletedAt;

    /**
     * @var Visitor
     * @ORM\ManyToOne(targetEntity="App\Entity\Visitor", inversedBy="trades")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The visitor that transfers the donations to the destination visitor")
     */
    private $sourceVisitor;

    /**
     * @var Visitor
     * @ORM\ManyToOne(targetEntity="App\Entity\Visitor", inversedBy="trades")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The visitor that receives the donations from the source visitor")
     */
    private $destinationVisitor;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\VisitorItem")
     * @ApiProperty(description="The visitor items of the source visitor to transfer to the destination visitor")
     */
    private $donations;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ApiProperty(description="The state of the trade. Can be 0 for 'Asked' (the source visitor has sent a trade request), 1 for 'Denied' (the destination visitor does not want to receive the items) and 2 for 'Accepted' (the destination receives the donations)")
     */
    private $state;

    public function __construct()
    {
        $this->donations = new ArrayCollection();
        $this->updatedAt = new \DateTime();
        $this->state = self::STATE_ASKED;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getSourceVisitor(): ?Visitor
    {
        return $this->sourceVisitor;
    }

    public function setSourceVisitor(?Visitor $sourceVisitor): self
    {
        $this->sourceVisitor = $sourceVisitor;

        return $this;
    }

    public function getDestinationVisitor(): ?Visitor
    {
        return $this->destinationVisitor;
    }

    public function setDestinationVisitor(?Visitor $destinationVisitor): self
    {
        $this->destinationVisitor = $destinationVisitor;

        return $this;
    }

    /**
     * @return Collection|VisitorItem[]
     */
    public function getDonations(): Collection
    {
        return $this->donations;
    }

    public function addDonations(VisitorItem $donations): self
    {
        if (!$this->donations->contains($donations)) {
            $this->donations[] = $donations;
        }

        return $this;
    }

    public function removeDonations(VisitorItem $donations): self
    {
        if ($this->donations->contains($donations)) {
            $this->donations->removeElement($donations);
        }

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }
}
