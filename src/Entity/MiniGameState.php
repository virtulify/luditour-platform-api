<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator as CustomAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiResource(
 *     description="The state of an instance of the mini game for one of the players that has played the mini game in a specified moment.",
 *     normalizationContext={"groups"={"miniGameState", "miniGameState:read"}},
 *     denormalizationContext={"groups"={"miniGameState", "miniGameState:write"}},
 *     collectionOperations={"POST"},
 *     itemOperations={"GET"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MiniGameStateRepository")
 * @ORM\EntityListeners({
 *     "App\EntityListener\MiniGameStateListener"
 * })
 * @CustomAssert\MiniGameStateConstraint()
 */
class MiniGameState
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"miniGameState:read"})
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="Soft delete field")
     * @Groups({"miniGameState:read"})
     */
    private $deletedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @ApiProperty(description="The date when the player has played the mini game")
     * @Groups({"miniGameState:read"})
     */
    private $lastTimePlayed;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @ApiProperty(description="The score performed by the player")
     * @Groups({"miniGameState"})
     */
    private $scorePerformed;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @ApiProperty(description="The score won by the player")
     * @Groups({"miniGameState:read"})
     */
    private $scoreWon;

    /**
     * @var int
     * @ApiProperty(description="The score lost by the player")
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"miniGameState:read"})
     */
    private $scoreLost;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorItem", mappedBy="winningMiniGameState")
     * @ApiProperty(description="The items won at that instance of the minigame")
     * @Groups({"miniGameState:read"})
     */
    private $itemsWon;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorItem", mappedBy="losingMiniGameState")
     * @ApiProperty(description="The visitor items that the player has lost in this mini game")
     * @Groups({"miniGameState:read"})
     */
    private $itemsLost;

    /**
     * @var MiniGame
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\MiniGame", inversedBy="miniGameStates")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The related mini game")
     * @Groups({"miniGameState"})
     */
    private $miniGame;

    /**
     * @var VisitorLine
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\VisitorLine", inversedBy="miniGameStates")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The visitor line that has played the mini game")
     * @Groups({"miniGameState"})
     */
    private $player;

    /**
     * @ApiProperty(description="The items consumed after participation of the mini game.")
     * @ORM\OneToMany(targetEntity="App\Entity\VisitorItem", mappedBy="consumingMiniGameState")
     * @Groups({"miniGameState:read"})
     */
    private $itemsConsumed;

    public function __construct()
    {
        $this->scoreLost = 0;
        $this->scoreWon = 0;
        $this->scorePerformed = 0;
        $this->lastTimePlayed = new \DateTime();
        $this->itemsWon = new ArrayCollection();
        $this->itemsLost = new ArrayCollection();
        $this->itemsConsumed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getLastTimePlayed(): ?\DateTimeInterface
    {
        return $this->lastTimePlayed;
    }

    public function setLastTimePlayed(\DateTimeInterface $lastTimePlayed): self
    {
        $this->lastTimePlayed = $lastTimePlayed;

        return $this;
    }

    public function getScorePerformed(): ?float
    {
        return $this->scorePerformed;
    }

    public function setScorePerformed(?float $scorePerformed): self
    {
        $this->scorePerformed = $scorePerformed;

        return $this;
    }

    public function getScoreWon(): ?float
    {
        return $this->scoreWon;
    }

    public function setScoreWon(?float $scoreWon): self
    {
        $this->scoreWon = $scoreWon;

        return $this;
    }

    public function getScoreLost(): ?float
    {
        return $this->scoreLost;
    }

    public function setScoreLost(?float $scoreLost): self
    {
        $this->scoreLost = $scoreLost;

        return $this;
    }

    /**
     * @return Collection|VisitorItem[]
     */
    public function getItemsWon(): Collection
    {
        return $this->itemsWon;
    }

    public function addItemsWon(VisitorItem $itemsWon): self
    {
        if (!$this->itemsWon->contains($itemsWon)) {
            $this->itemsWon[] = $itemsWon;
            $itemsWon->setWinningMiniGameState($this);
        }

        return $this;
    }

    public function removeItemsWon(VisitorItem $itemsWon): self
    {
        if ($this->itemsWon->contains($itemsWon)) {
            $this->itemsWon->removeElement($itemsWon);
            // set the owning side to null (unless already changed)
            if ($itemsWon->getWinningMiniGameState() === $this) {
                $itemsWon->setWinningMiniGameState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VisitorItem[]
     */
    public function getItemsLost(): Collection
    {
        return $this->itemsLost;
    }

    public function addItemsLost(VisitorItem $itemsLost): self
    {
        if (!$this->itemsLost->contains($itemsLost)) {
            $this->itemsLost[] = $itemsLost;
            $itemsLost->setLosingMiniGameState($this);
        }

        return $this;
    }

    public function removeItemsLost(VisitorItem $itemsLost): self
    {
        if ($this->itemsLost->contains($itemsLost)) {
            $this->itemsLost->removeElement($itemsLost);
            // set the owning side to null (unless already changed)
            if ($itemsLost->getLosingMiniGameState() === $this) {
                $itemsLost->setLosingMiniGameState(null);
            }
        }

        return $this;
    }

    public function getMiniGame(): ?MiniGame
    {
        return $this->miniGame;
    }

    public function setMiniGame(?MiniGame $miniGame): self
    {
        $this->miniGame = $miniGame;

        return $this;
    }

    public function getPlayer(): ?VisitorLine
    {
        return $this->player;
    }

    public function setPlayer(?VisitorLine $player): self
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return Collection|VisitorItem[]
     */
    public function getItemsConsumed(): Collection
    {
        return $this->itemsConsumed;
    }

    public function addItemsConsumed(VisitorItem $itemsConsumed): self
    {
        if (!$this->itemsConsumed->contains($itemsConsumed)) {
            $this->itemsConsumed[] = $itemsConsumed;
            $itemsConsumed->setConsumingMiniGameState($this);
        }

        return $this;
    }

    public function removeItemsConsumed(VisitorItem $itemsConsumed): self
    {
        if ($this->itemsConsumed->contains($itemsConsumed)) {
            $this->itemsConsumed->removeElement($itemsConsumed);
            // set the owning side to null (unless already changed)
            if ($itemsConsumed->getConsumingMiniGameState() === $this) {
                $itemsConsumed->setConsumingMiniGameState(null);
            }
        }

        return $this;
    }
}
