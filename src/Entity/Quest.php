<?php
/**
 * A rule is a specific rule generation of spot for a journey
 * E.g : Generate water pokemon for all water places in Lille
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:27
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Quests
 *
 * @ORM\Entity(repositoryClass="App\Repository\QuestRepository")
 * @ORM\Table(name="quests")
 * @ApiResource()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Quest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    // Cultural places,market places, ...
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="start_date")
     * @Assert\Type(type="DateTime")
     */
    private $startDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="end_date",nullable=true)
     * @Assert\Type(type="DateTime")
     */
    private $endDate;

    /**
     * The hour of day of starting the rule
     * @var \DateTime
     * @ORM\Column(type="time", name="start_period_time",nullable=true)
     * @Assert\Type(type="DateTime")
     */
    private $startPeriodTime;

    /**
     * The hour of day of ending the rule
     * @var \DateTime
     * @ORM\Column(type="time", name="end_period_time",nullable=true)
     * @Assert\Type(type="DateTime")
     */
    private $endPeriodTime;

    /**
     * @var int
     * If 0 => night and day, if 1 day only and 2 night only
     *
     * @ORM\Column(name="period_type", type="integer")
     */
    private $periodType;

    /**
     * @var int
     * if 0 => all types of weather, if 1 = sunny
     *
     * @ORM\Column(name="weather_type", type="integer")
     */
    private $weatherType;

    /**
     * Many Journey have Many Spots.
     * @ORM\ManyToMany(targetEntity="App\Entity\Spot")
     * @ORM\JoinTable(name="quest_spots",
     *      joinColumns={@ORM\JoinColumn(name="quest_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="spot_id", referencedColumnName="id")}
     *      )
     */
    private $spots;

    /**
     * @var boolean
     *
     * @ORM\Column(name="spot_visible", type="boolean")
     */
    private $spotVisible;

    /**
     * @var Experience
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="quests")
     */
    private $experience;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->spots = new ArrayCollection();
        $this->weatherType = 0;
        $this->spotVisible = true;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Quest
     */
    public function setId(int $id): Quest
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Quest
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return Quest
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     * @return Quest
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartPeriodTime()
    {
        return $this->startPeriodTime;
    }

    /**
     * @param mixed $startPeriodTime
     * @return Quest
     */
    public function setStartPeriodTime($startPeriodTime)
    {
        $this->startPeriodTime = $startPeriodTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndPeriodTime()
    {
        return $this->endPeriodTime;
    }

    /**
     * @param mixed $endPeriodTime
     * @return Quest
     */
    public function setEndPeriodTime($endPeriodTime)
    {
        $this->endPeriodTime = $endPeriodTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getPeriodType(): int
    {
        return $this->periodType;
    }

    /**
     * @param int $periodType
     * @return Quest
     */
    public function setPeriodType(int $periodType): Quest
    {
        $this->periodType = $periodType;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeatherType(): int
    {
        return $this->weatherType;
    }

    /**
     * @param int $weatherType
     * @return Quest
     */
    public function setWeatherType(int $weatherType): Quest
    {
        $this->weatherType = $weatherType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpots()
    {
        return $this->spots;
    }

    /**
     * @param mixed $spots
     * @return Quest
     */
    public function setSpots($spots)
    {
        $this->spots = $spots;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSpotVisible(): bool
    {
        return $this->spotVisible;
    }

    /**
     * @param bool $spotVisible
     * @return Quest
     */
    public function setSpotVisible(bool $spotVisible): Quest
    {
        $this->spotVisible = $spotVisible;
        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(Experience $experience): self
    {
        $this->experience = $experience;
        return $this;
    }
}