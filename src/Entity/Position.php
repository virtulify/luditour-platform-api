<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:33
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Journey
 *
 * @ORM\Entity(repositoryClass="App\Repository\PositionRepository")
 * @ORM\Table(name="positions")
 * @ApiResource()
 */
class Position
{
    /**
     * @var int
     * @Groups({"experience:read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="latitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $latitude;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="longitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $longitude;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="altitude", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $altitude;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="scope_min_lat", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="float")
     */
    private $scopeMinLat;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="scope_max_lat", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="float")
     */
    private $scopeMaxLat;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="scope_min_long", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="float")
     */
    private $scopeMinLong;

    /**
     * @var double
     * @Groups({"experience:read"})
     * @ORM\Column(name="scope_max_long", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $scopeMaxLong;

    /**
     * @Groups({"experience:read"})
     * //TODO listener post persist to create unique field with lat, lng
     * @var string
     * @ORM\Column(name="hash_code",type="string", length=191, unique=true)
     */
    private $hashCode;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Position
     */
    public function setId(int $id): Position
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return Position
     */
    public function setLatitude(float $latitude): Position
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return Position
     */
    public function setLongitude(float $longitude): Position
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getAltitude(): float
    {
        return $this->altitude;
    }

    /**
     * @param float $altitude
     * @return Position
     */
    public function setAltitude(float $altitude): Position
    {
        $this->altitude = $altitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMinLat(): float
    {
        return $this->scopeMinLat;
    }

    /**
     * @param float $scopeMinLat
     * @return Position
     */
    public function setScopeMinLat(float $scopeMinLat): Position
    {
        $this->scopeMinLat = $scopeMinLat;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMaxLat(): float
    {
        return $this->scopeMaxLat;
    }

    /**
     * @param float $scopeMaxLat
     * @return Position
     */
    public function setScopeMaxLat(float $scopeMaxLat): Position
    {
        $this->scopeMaxLat = $scopeMaxLat;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMinLong(): float
    {
        return $this->scopeMinLong;
    }

    /**
     * @param float $scopeMinLong
     * @return Position
     */
    public function setScopeMinLong(float $scopeMinLong): Position
    {
        $this->scopeMinLong = $scopeMinLong;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMaxLong(): float
    {
        return $this->scopeMaxLong;
    }

    /**
     * @param float $scopeMaxLong
     * @return Position
     */
    public function setScopeMaxLong(float $scopeMaxLong): Position
    {
        $this->scopeMaxLong = $scopeMaxLong;
        return $this;
    }

    /**
     * @return string
     */
    public function getHashCode(): string
    {
        return $this->hashCode;
    }

    /**
     * @param string $hashCode
     * @return Position
     */
    public function setHashCode(string $hashCode): Position
    {
        $this->hashCode = $hashCode;
        return $this;
    }

}