<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 02/11/2018
 * Time: 15:09
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Journey
 *
 * @ORM\Entity
 * @ORM\Table(name="journeys")
 * @ApiResource()
 */
class Journey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name",type="string")
     * @Assert\Length(min = 3, max = 15)
     */
    private $name;

    /**
     * Many Journey have Many Spots.
     * @ORM\ManyToMany(targetEntity="App\Entity\Spot")
     * @ORM\JoinTable(name="journey_spots",
     *      joinColumns={@ORM\JoinColumn(name="experience_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="spot_id", referencedColumnName="id")}
     *      )
     */
    private $spots;

    /**
     * @var Experience
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="journeys")
     */
    private $experience;

    public function __construct()
    {
        $this->spots = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSpots()
    {
        return $this->spots;
    }

    /**
     * @param mixed $spots
     */
    public function setSpots($spots): void
    {
        $this->spots = $spots;
    }

    /**
     * @return Experience
     */
    public function getExperience(): Experience
    {
        return $this->experience;
    }

    /**
     * @param Experience $experience
     */
    public function setExperience(Experience $experience): void
    {
        $this->experience = $experience;
    }
}