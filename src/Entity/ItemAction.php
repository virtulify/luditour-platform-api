<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 12/11/2018
 * Time: 17:47
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Item
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity
 * @ORM\Table(name="item_actions")
 * @ApiResource()
 */
class ItemAction
{
    /**
     * @var int
     *
     * @Groups({"item:read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="actions")
     */
    private $item;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     * @Groups({"item:read"})
     *
     * @ApiProperty(description="The type of the action fired by the use of the main Item.")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", nullable=true)
     * @Groups({"item:read"})
     * @ApiProperty(description="The value of the action fired by the use of the main Item. Can be null if no value required.")
     */
    private $value;

    /**
     * @var string
     * @Groups({"item:read"})
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(description="Visual description of the action and what that will do in the application (Must be readable for the app's user)")
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ItemAction
     */
    public function setId(int $id): ItemAction
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     * @return ItemAction
     */
    public function setItem(Item $item): ItemAction
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return ItemAction
     */
    public function setType(int $type): ItemAction
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ItemAction
     */
    public function setValue(string $value): ItemAction
    {
        $this->value = $value;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

}