<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"experience:read"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="35")
     * @ORM\Column(type="string", length=35)
     * @Groups({"experience:read"})
     */
    private $name;

    /**
     * @Groups({"experience:read"})
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @Groups({"experience:read"})
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\Type(type="integer")
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExperienceTeam", mappedBy="team", orphanRemoval=true)
     */
    private $experienceTeams;

    /**
     * @Groups({"experience:read"})
     * @ORM\Column(type="integer")
     */
    private $visitorCount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->experienceTeams = new ArrayCollection();
        $this->score = 0;
        $this->visitorCount = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return Collection|ExperienceTeam[]
     */
    public function getExperienceTeams(): Collection
    {
        return $this->experienceTeams;
    }

    public function addExperienceTeam(ExperienceTeam $experienceTeam): self
    {
        if (!$this->experienceTeams->contains($experienceTeam)) {
            $this->experienceTeams[] = $experienceTeam;
            $experienceTeam->setTeam($this);
        }

        return $this;
    }

    public function removeExperienceTeam(ExperienceTeam $experienceTeam): self
    {
        if ($this->experienceTeams->contains($experienceTeam)) {
            $this->experienceTeams->removeElement($experienceTeam);
            // set the owning side to null (unless already changed)
            if ($experienceTeam->getTeam() === $this) {
                $experienceTeam->setTeam(null);
            }
        }

        return $this;
    }

    public function getVisitorCount(): ?int
    {
        return $this->visitorCount;
    }

    public function setVisitorCount(int $visitorCount): self
    {
        $this->visitorCount = $visitorCount;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
