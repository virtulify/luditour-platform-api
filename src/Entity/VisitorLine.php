<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 14/10/2018
 * Time: 23:06
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Validator as CustomAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * VisitorLine
 *
 * @ORM\Entity(repositoryClass="App\Repository\VisitorLineRepository")
 * @ORM\EntityListeners({
 *     "App\EntityListener\VisitorLineListener"
 * })
 * @ORM\Table(name="visitor_lines")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @CustomAssert\UniqueVisitorByExperience(groups={"postValidation"})
 *
 * @ApiFilter(BooleanFilter::class, properties={"current"})
 * @ApiResource(
 *     normalizationContext={"groups"={"visitorline", "visitorline:read"}},
 *     denormalizationContext={"groups"={"visitorline", "visitorline:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={"validation_groups"={"default", "postValidation"}}
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"validation_groups"={"default", "putValidation"}},
 *          "delete"
 *      }
 * )
 */
class VisitorLine
{
    /**
     * @var int
     * @Groups({"visitorline:read", "visitor:read"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var double
     *
     * @Groups({"visitorline", "visitor:read"})
     *
     * @ORM\Column(name="registration_latitude", type="decimal", precision=20, scale=17)
     * @ApiProperty(description="Latitude where the visitor has registered for the related experience.")
     */
    private $registrationLatitude;

    /**
     * @var double
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(name="registration_longitude", type="decimal", precision=20, scale=17)
     * @ApiProperty(description="Longitude where the visitor has registered for the related experience.")
     */
    private $registrationLongitude;

    /**
     * @var double
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(name="registration_altitude", type="decimal", precision=20, scale=17)
     * @ApiProperty(description="Altitude where the visitor has registered for the related experience.")
     */
    private $registrationAltitude;

    /**
     * @var \DateTime
     * @Groups({"visitorline:read", "visitor:read"})
     * @ORM\Column(name="registration_date", type="datetime", nullable=true)
     * //Gedmo\Timestampable(on="change", field={"registrationLongitude"})
     * @ApiProperty(description="Latitude where the visitor has registered for the related experience.")
     */
    private $registrationDate;

    /**
     * @Groups({"visitorline:read", "visitor:read", "experienceObject:read"})
     * @var Visitor
     * @ORM\ManyToOne(targetEntity="App\Entity\Visitor", inversedBy="visitorLines")
     * @ORM\JoinColumn(name="visitor_id", referencedColumnName="id")
     */
    private $visitor;

    /**
     * @var boolean
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(name="current", type="boolean")
     */
    private $current;

    /**
     * JSON data representing the data needed for the specified experience.
     * @var string
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(name="row_data", nullable=true, type="text")
     */
    private $rowData;

    /**
     * @var ExperienceTeam
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\ExperienceTeam", inversedBy="visitorLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $experienceTeam;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Groups({"visitorline:read", "visitor:read"})
     */
    private $kilometersWalked;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"visitorline:read", "visitor:read"})
     */
    private $score;

    /**
     * @var float
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(type="decimal", precision=20, scale=17, nullable=true)
     */
    private $currentLatitude;

    /**
     * @var float
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(type="decimal", precision=20, scale=17, nullable=true)
     */
    private $currentLongitude;

    /**
     * @var float
     * @Groups({"visitorline", "visitor:read"})
     * @ORM\Column(type="decimal", precision=20, scale=17, nullable=true)
     */
    private $currentAltitude;

    /**
     * @var \DateTime
     * @Groups({"visitorline:read", "visitor:read"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"visitorline", "visitor:read"})
     */
    private $gameTime;

    /**
     * @var int
     * @Groups({"visitorline:read", "visitor:read"})
     * @ApiProperty(description="The number of stars to rate the experience the visitor has played in the current visitor line")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var string
     * @Groups({"visitorline:read", "visitor:read"})
     * @ApiProperty(description="The feedback of the visitor for the experience played in the current visitor line")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opinion;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MiniGameState", mappedBy="player")
     */
    private $miniGameStates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Trade", mappedBy="sourceVisitor")
     */
    private $trades;

    public function __construct()
    {
        $this->gameTime = 0;
        $this->kilometersWalked = 0;
        $this->score = 0;
        $this->registrationDate = new \DateTime();
        $this->current = false;
        $this->lastUpdate = new \DateTime();
        $this->miniGameStates = new ArrayCollection();
        $this->trades = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getRowData(): string
    {
        return $this->rowData;
    }

    /**
     * @param string $rowData
     */
    public function setRowData(string $rowData): void
    {
        $this->rowData = $rowData;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return VisitorLine
     */
    public function setId(int $id): VisitorLine
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getRegistrationLatitude(): float
    {
        return $this->registrationLatitude;
    }

    /**
     * @param float $registrationLatitude
     * @return VisitorLine
     */
    public function setRegistrationLatitude(float $registrationLatitude): VisitorLine
    {
        $this->registrationLatitude = $registrationLatitude;
        if (!isset($this->currentLatitude)) {
            $this->currentLatitude = $registrationLatitude;
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getRegistrationLongitude(): float
    {
        return $this->registrationLongitude;
    }

    /**
     * @param float $registrationLongitude
     * @return VisitorLine
     */
    public function setRegistrationLongitude(float $registrationLongitude): VisitorLine
    {
        $this->registrationLongitude = $registrationLongitude;
        if (!isset($this->currentLongitude)) {
            $this->currentLongitude = $registrationLongitude;
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getRegistrationAltitude(): float
    {
        return $this->registrationAltitude;
    }

    /**
     * @param float $registrationAltitude
     * @return VisitorLine
     */
    public function setRegistrationAltitude(float $registrationAltitude): VisitorLine
    {
        $this->registrationAltitude = $registrationAltitude;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationDate(): \DateTime
    {
        return $this->registrationDate;
    }

    /**
     * @param \DateTime $registrationDate
     * @return VisitorLine
     */
    public function setRegistrationDate(\DateTime $registrationDate): VisitorLine
    {
        $this->registrationDate = $registrationDate;
        return $this;
    }
// TODO
//    /**
//     * @return float
//     */
//    public function getArrivalLatitude(): float
//    {
//        return $this->arrivalLatitude;
//    }
//
//    /**
//     * @param float $arrivalLatitude
//     * @return VisitorLine
//     */
//    public function setArrivalLatitude(float $arrivalLatitude): VisitorLine
//    {
//        $this->arrivalLatitude = $arrivalLatitude;
//        return $this;
//    }
//
//    /**
//     * @return float
//     */
//    public function getArrivalLongitude(): float
//    {
//        return $this->arrivalLongitude;
//    }
//
//    /**
//     * @param float $arrivalLongitude
//     * @return VisitorLine
//     */
//    public function setArrivalLongitude(float $arrivalLongitude): VisitorLine
//    {
//        $this->arrivalLongitude = $arrivalLongitude;
//        return $this;
//    }

//    /**
//     * @return float
//     */
//    public function getArrivalAltitude(): float
//    {
//        return $this->arrivalAltitude;
//    }
//
//    /**
//     * @param float $arrivalAltitude
//     * @return VisitorLine
//     */
//    public function setArrivalAltitude(float $arrivalAltitude): VisitorLine
//    {
//        $this->arrivalAltitude = $arrivalAltitude;
//        return $this;
//    }
//
//    /**
//     * @return \DateTime
//     */
//    public function getArrivalDate(): \DateTime
//    {
//        return $this->arrivalDate;
//    }
//
//    /**
//     * @param \DateTime $arrivalDate
//     * @return VisitorLine
//     */
//    public function setArrivalDate(\DateTime $arrivalDate): VisitorLine
//    {
//        $this->arrivalDate = $arrivalDate;
//        return $this;
//    }

    /**
     * @return Visitor
     */
    public function getVisitor()
    {
        return $this->visitor;
    }

    /**
     * @param mixed $visitor
     * @return VisitorLine
     */
    public function setVisitor($visitor)
    {
        $this->visitor = $visitor;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCurrent(): bool
    {
        return $this->current;
    }

    /**
     * @param bool $current
     * @return VisitorLine
     */
    public function setCurrent(bool $current): VisitorLine
    {
        $this->current = $current;
        return $this;
    }

    public function getExperienceTeam(): ?ExperienceTeam
    {
        return $this->experienceTeam;
    }

    public function setExperienceTeam(?ExperienceTeam $experienceTeam): self
    {
        $this->experienceTeam = $experienceTeam;

        return $this;
    }

    public function getKilometersWalked(): ?float
    {
        return $this->kilometersWalked;
    }

    public function setKilometersWalked(float $kilometersWalked): self
    {
        $this->kilometersWalked = $kilometersWalked;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getCurrentLatitude(): ?float
    {
        return $this->currentLatitude;
    }

    public function setCurrentLatitude(?float $currentLatitude): self
    {
        $this->currentLatitude = $currentLatitude;

        return $this;
    }

    public function getCurrentLongitude(): ?float
    {
        return $this->currentLongitude;
    }

    public function setCurrentLongitude(?float $currentLongitude): self
    {
        $this->currentLongitude = $currentLongitude;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(?\DateTimeInterface $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    public function getCurrentAltitude(): ?float
    {
        return $this->currentAltitude;
    }

    public function setCurrentAltitude(?float $currentAltitude): self
    {
        $this->currentAltitude = $currentAltitude;

        return $this;
    }

    public function getGameTime(): ?int
    {
        return $this->gameTime;
    }

    public function setGameTime(?int $gameTime): self
    {
        $this->gameTime = $gameTime;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getOpinion(): ?string
    {
        return $this->opinion;
    }

    public function setOpinion(?string $opinion): self
    {
        $this->opinion = $opinion;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|MiniGameState[]
     */
    public function getMiniGameStates(): Collection
    {
        return $this->miniGameStates;
    }

    public function addMiniGameState(MiniGameState $miniGameState): self
    {
        if (!$this->miniGameStates->contains($miniGameState)) {
            $this->miniGameStates[] = $miniGameState;
            $miniGameState->setPlayer($this);
        }

        return $this;
    }

    public function removeMiniGameState(MiniGameState $miniGameState): self
    {
        if ($this->miniGameStates->contains($miniGameState)) {
            $this->miniGameStates->removeElement($miniGameState);
            // set the owning side to null (unless already changed)
            if ($miniGameState->getPlayer() === $this) {
                $miniGameState->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Trade[]
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setSourceVisitor($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->contains($trade)) {
            $this->trades->removeElement($trade);
            // set the owning side to null (unless already changed)
            if ($trade->getSourceVisitor() === $this) {
                $trade->setSourceVisitor(null);
            }
        }

        return $this;
    }
}