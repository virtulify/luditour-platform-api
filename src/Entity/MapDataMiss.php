<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 06/12/2018
 * Time: 15:11
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer
 *
 * @ORM\Entity
 * @ORM\Table(name="map_data_miss")
 * @ApiResource()
 */
class MapDataMiss
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var double
     *
     * @ORM\Column(name="scope_min_lat", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $scopeMinLat;

    /**
     * @var double
     *
     * @ORM\Column(name="scope_max_lat", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $scopeMaxLat;

    /**
     * @var double
     *
     * @ORM\Column(name="scope_min_long", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $scopeMinLong;

    /**
     * @var double
     *
     * @ORM\Column(name="scope_max_long", type="decimal", precision=20, scale=17)
     * @Assert\Type(type="double")
     */
    private $scopeMaxLong;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MapDataMiss
     */
    public function setId(int $id): MapDataMiss
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMinLat(): float
    {
        return $this->scopeMinLat;
    }

    /**
     * @param float $scopeMinLat
     * @return MapDataMiss
     */
    public function setScopeMinLat(float $scopeMinLat): MapDataMiss
    {
        $this->scopeMinLat = $scopeMinLat;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMaxLat(): float
    {
        return $this->scopeMaxLat;
    }

    /**
     * @param float $scopeMaxLat
     * @return MapDataMiss
     */
    public function setScopeMaxLat(float $scopeMaxLat): MapDataMiss
    {
        $this->scopeMaxLat = $scopeMaxLat;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMinLong(): float
    {
        return $this->scopeMinLong;
    }

    /**
     * @param float $scopeMinLong
     * @return MapDataMiss
     */
    public function setScopeMinLong(float $scopeMinLong): MapDataMiss
    {
        $this->scopeMinLong = $scopeMinLong;
        return $this;
    }

    /**
     * @return float
     */
    public function getScopeMaxLong(): float
    {
        return $this->scopeMaxLong;
    }

    /**
     * @param float $scopeMaxLong
     * @return MapDataMiss
     */
    public function setScopeMaxLong(float $scopeMaxLong): MapDataMiss
    {
        $this->scopeMaxLong = $scopeMaxLong;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     * @return MapDataMiss
     */
    public function setCreationDate(\DateTime $creationDate): MapDataMiss
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Inflate with visitor scope data
     * @param Visitor $visitor
     */
    public function inflate(Visitor $visitor) {
        $this->scopeMaxLat = $visitor->getScopeMaxLat();
        $this->scopeMinLat = $visitor->getScopeMinLat();
        $this->scopeMaxLong = $visitor->getScopeMaxLong();
        $this->scopeMinLong = $visitor->getScopeMinLong();
        $this->creationDate = new \DateTime();
    }

}