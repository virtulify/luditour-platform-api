<?php
/**
 * Created by PhpStorm.
 * User: qwarn
 * Date: 09/11/2018
 * Time: 13:20
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Journey
 *
 * @ORM\Entity
 * @ORM\Table(name="avatar_medias")
 */

class AvatarMedia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="hat_media_id", referencedColumnName="id")
     */
    private $hatMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="head_media_id", referencedColumnName="id")
     */
    private $headMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="torso_media_id", referencedColumnName="id")
     */
    private $torsoMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="leg_media_id", referencedColumnName="id")
     */
    private $legMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="foot_media_id", referencedColumnName="id")
     */
    private $footMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(name="hand_media_id", referencedColumnName="id")
     */
    private $handMedia;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AvatarMedia
     */
    public function setId(int $id): AvatarMedia
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHatMedia()
    {
        return $this->hatMedia;
    }

    /**
     * @param mixed $hatMedia
     * @return AvatarMedia
     */
    public function setHatMedia($hatMedia)
    {
        $this->hatMedia = $hatMedia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeadMedia()
    {
        return $this->headMedia;
    }

    /**
     * @param mixed $headMedia
     * @return AvatarMedia
     */
    public function setHeadMedia($headMedia)
    {
        $this->headMedia = $headMedia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTorsoMedia()
    {
        return $this->torsoMedia;
    }

    /**
     * @param mixed $torsoMedia
     * @return AvatarMedia
     */
    public function setTorsoMedia($torsoMedia)
    {
        $this->torsoMedia = $torsoMedia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLegMedia()
    {
        return $this->legMedia;
    }

    /**
     * @param mixed $legMedia
     * @return AvatarMedia
     */
    public function setLegMedia($legMedia)
    {
        $this->legMedia = $legMedia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFootMedia()
    {
        return $this->footMedia;
    }

    /**
     * @param mixed $footMedia
     * @return AvatarMedia
     */
    public function setFootMedia($footMedia)
    {
        $this->footMedia = $footMedia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHandMedia()
    {
        return $this->handMedia;
    }

    /**
     * @param mixed $handMedia
     * @return AvatarMedia
     */
    public function setHandMedia($handMedia)
    {
        $this->handMedia = $handMedia;
        return $this;
    }
}