<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator as CustomAssert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\Entity(repositoryClass="App\Repository\EnterRuleRepository")
 * @ApiResource(
 *     description="Rule describing the conditions required for a player to play the mini game"
 * )
 * @CustomAssert\EnterRuleConstraint()
 */
class EnterRule
{
    const CONDITION_NONE = 0;
    const CONDITION_EXP_REQUIRED = 1;
    const CONDITION_TEAM_REQUIRED = 2;
    const CONDITION_TIME_LIMIT = 3;
    const CONDITION_DONE_ONCE = 4;
    const CONDITION_WEATHER = 5;
    const CONDITION_CURRENT_TIME = 6;
    const CONDITION_ITEM_REQUIRED = 7;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ApiProperty(description="The type of condition required to be able to play the mini game")
     */
    private $conditionType;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @ApiProperty(description="The required experience to be able to play the mini game")
     */
    private $requiredExp;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The time span when the player is able to play the mini game (beginning date)")
     */
    private $timeLimitBegin;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\GreaterThan(propertyPath="timeLimitBegin")
     * @ApiProperty(description="The time span when the player is able to play the mini game (ending date)")
     */
    private $timeLimitEnd;

    /**
     * @var ExperienceTeam
     * @ORM\ManyToOne(targetEntity="App\Entity\ExperienceTeam")
     * @ApiProperty(description="The team that is able to play the mini game. Null if there is no limit in the team of the player.")
     */
    private $requiredTeam;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The date from when the mini game can be played.")
     */
    private $beginningAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="The date until the mini game can be played")
     * @Assert\GreaterThan(propertyPath="beginningAt")
     */
    private $endingAt;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @ApiProperty(description="The weather required to play the mini game")
     */
    private $weatherCondition;

    /**
     * @var MiniGame
     * @ORM\ManyToOne(targetEntity="App\Entity\MiniGame", inversedBy="enterRules")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(description="The related mini game")
     */
    private $miniGame;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(description="Soft delete field")
     */
    private $deletedAt;

    /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="App\Entity\Item")
     * @ApiProperty(description="The item that is required if the logged in visitor wants to participates to the related mini game. (specification of one of the fields available)")
     */
    private $requiredItem;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     * @ApiProperty(description="Defines if the required visitor item is removed after the participation to the mini game.")
     */
    private $isItemConsumed;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @ApiProperty(description="The number of the required item needed to play")
     */
    private $requiredQuantity;

    public function __construct()
    {
        $this->conditionType = self::CONDITION_NONE;
        $this->isItemConsumed = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionType(): ?int
    {
        return $this->conditionType;
    }

    public function setConditionType(int $conditionType): self
    {
        $this->conditionType = $conditionType;

        return $this;
    }

    public function getRequiredExp(): ?int
    {
        return $this->requiredExp;
    }

    public function setRequiredExp(?int $requiredExp): self
    {
        $this->requiredExp = $requiredExp;

        return $this;
    }

    public function getRequiredTeam(): ?ExperienceTeam
    {
        return $this->requiredTeam;
    }

    public function setRequiredTeam(?ExperienceTeam $requiredTeam): self
    {
        $this->requiredTeam = $requiredTeam;

        return $this;
    }

    public function getBeginningAt(): ?\DateTimeInterface
    {
        return $this->beginningAt;
    }

    public function setBeginningAt(?\DateTimeInterface $beginningAt): self
    {
        $this->beginningAt = $beginningAt;

        return $this;
    }

    public function getEndingAt(): ?\DateTimeInterface
    {
        return $this->endingAt;
    }

    public function setEndingAt(?\DateTimeInterface $endingAt): self
    {
        $this->endingAt = $endingAt;

        return $this;
    }

    public function getWeatherCondition(): ?string
    {
        return $this->weatherCondition;
    }

    public function setWeatherCondition(?string $weatherCondition): self
    {
        $this->weatherCondition = $weatherCondition;

        return $this;
    }

    public function getMiniGame(): ?MiniGame
    {
        return $this->miniGame;
    }

    public function setMiniGame(?MiniGame $miniGame): self
    {
        $this->miniGame = $miniGame;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeLimitBegin(): \DateTime
    {
        return $this->timeLimitBegin;
    }

    /**
     * @param \DateTime $timeLimitBegin
     * @return EnterRule
     */
    public function setTimeLimitBegin(\DateTime $timeLimitBegin): self
    {
        $this->timeLimitBegin = $timeLimitBegin;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeLimitEnd(): ?\DateTime
    {
        return $this->timeLimitEnd;
    }

    /**
     * @param \DateTime $timeLimitEnd
     * @return EnterRule
     */
    public function setTimeLimitEnd(?\DateTime $timeLimitEnd): self
    {
        $this->timeLimitEnd = $timeLimitEnd;
        return $this;
    }

    public function getRequiredItem(): ?Item
    {
        return $this->requiredItem;
    }

    public function setRequiredItem(?Item $requiredItem): self
    {
        $this->requiredItem = $requiredItem;

        return $this;
    }

    public function getIsItemConsumed(): ?bool
    {
        return $this->isItemConsumed;
    }

    public function setIsItemConsumed(?bool $isItemConsumed): self
    {
        $this->isItemConsumed = $isItemConsumed;

        return $this;
    }

    public function getRequiredQuantity(): ?int
    {
        return $this->requiredQuantity;
    }

    public function setRequiredQuantity(?int $requiredQuantity): self
    {
        $this->requiredQuantity = $requiredQuantity;

        return $this;
    }
}
