<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-03-19
 * Time: 11:20
 */

namespace App\Controller;


use App\Entity\Item;
use App\Entity\ItemAction;
use App\Entity\Media;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use App\Exception\ConflictException;
use App\Exception\ForbiddenException;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class RegisterNfcController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * RegisterNfcController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/api/nfc/register/{nfc}",
     *     methods={"PUT"},
     *     name="api_nfc_register",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"VisitorItem", "Item", "NFC"},
     *              "summary"="Attributes a new item based on the specified nfc tag to the current visitor.",
     *              "parameters"={
     *                  {
     *                      "name"="nfc",
     *                      "in"="path",
     *                      "required"="true",
     *                      "type"="string"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The attributed visitor item.",
     *                      "schema"={
     *                          "$ref"="#/definitions/VisitorItem"
     *                      }
     *                  },
     *                  "409"={
     *                      "description"="If the nfc tag has already been attributed to the current visitor.",
     *                      "type"="string"
     *                  },
     *                  "404"={
     *                      "description"="If the specified nfc tag is not found.",
     *                      "type"="string"
     *                  },
     *                  "403"={
     *                      "description"="If the specified nfc tag is not used .",
     *                      "type"="string"
     *                  },
     *              }
     *          }
     *     }
     * )
     * @param string $nfc
     * @return array
     */
    public function __invoke(string $nfc)
    {
        /** @var Visitor $currentVisitor */
        $currentVisitor = $this->getUser();

        /** @var Item $item */
        $item = $this->entityManager->getRepository(Item::class)->findOneBy(['nfcCommonTag' => $nfc]);
        // Create visitor item if this is a common nfc tag
        if ($item !== null) {
            $visitorItem = new VisitorItem();
            $visitorItem->setItem($item);
            $visitorItem->setVisitor($currentVisitor);

            $this->entityManager->persist($visitorItem);
            $this->entityManager->flush();

            return $this->visitorItemOutput($visitorItem);
        }

        /** @var VisitorItem $visitorItem */
        // Attribute the visitor item to the current visitor (and ensure it is not used by anyone before)
        $visitorItem = $this->entityManager->getRepository(VisitorItem::class)->findOneBy(['nfcId' => $nfc]);
        if ($visitorItem !== null) {
            if ($visitorItem->getVisitor() && $visitorItem->getVisitor()->getId() !== $currentVisitor->getId()) {
                throw new ForbiddenException('There is already a user that uses the submitted NFC tag.');
            }

            if ($visitorItem->getVisitor() && $visitorItem->getVisitor()->getId() === $currentVisitor->getId()) {
                throw new ConflictException('NFC tag already used by the current visitor.');
            }

            $visitorItem->setVisitor($currentVisitor);
            $this->entityManager->flush();

            return $this->visitorItemOutput($visitorItem);
        }

        // The NFC tag is not found here.
        throw new NotFoundException('NFC tag not found.');
    }

    /**
     * Creates the visitor item object that will be serialized in json
     * @param VisitorItem $visitorItem
     * @return array
     */
    private function visitorItemOutput(VisitorItem $visitorItem)
    {
        /** @var Item $item */
        $item = $visitorItem->getItem();

        // Item output
        $itemOutput = [
            'id' => $item->getId(),
            'name' => $item->getName(),
            'appearanceImageMedia' => $this->mediaOutput($item->getAppearanceImageMedia()),
            'appearanceObjectMedia' => $this->mediaOutput($item->getAppearanceObjectMedia()),
            'weight' => $item->getWeight(),
            'multipleUseCount' => $item->getMultipleUseCount(),
            'actions' => $item->getActions()->map(function (ItemAction $itemAction) {
                return [
                    'id' => $itemAction->getId(),
                    'type' => $itemAction->getType(),
                    'value' => $itemAction->getValue(),
                    'description' => $itemAction->getDescription()
                ];
            })->getValues(),
            'usePeriodStart' => $item->getUsePeriodStart(),
            'usePeriodEnd' => $item->getUsePeriodEnd(),
            'usePeriodTimeStart' => $item->getUsePeriodTimeStart(),
            'usePeriodTimeEnd' => $item->getUsePeriodTimeEnd(),
            'effectPeriod' => $item->getEffectPeriod(),
            'nfcCommonTag' => $item->getNfcCommonTag(),
            'description' => $item->getDescription()
        ];

        return [
            'id' => $visitorItem->getId(),
            'useCount' => $visitorItem->getUseCount(),
            'nfcId' => $visitorItem->getNfcId(),
            'updated' => $visitorItem->getUpdated(),
            'activationDate' => $visitorItem->getActivationDate(),
            'endDate' => $visitorItem->getEndDate(),
            'item' => $itemOutput
        ];
    }

    private function mediaOutput(Media $media)
    {
        return !$media ? [] :
            [
                'id' => $media->getId(),
                'hashCode' => $media->getHashCode(),
                'webPath' => $media->getWebPath(),
                'filename' => $media->getFilename(),
                'diskUsage' => $media->getDiskUsage()
            ];
    }
}