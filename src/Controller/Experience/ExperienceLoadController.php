<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 05-04-19
 * Time: 17:34
 */

namespace App\Controller\Experience;


use App\Entity\Experience;
use App\Entity\ExperienceTeam;
use App\Entity\Item;
use App\Entity\Journey;
use App\Entity\MiniGame;
use App\Entity\Quest;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ExperienceLoadController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExperienceLoadController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/api/experiences/{id<\d+>}/load",
     *     methods={"GET"},
     *     name="api_experiences_get_item_load",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Experience", "Application"},
     *              "summary"="Retrieves the experience specified by its id and loads the items, mini games, etc",
     *              "parameters"={
     *                  {
     *                      "name"="id",
     *                      "type"="integer",
     *                      "in"="path",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The loaded experience data",
     *                      "type"="object"
     *                  },
     *                  "400"={
     *                      "description"="If the request is not valid.",
     *                      "type"="string"
     *                  },
     *                  "404"={
     *                      "description"="If the experience does not exist.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Experience $experience
     * @return Response
     */
    public function __invoke(Experience $experience)
    {
        // load mini games and the related enter rules
        $miniGames = $this->entityManager->getRepository(MiniGame::class)->findByExperienceJoinedEnterRules($experience);

        // Load the items related to the experience
        $items = $this->entityManager->getRepository(Item::class)->findByExperience($experience);

        // Load the experience teams with the team related
        $experienceTeams = $this->entityManager->getRepository(ExperienceTeam::class)->findByExperienceJoinedTeam($experience);

        // Load the quests related to the experience
        $quests = $this->entityManager->getRepository(Quest::class)->findAvailableByExperience($experience);

        // Load the journeys of the experience
        $journeys = $this->entityManager->getRepository(Journey::class)->findBy([
            'experience' => $experience
        ]);

        return new JsonResponse([
            'miniGames' => $miniGames,
            'items' => $items,
            'experienceTeams' => $experienceTeams,
            'quests' => $quests,
            'journeys' => $journeys
        ]);
    }
}