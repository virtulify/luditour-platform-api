<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 05-04-19
 * Time: 15:25
 */

namespace App\Controller\Experience;


use App\Entity\Experience;
use App\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class ExperienceListController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExperienceListController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/api/experiences/list",
     *     methods={"GET"},
     *     name="api_get_experiences_list_app_collection",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Experience"},
     *              "summary"="Retrieves the list of experiences that the logged in visitor can access from the database",
     *              "parameters"={
     *                  {
     *                      "name"="latitude",
     *                      "type"="number",
     *                      "format"="double",
     *                      "in"="query",
     *                      "description"="The latitude of the logged in visitor.",
     *                      "required"="true"
     *                  },
     *                  {
     *                      "name"="longitude",
     *                      "type"="number",
     *                      "format"="double",
     *                      "in"="query",
     *                      "description"="The longitude of the logged in visitor.",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="Message success",
     *                      "type"="object"
     *                  },
     *                  "400"={
     *                      "description"="If the longitude or latitude is not specified.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Request $request
     * @return Experience[]|array
     */
    public function __invoke(Request $request)
    {
        if (!$request->query->has('latitude') || !$request->query->has('longitude')) {
            return $this->entityManager->getRepository(Experience::class)->findPublicExperiences();
        }

        if (!is_float($request->get('latitude')) || !is_float($request->get('longitude'))) {
            throw new BadRequestException('The latitude and longitude query parameters must be valid floats.');
        }

        $latitude = floatval($request->query->get('latitude'));
        $longitude = floatval($request->query->get('longitude'));

        return $this->entityManager->getRepository(Experience::class)->findAvailableExperiences($latitude, $longitude);
    }
}