<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 28-03-19
 * Time: 17:55
 */

namespace App\Controller\VisitorItem;


use App\Entity\VisitorItem;
use App\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class VisitorItemDeletesController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * VisitorItemController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     name="api_delete_visitor_items_collection",
     *     methods={"DELETE"},
     *     path="/api/visitor_items",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"VisitorItem"},
     *              "summary"="Deletes the visitor items specified by the ids array in query string.",
     *              "parameters"={
     *                  {
     *                      "name"="ids",
     *                      "type"="string",
     *                      "in"="query",
     *                      "description"="The array of visitor item's id to delete.",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="Message success",
     *                      "type"="string"
     *                  },
     *                  "400"={
     *                      "description"="If the array of ids is not well encoded or does not exist.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request)
    {
        $idParams = $request->query->get('ids');
        if (!$idParams) {
            throw new BadRequestException("The table of id's to delete is missing");
        }

        $ids = explode(',', $idParams);
        foreach ($ids as $id) {
            if (!is_int($id)) {
                throw new BadRequestException("The ids must be integer values.");
            }
        }
        $visitorItems = $this->entityManager->getRepository(VisitorItem::class)->findSeveralByIds($ids);
        $deletedCount = 0;

        /** @var VisitorItem $visitorItem */
        foreach ($visitorItems as $visitorItem) {
            $this->entityManager->remove($visitorItem);
            $deletedCount++;
        }

        $this->entityManager->flush();
        return [
            'message' => $deletedCount . ' visitor items deleted'
        ];
    }
}