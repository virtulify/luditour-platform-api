<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 08-04-19
 * Time: 10:26
 */

namespace App\Controller\Visitor;


use App\Entity\Visitor;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

final class VisitorLoadController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * VisitorLoadController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/api/visitors",
     *     methods={"GET"},
     *     name="api_visitors_get_load_item",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Visitor", "Application"},
     *              "summary"="Retrieves the logged in visitor data and loads its inventory, visitor lines and its friends",
     *              "parameters"={
     *                  {
     *                      "name"="id",
     *                      "type"="integer",
     *                      "in"="path",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The loaded visitor data",
     *                      "type"="object"
     *                  },
     *                  "400"={
     *                      "description"="If the request is not valid.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @return array
     */
    public function __invoke()
    {
        /** @var Visitor $visitor */
        $visitor = $this->getUser();
        return $this->entityManager->getRepository(Visitor::class)->findVisitorLoad($visitor->getId());
    }
}