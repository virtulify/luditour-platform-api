<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 13-03-19
 * Time: 14:32
 */

namespace App\Controller;


use App\Exception\InternalServerErrorException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

final class OsefController extends AbstractController
{
    /**
     * @Route(
     *     path="/api/osef",
     *     name="api_osef_route",
     *     methods={"GET"},
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"OSEF"},
     *              "summary"="Osef, route just used to test something.",
     *              "responses"={
     *                  "200"={
     *                      "description"="I don't know, it's OSEF."
     *                  },
     *                  "409"={
     *                      "description"="Maybe..."
     *                  },
     *                  "404"={
     *                      "description"="Maybe..."
     *                  },
     *                  "403"={
     *                      "description"="Maybe..."
     *                  },
     *                  "500"={
     *                      "description"="Maybe..."
     *                  }
     *              }
     *          }
     *     }
     * )
     */
    public function __invoke()
    {
        throw new InternalServerErrorException('Ouais pas ouf');
    }
}