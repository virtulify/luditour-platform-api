<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 29-03-19
 * Time: 11:20
 */

namespace App\Controller;


use App\Dto\PingAppDto;
use App\Entity\Experience;
use App\Entity\ExperienceObject;
use App\Entity\ExperienceTeam;
use App\Entity\Spot;
use App\Entity\Trade;
use App\Entity\VisitorLine;
use App\Exception\BadRequestException;
use App\Manager\ExperienceObjectManager;
use App\Manager\ToolManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class AppDataController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ToolManager
     */
    private $toolManager;

    /**
     * @var ExperienceObjectManager
     */
    private $experienceObjectManager;

    /**
     * AppDataController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ToolManager $toolManager
     * @param ExperienceObjectManager $experienceObjectManager
     */
    public function __construct(EntityManagerInterface $entityManager, ToolManager $toolManager, ExperienceObjectManager $experienceObjectManager)
    {
        $this->entityManager = $entityManager;
        $this->toolManager = $toolManager;
        $this->experienceObjectManager = $experienceObjectManager;
    }

    /**
     * @Route(
     *     path="/api/experiences/{experienceId<\d+>}/visitor_lines/{visitorLineId<\d+>}/ping",
     *     name="api_put_ping_from_app",
     *     methods={"PUT"},
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Experience", "VisitorLine", "Application"},
     *              "summary"="Updates the position of the specified visitor line and retrieves the current scores of all the exprience teams related to the specified experience, the spots + experience objects that are accessible from the current position of the visitor.",
     *              "parameters"={
     *                  {
     *                      "name"="experienceId",
     *                      "in"="path",
     *                      "required"="true",
     *                      "type"="integer"
     *                  },
     *                  {
     *                      "name"="visitorLineId",
     *                      "in"="path",
     *                      "required"="true",
     *                      "type"="integer"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The attributed visitor item.",
     *                      "type"="object",
     *                      "properties"={
     *                          "scores"={
     *                              "type"="array",
     *                              "items"={
     *                                  "type"="object",
     *                                  "properties"={
     *                                      "experienceTeamId"={"type"="integer"},
     *                                      "score"={"type"="number", "format"="float"}
     *                                  }
     *                              }
     *                          },
     *                          "trades"={
     *                              "type"="array",
     *                              "items"={
     *                                  "type"="object"
     *                              }
     *                          },
     *                          "spots"={
     *                              "type"="array",
     *                              "items"={
     *                                  "type"="object"
     *                              }
     *                          },
     *                          "experienceObjects"={
     *                              "type"="array",
     *                              "items"={
     *                                  "type"="object"
     *                              }
     *                          }
     *                      }
     *                  },
     *                  "404"={
     *                      "type"="string",
     *                      "description"="If the experience or the visitor line is not found."
     *                  }
     *              }
     *          }
     *     }
     * )
     *
     * @ParamConverter("experience", options={"id"="experienceId"})
     * @ParamConverter("visitorLine", options={"id"="visitorLineId"})
     *
     * @param Request $request
     * @param Experience $experience
     * @param VisitorLine $visitorLine
     * @return array
     */
    public function __invoke(Request $request, Experience $experience, VisitorLine $visitorLine)
    {
        $body = json_decode($request->getContent(), true);
        $pingAppDto = new PingAppDto($body);
        $this->updateVisitorLine($visitorLine, $pingAppDto);

        // retrieve the valid area coordinates (500 meters around the current position)
        $coordinates = $this->toolManager->getAreaCoordinates($pingAppDto->latitude, $pingAppDto->longitude);

        // retrieve the array of new trades
        $newTrades = $this->entityManager->getRepository(Trade::class)->findAskedTradesByDestination($this->getUser());

        // retrieve the array of spots that are located around the current visitor
        $availableSpots = $this->entityManager->getRepository(Spot::class)->findNearLocationByExperience($coordinates, $experience->getId());

        // retrieve the array of available experience object for the current position and experience
        $availableExperienceObjects = $this->experienceObjectManager->getNearExperienceObjects($experience, $pingAppDto->latitude, $pingAppDto->longitude);

        // retrieve the array of scores for each team of the event
        $scores = $this->getExperienceTeamScores($experience);

        return [
            'trades' => $newTrades,
            'spots' => $availableSpots,
            'experienceObjects' => $availableExperienceObjects,
            'scores' => $scores
        ];
    }

    /**
     * Updates the score, gametime and position of the specified visitorLine with the PingAppDto
     * @param VisitorLine $visitorLine
     * @param PingAppDto $pingAppDto
     */
    private function updateVisitorLine(VisitorLine $visitorLine, PingAppDto $pingAppDto)
    {
        $visitorLine->setScore($pingAppDto->score);
        $visitorLine->setGameTime($pingAppDto->gameTime);
        $visitorLine->setCurrentLongitude($pingAppDto->longitude);
        $visitorLine->setCurrentLatitude($pingAppDto->latitude);
        $this->entityManager->flush();
    }

    /**
     * Builds the array of experience team scores.
     * @param Experience $experience
     * @return array the scores of each experience team
     */
    private function getExperienceTeamScores(Experience $experience)
    {
        $scores = [];
        /** @var ExperienceTeam $experienceTeam */
        foreach ($experience->getExperienceTeams() as $experienceTeam) {
            $scores[] = [
                'experienceTeamId' => $experienceTeam->getId(),
                'score' => $experienceTeam->getScore()
            ];
        }

        return $scores;
    }
}