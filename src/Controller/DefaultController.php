<?php
/**
 * Created by PhpStorm.
 * User: Stagiaire
 * Date: 09-11-18
 * Time: 16:41
 */

namespace App\Controller;

use App\Entity\ExperienceObject;
use App\Entity\Visitor;
use App\Entity\VisitorItem;
use App\Entity\VisitorLine;
use App\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DefaultController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/maps")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        if (!$request->query->has('protection') || $request->query->get('protection') !== 'a4QvnpHPxv3xLXpItVxH') {
            return $this->redirect('/');
        }

        /** @var integer $registeredVisitors */
        $registeredVisitors = $this->entityManager->getRepository(Visitor::class)->countAll();

        $activeVisitors = $this->entityManager->getRepository(VisitorLine::class)->findActive();
        $activeVisitorCount = count($activeVisitors);

        /** @var mixed $activeExperienceObjects */
        $activeExperienceObjects = $this->entityManager->getRepository(ExperienceObject::class)->findActive();

        $activeExperienceObjectsCount = count($activeExperienceObjects);

        $activatedVisitorItems = $this->entityManager->getRepository(VisitorItem::class)->countFreshActivated();

        return $this->render('realtime.html.twig', [
            'registeredVisitors' => $registeredVisitors,
            'activeVisitors' => $activeVisitors,
            'activeVisitorCount' => $activeVisitorCount,
            'activeExperienceObjects' => $activeExperienceObjects,
            'activeExperienceObjectsCount' => $activeExperienceObjectsCount,
            'activatedVisitorItems' => $activatedVisitorItems
        ]);
    }
}