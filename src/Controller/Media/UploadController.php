<?php

namespace App\Controller\Media;


use ApiPlatform\Core\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Media;
use App\Form\MediaFileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class UploadController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * UploadController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     path="/api/media/upload",
     *     methods={"POST"},
     *     name="api_post_medias_upload_collection",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Media"},
     *              "summary"="Uploads a media file.",
     *              "parameters"={
     *                  {
     *                      "name"="file",
     *                      "type"="file",
     *                      "in"="formData",
     *                      "description"="The notification content file to upload",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The newly created media.",
     *                      "schema"={
     *                          "$ref"="#/definitions/Media"
     *                      }
     *                  },
     *                  "400"={
     *                      "description"="If the submitted file is not correct.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Request $request
     * @return Media
     */
    public function __invoke(Request $request)
    {
        $media = new Media();

        $form = $this->createForm(MediaFileType::class, $media);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($media);
            $this->entityManager->flush();

            // Prevent the serialization of the file property
            $media->file = null;

            return $media;
        }

        // This will be handled by API Platform and returns a validation error.
        throw new ValidationException($this->validator->validate($media));
    }
}