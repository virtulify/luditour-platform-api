<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 20-12-18
 * Time: 10:42
 */

namespace App\Controller\AssetBundleMedia;


use App\Entity\AssetBundleMedia;
use App\Form\AssetBundleMediaType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class AssetBundleMediaCreateController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * AssetBundleMediaCreateController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     path="/api/asset_bundle_media/mdr",
     *     methods={"POST"},
     *     name="api_asset_bundle_media_posteeee_collection",
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"AssetBundleMedia"},
     *              "summary"="Creates a new Asset Bundle Media.",
     *              "parameters"={
     *                  {
     *                      "name"="Asset bundle media",
     *                      "in"="body",
     *                      "type"="object",
     *                      "properties"={
     *                          "name"={"type"="string"},
     *                          "description"={"type"="string"},
     *                          "androidMedia"={"type"="integer"},
     *                          "iosMedia"={"type"="integer"},
     *                          "editorMedia"={"type"="integer"}
     *                      }
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The newly created asset bundle media.",
     *                      "schema"={
     *                          "$ref"="#/definitions/Media"
     *                      }
     *                  },
     *                  "400"={
     *                      "description"="If the submitted file is not correct.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     *
     * @param Request $request
     * @return AssetBundleMedia
     */
    public function __invoke(Request $request)
    {
        $json = json_decode($request->getContent());
        $assetBundleMedia = new AssetBundleMedia();
        $form = $this->createForm(AssetBundleMediaType::class, $assetBundleMedia);
        $form->submit($json);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($assetBundleMedia);
            $this->entityManager->flush();
            return $assetBundleMedia;
        }

        throw new BadRequestHttpException($this->validator->validate($assetBundleMedia));
    }
}