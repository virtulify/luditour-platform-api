<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 05-04-19
 * Time: 12:11
 */

namespace App\Controller\Trade;


use App\Entity\Trade;
use App\Entity\Visitor;
use App\Exception\ForbiddenException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

final class DenyTradeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DenyTradeController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/api/trades/{id<\d+>}/deny",
     *     name="api_put_trades_deny_item",
     *     methods={"PUT"},
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Trade"},
     *              "summary"="The logged in visitor denies the specified trade.",
     *              "parameters"={
     *                  {
     *                      "name"="id",
     *                      "type"="string",
     *                      "in"="path",
     *                      "required"="true"
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                  },
     *                  "400"={
     *                      "description"="",
     *                      "type"="string"
     *                  },
     *                  "403"={
     *                      "description"="The logged in visitor is not the destination of the trade and so, cannot accept the specified trade"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Trade $trade
     * @return array
     */
    public function __invoke(Trade $trade)
    {
        /** @var Visitor $visitor */
        $visitor = $this->getUser();
        if ($visitor->getId() !== $trade->getDestinationVisitor()->getId()) {
            throw new ForbiddenException('The logged in visitor must be the destination of the trade.');
        }

        $trade->setState(Trade::STATE_DENIED);
        $this->entityManager->flush();
    }
}