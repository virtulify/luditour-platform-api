<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 05-04-19
 * Time: 14:04
 */

namespace App\Controller\Setting;


use App\Entity\Setting;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

final class GeneralSettingsController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GeneralSettingsController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route(
     *     path="/settings",
     *     name="api_get_settings_general_collection",
     *     methods={"GET"},
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"Setting", "Application"},
     *              "summary"="Retrieves the general settings for the Hootside application.",
     *              "responses"={
     *                  "200"={
     *                      "description"="The list of the general settings for the Hootside application.",
     *                      "type"="array",
     *                      "items"={
     *                          "$ref"="#/definitions/Setting"
     *                      }
     *                  }
     *              }
     *          }
     *     }
     * )
     * @return Setting[]|object[]
     */
    public function __invoke()
    {
        return $this->entityManager->getRepository(Setting::class)
            ->findBy([
                'experience' => null
            ]);
    }
}