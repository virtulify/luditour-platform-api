<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 06-03-19
 * Time: 18:05
 */

namespace App\Controller\ExperienceObject;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\ExperienceObject;
use App\Entity\GenerationRule;
use App\Entity\Media;
use App\Entity\VisitorLine;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use App\Form\ExperienceObjectType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class ExperienceObjectPostController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ExperienceObjectPostController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     path="/api/experience_objects",
     *     name="api_experience_object_post_with_generation_rule_collecton",
     *     methods={"POST"},
     *     defaults={
     *          "_api_respond"=true,
     *          "_api_normalization_context"={"api_sub_level"=true},
     *          "_api_swagger_context"={
     *              "tags"={"ExperienceObject", "GenerationRule"},
     *              "summary"="Creates a new Experience Object with the related Generation Rule.",
     *              "parameters"={
     *                  {
     *                      "name"="content",
     *                      "in"="body",
     *                      "required"="true",
     *                      "type"="object",
     *                      "properties"={
     *                          "creator"={"type"="integer"},
     *                          "latitude"={"type"="number", "format"="double"},
     *                          "longitude"={"type"="number", "format"="double"},
     *                          "altitude"={"type"="number", "format"="double"},
     *                          "mapAppearanceMedia"={"type"="string"}
     *                      }
     *                  }
     *              },
     *              "responses"={
     *                  "200"={
     *                      "description"="The newly created experience object.",
     *                      "schema"={
     *                          "$ref"="#/definitions/ExperienceObject"
     *                      }
     *                  },
     *                  "400"={
     *                      "description"="If the submitted file is not correct.",
     *                      "type"="string"
     *                  },
     *                  "404"={
     *                      "description"="If the related Generation rule is not found.",
     *                      "type"="string"
     *                  }
     *              }
     *          }
     *     }
     * )
     * @param Request $request
     * @return ExperienceObject
     */
    public function __invoke(Request $request)
    {
        $json = json_decode($request->getContent(), true);
        if ($json['mapAppearanceMedia'] == null) {
            throw new BadRequestException("The appearance media is required.");
        }

        $media = $this->entityManager->getRepository(Media::class)->findOneBy(['filename' => $json['mapAppearanceMedia']]);

        /** @var GenerationRule $generationRule */
        $generationRule = $media ? $this->entityManager->getRepository(GenerationRule::class)->findOneBy(['mapAppearanceMedia' => $media]) : null;

        if ($generationRule == null) {
            throw new NotFoundException('The generation rule specified by the mapAppearanceMedia ' . $json['mapAppearanceMedia'] . ' is not found');
        }

        $visitorLine = $this->entityManager->getRepository(VisitorLine::class)->findOneBy([
            'id' => intval($json['creator']),
            'visitor' => $this->getUser()
        ]);

        if (!$visitorLine) {
            throw new BadRequestException('Creator visitor line not related to the current visitor.');
        }

        // update score of visitor line TODO : need generalization
        $newScore = $visitorLine->getScore();

        // if simple tree, one point
        if ($generationRule->getId() === 12 || $generationRule->getId() === 13) {
            $newScore++;
        }

        // if mega tree, two points
        // if simple tree, one point
        if ($generationRule->getId() === 16 || $generationRule->getId() === 17) {
            $newScore += 2;
        }
        $visitorLine->setScore($newScore);

        $experienceObject = new ExperienceObject();
        $experienceObject->setCreator($visitorLine);
        $experienceObject->setGenerationRule($generationRule);
        $experienceObject->setLatitude(floatval($json['latitude']));
        $experienceObject->setAltitude(floatval($json['altitude']));
        $experienceObject->setLongitude(floatval($json['longitude']));

        $errors = $this->validator->validate($experienceObject);
        if (!empty($errors)) {
            throw new BadRequestException($errors);
        }

        $this->entityManager->persist($experienceObject);
        $this->entityManager->flush();

        return $experienceObject;

//        $form = $this->createForm(ExperienceObjectType::class, $experienceObject);

//        $json['generationRule'] = $generationRule->getId();
//        if ($form->isValid()) {
//            $this->entityManager->persist($experienceObject);
//            $this->entityManager->flush();
//            return $experienceObject;
//        }
    }
}