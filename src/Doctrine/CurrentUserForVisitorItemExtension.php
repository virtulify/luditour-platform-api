<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 22-02-19
 * Time: 15:56
 */

namespace App\Doctrine;


use App\Entity\VisitorItem;

class CurrentUserForVisitorItemExtension extends AbstractCurrentUserExtension
{

    /**
     * Returns the supported class of the CurrentUserExtension
     * @return string the supported class of the CurrentUserExtension
     */
    protected function getSupportedResourceClass()
    {
        return VisitorItem::class;
    }
}