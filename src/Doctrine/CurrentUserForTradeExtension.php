<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 05-04-19
 * Time: 11:17
 */

namespace App\Doctrine;


use App\Entity\Trade;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CurrentUserForTradeExtension
 * @package App\Doctrine
 *
 * Filters the selected trades on the source or destination (must be the current visitor or an admin)
 */
class CurrentUserForTradeExtension extends AbstractCurrentUserExtension
{

    /**
     * Returns the supported class of the CurrentUserExtension
     * @return string the supported class of the CurrentUserExtension
     */
    protected function getSupportedResourceClass()
    {
        return Trade::class;
    }

    /**
     * (overrides this "addWhere" method to perform an "OR" operation on the trade's source or destination.
     * @param QueryBuilder $queryBuilder
     * @param string $resourceClass
     */
    protected function addWhere(QueryBuilder $queryBuilder, string $resourceClass)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof UserInterface && $this->getSupportedResourceClass() === $resourceClass && !$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.sourceVisitor = :current_user OR %s.destinationVisitor = :current_user', $rootAlias, $rootAlias));
            $queryBuilder->setParameter('current_user', $user->getId());
        }
    }
}