<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 14-02-19
 * Time: 11:25
 */

namespace App\Doctrine;


use App\Entity\Visitor;

class CurrentUserForVisitorExtension extends AbstractCurrentUserExtension
{

    /**
     * Returns the supported class of the CurrentUserExtension
     * @return string the supported class of the CurrentUserExtension
     */
    protected function getSupportedResourceClass()
    {
        return Visitor::class;
    }

    protected function getFieldOfUser()
    {
        return 'id';
    }
}