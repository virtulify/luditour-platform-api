<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 22-02-19
 * Time: 15:44
 */

namespace App\Doctrine;


use App\Entity\VisitorLine;

class CurrentUserForVisitorLineExtension extends AbstractCurrentUserExtension
{

    /**
     * Returns the supported class of the CurrentUserExtension
     * @return string the supported class of the CurrentUserExtension
     */
    protected function getSupportedResourceClass()
    {
        return VisitorLine::class;
    }
}