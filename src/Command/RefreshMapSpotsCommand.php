<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RefreshMapSpotsCommand extends Command
{
    protected static $defaultName = 'RefreshMapSpots';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('filepathOSM', InputArgument::REQUIRED, 'OSM Filepath to process')/*  ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description') */
            ->addArgument('category', InputArgument::REQUIRED, 'Type of category')/*  ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description') */
            ->addArgument('dataType', InputArgument::REQUIRED, 'Type of information')/*  ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description') */
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $filepathOSM = $input->getArgument('filepathOSM');
        $dataType = $input->getArgument('dataType');
        $category = $input->getArgument('category');

        $osm = new \Services_OpenStreetMap();

        $osm->loadXml($filepathOSM);
        $results = $osm->search(array($category => $dataType));
        $io->writeln("List of $dataType in $category\n");
        $io->writeln("==================\n\n");

        /**
         * @var $result \Services_OpenStreetMap_Node
         */
        foreach ($results as $result) {
            $name = $result->getTag('name');
            if (!empty($name)) {
                $io->writeln($name . " " . $result->getLat() . ';' . $result->getLon());
                $io->writeln("--");

            }
        }

        $io->success('Data got from OpenStreetMap with success.');
    }
}
