<?php

namespace App\Validator;

use App\Entity\EnterRule;
use App\Entity\MiniGameState;
use App\Entity\VisitorItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MiniGameStateConstraintValidator extends ConstraintValidator
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * MiniGameStateConstraintValidator constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * Validates if the player specified in the mini game state is related to the logged in visitor
     * @param MiniGameState $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var MiniGameStateConstraint $constraint */
        $this->checkEnterRuleGrant($value, $constraint);
    }

    /**
     * Checks whether the enter rules of the related mini game are all respected by the mini game state.
     * @param MiniGameState $miniGameState
     * @param MiniGameStateConstraint $constraint
     */
    private function checkEnterRuleGrant(MiniGameState $miniGameState, MiniGameStateConstraint $constraint)
    {
        /** @var EnterRule[] $enterRules */
        $enterRules = $this->entityManager->getRepository(EnterRule::class)->findBy(['miniGame' => $miniGameState->getMiniGame()->getId()]);
        foreach ($enterRules as $enterRule) {
            if (!$this->resolveEnterRule($miniGameState, $enterRule)) {
                $this->context->buildViolation($constraint->checkEnterRuleMessage)
                    ->setParameter('{{ enterRule }}', $enterRule->getId())
                    ->addViolation();
            }
        }
    }

    /**
     * Validates the specified enter rule
     * @param MiniGameState $miniGameState
     * @param EnterRule $enterRule
     * @return bool if the enter rule is respected or else false
     */
    private function resolveEnterRule(MiniGameState $miniGameState, EnterRule $enterRule)
    {
        switch ($enterRule->getConditionType()) {
            case EnterRule::CONDITION_NONE:
                return true;
            case EnterRule::CONDITION_EXP_REQUIRED:
                return $this->validateExpRequired($enterRule, $miniGameState);
            case EnterRule::CONDITION_TIME_LIMIT:
                return $this->validateTimeLimit($enterRule);
            case EnterRule::CONDITION_DONE_ONCE:
                return $this->validateDoneOnce($enterRule, $miniGameState);
            case EnterRule::CONDITION_WEATHER:
                return $this->validateWeather($enterRule);
            case EnterRule::CONDITION_CURRENT_TIME:
                return $this->validateCurrentTime($enterRule);
            case EnterRule::CONDITION_ITEM_REQUIRED:
                return $this->validateItemRequired($enterRule);
            default:
                return false;
        }
    }

    /**
     * Validates an enter rule that has a "EXP REQUIRED" condition type
     * @param EnterRule $enterRule
     * @param MiniGameState $miniGameState
     * @return bool true if the current visitor has enough score or else false
     */
    private function validateExpRequired(EnterRule $enterRule, MiniGameState $miniGameState)
    {
        return $miniGameState->getPlayer()->getScore() >= $enterRule->getRequiredExp();

    }

    /**
     * Validates an enter rule that has a "TIME LIMIT" condition type
     * @param EnterRule $enterRule
     * @return bool true if the current date time is in the time limit range or else false
     */
    private function validateTimeLimit(EnterRule $enterRule)
    {
        return true;
    }

    /**
     * Valdates an enter rule that has a "ONE TIME LIMIT" condition type
     * @param EnterRule $enterRule
     * @param MiniGameState $miniGameState
     * @return bool true if the current visitor has never done the mini game or else false
     */
    private function validateDoneOnce(EnterRule $enterRule, MiniGameState $miniGameState)
    {
        $playedMiniGameStates = $this->entityManager->getRepository(MiniGameState::class)
            ->findByVisitorLineAndMiniGame($miniGameState->getPlayer(), $miniGameState->getMiniGame());
        return empty($playedMiniGameStates);
    }

    /**
     * Validates an enter rule that has a "WEATHER" condition type
     * @param EnterRule $enterRule
     * @return bool true if the weather is valid in the weather condition granted by the enter rule
     */
    private function validateWeather(EnterRule $enterRule)
    {
        return true;
    }

    /**
     * Validates an enter rule that has a "CURRENT TIME" condition type
     * @param EnterRule $enterRule
     * @return bool true if the current time is in the range of accepted dates for the current mini game
     */
    private function validateCurrentTime(EnterRule $enterRule)
    {
        $now = new \DateTime();
        return $now > $enterRule->getEndingAt() || $now < $enterRule->getBeginningAt();
    }

    /**
     * Validates an enter rule that has a "ITEM REQUIRED" condition type
     * @param EnterRule $enterRule
     * @return bool true if the current visitor has the item specified by the enter rule's required item or else false
     */
    private function validateItemRequired(EnterRule $enterRule)
    {
        $visitorItems = $this->entityManager->getRepository(VisitorItem::class)->findBy([
            'visitor' => $this->tokenStorage->getToken()->getUser(),
            'deletedAt' => null,
            'item' => $enterRule->getRequiredItem(),
        ], null, $enterRule->getRequiredQuantity());
        return !empty($visitorItems) && count($visitorItems) >= $enterRule->getRequiredQuantity();
    }
}
