<?php

namespace App\Validator;

use App\Entity\ExperienceObject;
use App\Entity\Visitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CurrentUserForCreatorValidator extends ConstraintValidator
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * CurrentUserForVisitorLineValidator constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ExperienceObject $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var CurrentUserForCreator $constraint */

        $visitorLine = $value->getCreator();

        /** @var Visitor $visitor */
        $visitor = $this->tokenStorage->getToken()->getUser();

        if ($visitor->getVisitorLines()->contains($visitorLine)) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ visitorLineId }}', $value)
            ->atPath('creator')
            ->addViolation();
    }
}
