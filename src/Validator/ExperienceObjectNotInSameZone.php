<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExperienceObjectNotInSameZone extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'There is another experience object too near from the submitted experience object.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
