<?php

namespace App\Validator;

use App\Entity\ExperienceObject;
use App\Manager\ExperienceObjectManager;
use App\Manager\ToolManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExperienceObjectNotInSameZoneValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ToolManager
     */
    private $toolManager;

    /**
     * ExperienceObjectNotInSameZoneValidator constructor.
     * @param EntityManagerInterface $entityManager
     * @param ToolManager $toolManager
     */
    public function __construct(EntityManagerInterface $entityManager, ToolManager $toolManager)
    {
        $this->entityManager = $entityManager;
        $this->toolManager = $toolManager;
    }

    /**
     * Checks if the experience object is too near from another experience object that is already activated.
     * @param ExperienceObject $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $experience = $value->getGenerationRule()->getExperience();

        $coordinates = $this->toolManager->getAreaCoordinates($value->getLatitude(), $value->getLongitude(), $value->getGenerationRule()->getViewRange());

        /** @var ExperienceObject[] $experienceObjectsNear */
        $experienceObjectsNear = $this->entityManager->getRepository(ExperienceObject::class)->findNearLocation($coordinates);

        if (count($experienceObjectsNear) !== 0) {
            /** @var ExperienceObjectNotInSameZone $constraint */
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
