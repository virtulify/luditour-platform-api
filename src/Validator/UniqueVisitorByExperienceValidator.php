<?php

namespace App\Validator;

use App\Entity\Visitor;
use App\Entity\VisitorLine;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueVisitorByExperienceValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UniqueVisitorByExperienceValidator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param VisitorLine $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var UniqueVisitorByExperience $constraint */
        /** @var Visitor $visitor */
        $visitor = $value->getVisitor();

        // SELECT the visitor lines of the current user related to the linked experience team
        $linesOfCurrentTeam = $this->entityManager->getRepository(VisitorLine::class)
            ->findOneBy([
                'visitor' => $visitor,
                'experienceTeam' => $value->getExperienceTeam()
            ]);

        if ($linesOfCurrentTeam !== null) {
            $this->context->buildViolation($constraint->message)
                ->atPath('experienceTeam')
                ->addViolation();
        }
    }
}
