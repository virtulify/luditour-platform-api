<?php

namespace App\Validator;

use App\Entity\Trade;
use App\Entity\Visitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TraderConstraintValidator extends ConstraintValidator
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * TraderConstraintValidator constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Trade $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var TradeConstraint $constraint */
        $this->notFriendsValidation($value, $constraint);
        $this->itemsNotTradeableValidation($value, $constraint);
        $this->sourceNotOwnerValidation($value, $constraint);
        $this->sourceSameDestinationValidation($value, $constraint);
        $this->visitorCurrentValidation($value, $constraint);
    }

    /**
     * Validates whether the source and destination of the trade are friends
     * @param Trade $trade
     * @param TradeConstraint $constraint
     */
    private function notFriendsValidation(Trade $trade, TradeConstraint $constraint)
    {
        $areFriends = $trade->getDestinationVisitor()->getFriends()->contains($trade->getSourceVisitor());
        if (!$areFriends) {
            $this->context->buildViolation($constraint->notFriends)
                ->setParameters([
                    '{{ sourceName }}' => $trade->getSourceVisitor()->getUsername(),
                    '{{ destination }}' => $trade->getDestinationVisitor()->getUsername()
                ])
                ->addViolation();
        }
    }

    /**
     * Validates whether the trade's donations are tradeable
     * @param Trade $trade
     * @param TradeConstraint $constraint
     */
    private function itemsNotTradeableValidation(Trade $trade, TradeConstraint $constraint)
    {
        foreach ($trade->getDonations() as $donation) {
            if (!$donation->getItem()->getIsTradeable()) {
                $this->context->buildViolation($constraint->itemsNotTradebable)
                    ->setParameter('{{ itemName }}', $donation->getItem()->getName())
                    ->addViolation();
            }
        }
    }

    /**
     * Validates whether the source is the owner of the trade's donations
     * @param Trade $trade
     * @param TradeConstraint $constraint
     */
    private function sourceNotOwnerValidation(Trade $trade, TradeConstraint $constraint)
    {
        foreach ($trade->getDonations() as $donation) {
            if ($donation->getVisitor()->getId() !== $trade->getSourceVisitor()->getId()) {
                $this->context->buildViolation($constraint->sourceNotOwner)
                    ->setParameter('{{ sourceName }}', $trade->getSourceVisitor()->getUsername())
                    ->addViolation();
            }
        }
    }

    /**
     * Validates whether the source and the destination are not the same guys
     * @param Trade $trade
     * @param TradeConstraint $constraint
     */
    private function sourceSameDestinationValidation(Trade $trade, TradeConstraint $constraint)
    {
        if ($trade->getSourceVisitor()->getId() === $trade->getDestinationVisitor()->getId()) {
            $this->context->buildViolation($constraint->sourceSameDestination)
                ->setParameter('{{ visitorName }}', $trade->getSourceVisitor()->getUsername())
                ->addViolation();
        }
    }

    /**
     * Validates whether the source or the destination visitor is related to the logged in visitor.
     * @param Trade $trade
     * @param TradeConstraint $constraint
     */
    private function visitorCurrentValidation(Trade $trade, TradeConstraint $constraint)
    {
        /** @var Visitor $visitor */
        $visitor = $this->tokenStorage->getToken()->getUser();
        if ($trade->getSourceVisitor()->getId() !== $visitor->getId() && $trade->getDestinationVisitor()->getId() !== $visitor->getId()) {
            $this->context->buildViolation($constraint->visitorCurrent)
                ->addViolation();
        }
    }
}
