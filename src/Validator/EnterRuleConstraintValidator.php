<?php

namespace App\Validator;

use App\Entity\EnterRule;
use App\Entity\Experience;
use App\Entity\Item;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EnterRuleConstraintValidator
 * @package App\Validator
 * Validator for all enter rule validations
 */
class EnterRuleConstraintValidator extends ConstraintValidator
{
    /**
     * @param EnterRule $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        $experience = $value->getMiniGame()->getExperience();
        /** @var EnterRuleConstraint $constraint */
        $this->itemRequired($value, $constraint);
        $this->itemRelatedToExperience($value->getRequiredItem(), $experience, $constraint);
        $this->teamRelatedToExperience($value, $constraint);
    }

    /**
     * Checks if the experience of the Experience team related to the specified enter rule is the same as the experience specified by the mini game
     * (if there is no experience team specified, the test will be accepted).
     * @param EnterRule $enterRule
     * @param EnterRuleConstraint $constraint
     * @return void builds a violation context if the related experience team is not related to the experience of the mini game
     */
    private function teamRelatedToExperience(EnterRule $enterRule, EnterRuleConstraint $constraint)
    {
        $experienceTeam = $enterRule->getRequiredTeam();
        if ($experienceTeam == null || $experienceTeam->getExperience()->getId() !== $enterRule->getMiniGame()->getExperience()->getId()) {
            $this->context->buildViolation($constraint->teamMessage)
                ->setParameter('{{ team }}', $enterRule->getRequiredTeam()->getId())
                ->atPath('requiredTeam')
                ->addViolation();
        }
    }

    /**
     * builds a validation error when the specified item is not related to the experience of the mini game
     * @param Item $item
     * @param Experience $experience
     * @param EnterRuleConstraint $constraint
     */
    private function itemRelatedToExperience(Item $item, Experience $experience, EnterRuleConstraint $constraint)
    {
        foreach ($item->getExperiences() as $itemExperience) {
            if ($experience->getId() !== $itemExperience->getId()) {
                $this->context->buildViolation($constraint->itemsMessage)
                    ->setParameter('{{ item }}', $item->getName())
                    ->atPath('requiredItem')
                    ->addViolation();
            }
        }
    }

    /**
     * Validates an enter rule with a REQUIRED ITEM condition type. Checks whether the required item exists.
     * @param EnterRule $enterRule
     * @param EnterRuleConstraint $constraint
     */
    private function itemRequired(EnterRule $enterRule, EnterRuleConstraint $constraint)
    {
        if ($enterRule->getConditionType() === EnterRule::CONDITION_ITEM_REQUIRED && !$enterRule->getRequiredItem()) {
            $this->context->buildViolation($constraint->requiredItemMessage)
                ->atPath('requiredItem')
                ->addViolation();
        }
    }
}
