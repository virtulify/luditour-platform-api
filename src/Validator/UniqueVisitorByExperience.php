<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueVisitorByExperience extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'The submitted visitor cannot have more than one visitor line for the same experience team.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
