<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MiniGameStateConstraint extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $relatedVisitorMessage
        = 'The specified player\'s id ({{ visitorLineId }}) is related to the logged in visitor ({{ visitorId }}).';

    public $checkEnterRuleMessage = 'The enter rule {{ enterRule }} refuses the access of the mini game for the current visitor.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
