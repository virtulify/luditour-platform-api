<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TradeConstraint extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $notFriends = 'The source visitor ({{ sourceName }}) and the destination visitor ({{ destinationName }}) are not friends.';

    public $sourceNotOwner = 'The source visitor ({{ sourceName }}) is not the owner of the items to trade.';

    public $itemsNotTradebable = 'The item ({{ itemName }}) is not tradeable.';

    public $sourceSameDestination = 'The source and destination are the same ({{ visitorName }}).';

    public $visitorCurrent = 'Cannot access a trade that is not related to the logged in visitor';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
