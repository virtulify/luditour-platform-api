<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EnterRuleConstraint extends Constraint
{
    public $teamMessage = 'The team "{{ team }}" must be related to the experience of the mini game.';

    public $itemsMessage = 'The item "{{ item }}" must be related to the experience of the mini game.';

    public $requiredItemMessage = 'The condition type of the current enter rule is "REQUIRED ITEM" and cannot be null';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
