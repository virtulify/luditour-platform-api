<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 01-03-19
 * Time: 13:45
 */

namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\ExperienceOutput;
use App\Entity\Experience;

final class ExperienceOutputDataTransformer implements DataTransformerInterface
{

    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param Experience $object
     *
     * @param string $to
     * @param array $context
     * @return ExperienceOutput
     */
    public function transform($object, string $to, array $context = [])
    {
        $output = new ExperienceOutput($object);
        return $output;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        return ExperienceOutput::class === $to && $object instanceof Experience;
    }
}