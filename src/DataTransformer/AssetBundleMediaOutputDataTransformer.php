<?php
/**
 * Created by Augmenteo.
 * User: Stagiaire
 * Date: 06-03-19
 * Time: 15:26
 */

namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\AssetBundleMediaOutput;
use App\Entity\AssetBundleMedia;

final class AssetBundleMediaOutputDataTransformer implements DataTransformerInterface
{

    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param AssetBundleMedia $object
     *
     * @param string $to
     * @param array $context
     * @return AssetBundleMediaOutput
     */
    public function transform($object, string $to, array $context = [])
    {
        $output = new AssetBundleMediaOutput($object);
        return $output;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        return AssetBundleMediaOutput::class === $to && $object instanceof AssetBundleMedia;
    }
}